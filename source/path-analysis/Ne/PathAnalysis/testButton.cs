﻿using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using ESRI.ArcGIS.ArcMapUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using Teklabz.Ne.PathAnalysis.EndUser.OutputManager;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;

namespace Teklabz.Ne.PathAnalysis
{
    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
  public  class TestButton : BaseCommand
    {
        public const string Guid = "7f54b0de-0b45-4dfa-a3f3-9ebe4f5efacb";
        public const string ProgId = "Teklabz.Ne.PathAnalysis.TestButton";

        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType)
        {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType)
        {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType)
        {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType)
        {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);
        }

        #endregion

        #endregion
        public TestButton()
        {
            m_category = "Teklabz";
            m_caption = "Test Tool";
            m_message = "Test Tool";
            m_toolTip = "Test Tool";
            m_name = "select Test ";

            try
            {
                var bitmapResourceName = GetType().Name + ".bmp";
                m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        public override void OnCreate(object hook)
        {
            if (hook == null)
                return;
            if (hook is IMxApplication)
                m_enabled = true;
            else
                m_enabled = false;
        }

        public override void OnClick()
        {
            var structures = NeApiAccess.Instance.Structures.Selected;
            var featureList = new List<NeInventoryFeature>();
            foreach (var str in structures)
            {
                featureList.Add(str);
            }
            var ex = new ExtentInfo();
            ex.GetExtent(featureList, "C:\\Users\\farah\\Desktop\\notsure.png");
         
        }

       

    }
}
