﻿using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Security;

namespace Teklabz.Ne.PathAnalysis.Common
{
    public class SecurityManager {

        private const string AppSettings = "NETB.PA.AppSet";
        private const string ReportAttribute = "NETB.PA.RepAtt";
        private const string TemplateModel = "NETB.PA.TempMod";
        private const string TemplateName = "NETB.PA.TempName";
        private const string PathAnalysisEndUser = "TK_PA_ENDUSER";

        private static bool HasRole(NeUser user, params string[] roles) {
            NeApiAccess.Instance.Logger.LogMessage("Checking user's security models.");
            var userPrivileges = user.SecurityModels;
            foreach (var role in roles) {
                if (userPrivileges.ContainsKey(role))
                    return true;
            }

            NeApiAccess.Instance.Logger.LogMessage("Checking group's security models.");
            var groups = user.Groups;
            foreach (var neGroup in groups) {
                var groupPrivileges = neGroup.SecurityModels;
                foreach (var role in roles) {
                    if (groupPrivileges.ContainsKey(role))
                        return true;
                }
            }

            return false;
        }

        private static bool IsApplicationSettings(NeUser user) {
            return HasRole(user, AppSettings);
        }

        private static bool IsReportAttributeUser(NeUser user) {
            return HasRole(user, ReportAttribute);
        }

        private static bool IsTemplateModelUser(NeUser user) {
            return HasRole(user, TemplateModel);
        }

        private static bool IsTemplateNameUser(NeUser user) {
            return HasRole(user, TemplateName);
        }

        private static bool IsEndUser(NeUser user) {
            return HasRole(user, PathAnalysisEndUser);
        }

        public static bool CanPerformAppSettings() {
            var neApi = NeApiAccess.Instance;
            if (!IsApplicationSettings(neApi.Users.CurrentUser)) {
                neApi.Logger.ShowErrorDialog("Insufficient Privileges",
                    "Only privileged users with security role " + AppSettings +
                    " can use this operation.");
                return false;
            }

            return true;
        }

        public static bool CanPerformTemplateModel() {
            var neApi = NeApiAccess.Instance;
            if (!IsTemplateModelUser(neApi.Users.CurrentUser)) {
                neApi.Logger.ShowErrorDialog("Insufficient Privileges",
                    "Only privileged users with security role " + TemplateModel +
                    " can use this operation.");
                return false;
            }

            return true;
        }

        public static bool CanPerformReportAttributes() {
            var neApi = NeApiAccess.Instance;
            if (!IsReportAttributeUser(neApi.Users.CurrentUser)) {
                neApi.Logger.ShowErrorDialog("Insufficient Privileges",
                    "Only privileged users with security role " + ReportAttribute +
                    " can use this operation.");
                return false;
            }

            return true;
        }

        public static bool CanPerformTemplateName() {
            var neApi = NeApiAccess.Instance;
            if (!IsTemplateNameUser(neApi.Users.CurrentUser)) {
                neApi.Logger.ShowErrorDialog("Insufficient Privileges",
                    "Only privileged users with security role " + TemplateName +
                    " can use this operation.");
                return false;
            }

            return true;
        }
        
        public static bool CanPerformEndUserPathAnalysis() {
            var neApi = NeApiAccess.Instance;
            if (!IsEndUser(neApi.Users.CurrentUser)) {
                neApi.Logger.ShowErrorDialog("Insufficient Privileges",
                    "Only privileged users with security role " + PathAnalysisEndUser +
                    " can use this operation.");
                return false;
            }

            return true;
        }

    }
}
