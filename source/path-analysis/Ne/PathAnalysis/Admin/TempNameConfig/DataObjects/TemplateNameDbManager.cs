﻿using ESRI.ArcGIS.Geodatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects
{
    public class TemplateNameDbManager
    {
        public const string TableName = "TK_PA_TEMPLATE_NAMES";
        public const string TempName = "TEMPLATE_NAME";
        public const string DescriptionFld = "TEMPLATE_DESCRIPTION";
        public const string ObjectIdFld = "OBJECTID";


        public static readonly List<string> TemplateModelList = new List<string> {
            ObjectIdFld,
            TempName,
            DescriptionFld

        };
        public static bool CheckTable()
        {
            var findTbl = NeApiAccess.Instance.Database.GetNeTable(TableName);
            if (findTbl == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(string.Format("Table {0} does not exist.", TableName));
                return false;
            }
            return true;
        }



        private static string ValidateFields(ITable table, List<string> tableList)
        {
            var message = string.Empty;
            try
            {
                foreach (var field in tableList)
                {
                    var indx = table.FindField(field);
                    if (indx == -1)
                    {
                        message += (string.IsNullOrEmpty(message) ? "The Fields : " : ",") + field;
                    }
                }
                if (!string.IsNullOrEmpty(message))
                {
                    message += " Was Not Found ";
                }
            }
            catch (Exception ex)
            {
                message += " " + ex;
            }
            return message;
        }
        private static void FillRowBuffer(IRowBuffer roBuffer, ITable table, TemplateNames model)
        {
            roBuffer.Value[table.FindField(TempName)] = model.TemplateName;
            roBuffer.Value[table.FindField(DescriptionFld)] = model.Description;
        
        }

        /// <summary>
        /// Get All Values From StcOwaWoAutoTrans Table as List of Objects
        /// </summary>
        /// <returns>List of table Objects</returns>
        public static List<TemplateNames> GetAllTtemplateNames()
        {
            var list = FindTemplateNames(null);
            return list;
        }

        /// <summary>
        /// Get Value From StcOwaWoAutoTrans Table as One Object Using ObjectID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>WoAutoTrans Object Or Null</returns>
        public static TemplateNames FindTempNames(int id)
        {
            var list = FindTemplateNames(ObjectIdFld + " = " + id);
            return list.FirstOrDefault();
        }

        /// <summary>
        /// Get All Values From StcOwaWoAutoTrans Table as List of Objects using SQL query
        /// </summary>
        /// <param name="query">SQL Query</param>
        /// <returns>List of table Objects</returns>
        public static List<TemplateNames> FindTemplateNames(string query)
        {
            var neApi = NeApiAccess.Instance;
            var templateNames = new List<TemplateNames>();
            const string tableName = TableName;
            try
            {
                var objects = neApi.Database.FindNeObjects(tableName, query);
                foreach (var neObject in objects)
                {
                    var allModels = new TemplateNames();
                    if (!neObject.Fields[ObjectIdFld].IsValueNull())
                        allModels.ObjectId = neObject.Fields[ObjectIdFld].GetValue<int>();
                    if (!neObject.Fields[TempName].IsValueNull())
                        allModels.TemplateName = neObject.Fields[TempName].GetValue<string>();
                    if (!neObject.Fields[DescriptionFld].IsValueNull())
                        allModels.Description = neObject.Fields[DescriptionFld].GetValue<string>();


                    templateNames.Add(allModels);
                }
                templateNames = templateNames.OrderBy(x => x.ToString()).ToList();
            }
            catch (Exception ex)
            {
                neApi.Logger.LogError(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
                throw new Exception(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
            }
            return templateNames;
        }


        public static bool Add(TemplateNames model)
        {
            var neApi = NeApiAccess.Instance;
            ICursor crsr = null;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                throw new Exception(string.Format("Table {0} was not found in the database", TableName));
            }
            try
            {
                transaction.StartTransaction();
                string message = ValidateFields(table, TemplateModelList);
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
                var roBuffer = table.CreateRowBuffer();
                crsr = table.Insert(true);
                FillRowBuffer(roBuffer, table, model);
                var oid = crsr.InsertRow(roBuffer);
                crsr.Flush();
                model.ObjectId = (int)oid;
                transaction.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Adding  " + TableName + " Values ", ex);
                return false;
            }
            finally
            {
                if (crsr != null)
                {
                    Marshal.ReleaseComObject(crsr);
                }
            }
        }

        /// <summary>
        /// Update StcOwaWoAutoTrans Table value
        /// </summary>
        /// <param name="model"></param>
        /// <returns>True Or False</returns>
        public static bool Update(TemplateNames model)
        {
            var neApi = NeApiAccess.Instance;
            ICursor crsr = null;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                return false;
            }
            try
            {
                transaction.StartTransaction();
                string message = ValidateFields(table, TemplateModelList);
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
                var queryFilter = new QueryFilter();
                queryFilter.WhereClause = "OBJECTID=" + model.ObjectId;
                crsr = table.Search(queryFilter, false);
                var row = crsr.NextRow();
                FillRowBuffer(row, table, model);
                row.Store();
                transaction.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Updating  " + TableName + " Values ", ex);
                return false;
            }
            finally
            {
                if (crsr != null)
                {
                    Marshal.ReleaseComObject(crsr);
                }
            }
        }

        /// <summary>
        /// Remove StcOwaWoAutoTrans Table Value Using record Object ID
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns>True Or False</returns>
        public static bool Delete(int objectId)
        {
            var neApi = NeApiAccess.Instance;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                throw new Exception(string.Format("Table {0} was not found in the database", TableName));
            }
            try
            {
                transaction.StartTransaction();
                var queryFilter = new QueryFilter();
                queryFilter.WhereClause = string.Format("OBJECTID={0}", objectId);
                table.DeleteSearchedRows(queryFilter);
                ICursor crsr = table.Search(queryFilter, false);
                transaction.CommitTransaction();
                return crsr.NextRow() == null;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Deleting " + TableName + " Values ", ex);
                return false;
            }
        }
    }
}
