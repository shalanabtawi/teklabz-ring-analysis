﻿namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects
{
    public class TemplateModels
    {
        public string TemplateName { set; get; }
        public string ClassName { set; get; }
        public string TypeName { set; get; }
        public string CategoryName { set; get; }
        public string ModelName { set; get; }
        public int ObjectId;

        public TemplateModels(string templateName, string className, string typeName, string categoryName, string modelName)
        {
            TemplateName = templateName;
            ClassName = className;
            TypeName = typeName;
            CategoryName = categoryName;
            ModelName = modelName;
         
        }

        public TemplateModels()
        {
        }

        public static int Compare(TemplateModels tempModel, TemplateModels otherTempModel) {
            var tem = tempModel.TemplateName;
            var className = tempModel.ClassName;
            var cat = tempModel.CategoryName;
            var type = tempModel.TypeName;
            var model = tempModel.ModelName;

            var otherTem = otherTempModel.TemplateName;
            var otherClassName = otherTempModel.ClassName;
            var otherCat = otherTempModel.CategoryName;
            var otherType = otherTempModel.TypeName;
            var otherModel = otherTempModel.ModelName;

            if (!string.Equals(tem, otherTem)) {
                return tem.CompareTo(otherTem);
            }
            if (!string.Equals(className, otherClassName))
            {
                return className.CompareTo(otherClassName);
            }
            if (cat == "ANY" && otherCat != "ANY")
            {
                return -1;
            }
            if (otherCat == "ANY" && cat != "ANY")
            {
                return 1;
            }
            if (!string.Equals(cat, otherCat))
            {
                return cat.CompareTo(otherCat);
            }
            if (type == "ANY" && otherType != "ANY")
            {
                return -1;
            }
            if (otherType == "ANY" && type != "ANY")
            {
                return 1;
            }
            if (!string.Equals(type, otherType))
            {
                return type.CompareTo(otherType);
            }
            if (model == "ANY" && otherModel != "ANY")
            {
                return -1;
            }
            if (otherModel == "ANY" && model != "ANY")
            {
                return 1;
            }
            if (!string.Equals(model, otherModel))
            {
                return model.CompareTo(otherModel);
            }

            //if(model=="ANY" && otherModel == "ANY")
            //{
            //    return 0;
            //}

            return 0;  }
    }
}
