﻿
namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects
{
 public   class ComboFilterItem
    {
        public string Code { set; get; }
        public string Value { set; get; }

        public ComboFilterItem(string code, string value)
        {
            Code = code;
            Value = value;
        }

        public ComboFilterItem()
        {
        }

        public   override string ToString()
        {
            return Value ;
        }
    }
}
