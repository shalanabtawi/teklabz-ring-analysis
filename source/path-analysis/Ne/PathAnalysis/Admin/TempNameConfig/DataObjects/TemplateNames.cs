﻿namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects
{
    public class TemplateNames
    {
        public string TemplateName { set; get; }
        public string Description { set; get; }

        public int ObjectId;

        public TemplateNames(string templateName, string desc)
        {
            TemplateName = templateName;
            Description = desc;
        
        }

        public override string ToString() {
            return TemplateName;
        }

        public TemplateNames()
        {
        }
    }
}
