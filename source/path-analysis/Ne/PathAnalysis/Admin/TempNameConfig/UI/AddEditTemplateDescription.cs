﻿using Equin.ApplicationFramework;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.Admin.TemplateModelConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;

namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.UI
{
    public partial class AddEditTemplateDescription : Form
    {
        public TemplateNames crntTemp;
        public BindingListView<TemplateNames> AllValues { set; get; }
        public BindingListView<TemplateModels> OriginalList { set; get; }
        public AddEditTemplateDescription(TemplateNames editObj = null)
        {
            InitializeComponent();
            dgvTemplateModels.AutoGenerateColumns = false;
            if (editObj == null)
            {
                return;
            }
            crntTemp = editObj;
            txtBxDesc.Text = editObj.Description;
            txtBxName.Text = editObj.TemplateName;
          //  txtBxName.Enabled = false;
            FillTemplateModels();

        }

        private void FillTemplateModels()
        {
            var allObjects = TemplateModelDbManager.FindTemplateModels("TEMPLATE_NAME='" + txtBxName.Text + "'");
            allObjects.Sort(TemplateModels.Compare);

            var updatedList = new BindingListView<TemplateModels>(allObjects);
            var diff = new BindingListView<TemplateModels>(allObjects);
            OriginalList = diff;
            dgvTemplateModels.DataSource = updatedList;
        }

        private void Ok(object sender, EventArgs e)
        {
            var descrip = txtBxDesc.Text;
            var tempName = txtBxName.Text;

            if (!ValidateInput())
            {
                return;
            }

            if (crntTemp != null && crntTemp.ObjectId != 0)
            {

                crntTemp.Description = descrip;
                crntTemp.TemplateName = tempName;

                if (CheckIfAlreadyExists(crntTemp, true))
                {
                    return;
                }
                TemplateNameDbManager.Update(crntTemp);
            }
            else
            {

                var newAtts = new TemplateNames(tempName, descrip);
                if (CheckIfAlreadyExists(newAtts, false))
                {
                    return;
                }
                if (crntTemp == null)
                {
                    crntTemp = new TemplateNames();

                }
                crntTemp = newAtts;
                TemplateNameDbManager.Add(crntTemp);
            }
            SaveTempModel();
            DialogResult = DialogResult.OK;
        }

        private void SaveTempModel()
        {
            var tempModels = dgvTemplateModels.DataSource as BindingListView<TemplateModels>;
            //remove all existing before 
            var existing = TemplateModelDbManager.FindTemplateModels("TEMPLATE_NAME='" + txtBxName.Text + "'");
            foreach (var deleteObj in existing)
            {
                TemplateModelDbManager.Delete(deleteObj.ObjectId);
            }

            //add all models
            foreach (var temp in tempModels.DataSource)
            {
                var crntTemp=temp as TemplateModels;
                crntTemp.TemplateName = txtBxName.Text;
                TemplateModelDbManager.Add(crntTemp);
            }

        }

        private bool ValidateInput()
        {
            var name = txtBxName.Text;
            if (name == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you enter a name.");
                txtBxName.Focus();
                return false;
            }
            var des = txtBxDesc.Text;
            if (des == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you enter a description.");
                txtBxDesc.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(name.Trim()) || string.IsNullOrEmpty(des.Trim()))
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                      "Please make sure the name and/or the description is filled.");
                return false;
            }
            var allModels= dgvTemplateModels.DataSource as BindingListView<TemplateModels>;
            if (dgvTemplateModels.DataSource == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                   "Please include at least one template model.");
                return false;
            }
            if (allModels.Count == 0)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please include at least one template model.");
                return false;
            }
            return true;
        }

        private void Close(object sender, EventArgs e)
        {
            
            Cursor = Cursors.Default;
            Close();
        }

        private bool CheckFilled()
        {
            var tempName = txtBxName.Text;
            var tempDesc = txtBxDesc.Text;
            var allModels=dgvTemplateModels.DataSource as BindingListView<TemplateModels>;
            if(tempName!=null && tempDesc != null)
            {
                dgvTemplateModels.DataSource = OriginalList;
            }
            return true;   }

        private bool CheckIfAlreadyExists(TemplateNames disp, bool edit)
        {
            if (disp == null)
            {
                return true;
            }
            if (AllValues == null)
            {
                return false;
            }
            foreach (var row in AllValues.DataSource)
            {
                var temp = row as TemplateNames;
                var des = temp.Description;
                var tempName = temp.TemplateName;

                if (edit)
                {
                    if (disp.ObjectId != temp.ObjectId &&
                                               tempName.Equals(disp.TemplateName))
                    {

                        NeApiAccess.Instance.Logger.ShowErrorDialog(
                                    "Please note that the template name is already used, No duplications are allowed and therefore the row wont be added.");
                        return true;
                    }
                }
                else if (tempName.Trim().Equals(disp.TemplateName.Trim()) || tempName.ToLower().Trim().Equals(disp.TemplateName.ToLower().Trim()))
                {

                    NeApiAccess.Instance.Logger.ShowErrorDialog(
                           "Please note that the template name is already used, No duplications are allowed and therefore the row wont be added.");

                    return true;
                }
            }
            return false;
        }

        private void AddTemplate(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var tempName = txtBxName.Text;
                if (!ValidateName())
                {
                    return;
                }
                var bindingList = (BindingListView<TemplateModels>)dgvTemplateModels.DataSource;
                var openForm = new AddEditTemplateModelForm(tempName) { AllValues = bindingList };
               
                var returnDialog = openForm.ShowDialog();
                if (returnDialog != DialogResult.OK)
                {
                    Cursor = Cursors.Default;
                    return;
                }
                Cursor = Cursors.Default;
                var added = openForm.crntTemp;
                if (openForm.AllValues == null)
                {
                    var addedList = new List<TemplateModels>();
                    addedList.Add(added);
                    openForm.AllValues = new BindingListView<TemplateModels>(addedList);
                }
                else openForm.AllValues.DataSource.Add(added);
                var conList = openForm.AllValues.DataSource as List<TemplateModels>;
                conList.Sort(TemplateModels.Compare);

                var updatedList = new BindingListView<TemplateModels>(openForm.AllValues.DataSource);

                dgvTemplateModels.DataSource = updatedList;
                dgvTemplateModels.Refresh();
                Cursor = Cursors.Default;
                SelectRow(added);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                NeApiAccess.Instance.Logger.LogError("Error while trying to select row");
            }
        }

        private bool ValidateName()
        {
            var tempName = txtBxName.Text;
            if (tempName == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a Template Name");
                txtBxName.Focus();
                return false;
            }

            var tempDesc = txtBxDesc.Text;
            if (tempDesc == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a Template Description");
                txtBxDesc.Focus();
                return false;
            }
            return true;   }

        private void SelectRow(TemplateModels obj)
        {
            dgvTemplateModels.ClearSelection();

            foreach (DataGridViewRow gvRow in dgvTemplateModels.Rows)
            {
                var src = gvRow.DataBoundItem as ObjectView<TemplateModels>;
                if (src == null)
                {
                    continue;
                }

                if (string.Equals(src.Object.ObjectId, obj.ObjectId))
                {
                    dgvTemplateModels.CurrentCell = dgvTemplateModels.Rows[gvRow.Index].Cells[0];
                    dgvTemplateModels.Rows[gvRow.Index].Selected = true;
                    break;
                }
            }
        }

        private void UpdateTemplate(object sender, EventArgs e)
        {
            UpdateSelected();
            Cursor = Cursors.Default;
        }

        private void UpdateSelected()
        {
            try
            {
                if (dgvTemplateModels.DataSource == null)
                {
                    return;
                }
                if (dgvTemplateModels.SelectedRows.Count <= 0)
                {
                    NeApiAccess.Instance.Logger.ShowErrorDialog("No row was selected.");
                    return;
                }
                var bindingList = (BindingListView<TemplateModels>)dgvTemplateModels.DataSource;
                if (dgvTemplateModels.SelectedRows.Count == 0)
                {
                    return;
                }

                var needsEditing = (ObjectView<TemplateModels>)dgvTemplateModels.SelectedRows[0].DataBoundItem;

                if (needsEditing == null)
                {
                    return;
                }
                var crntModel = needsEditing.Object;
                Cursor = Cursors.WaitCursor;
                var openForm = new AddEditTemplateModelForm(txtBxName.Text,needsEditing.Object) { AllValues = bindingList };

      
                var returnDialog = openForm.ShowDialog();
                if (returnDialog != DialogResult.OK)
                {
                    return;
                }
                Cursor = Cursors.Default;
                var newAtt = openForm.crntTemp;
                if (newAtt != null)
                {
                    newAtt.ObjectId = crntModel.ObjectId;
                }

                var edittedIndex = bindingList.DataSource.IndexOf(crntModel);
                bindingList.DataSource[edittedIndex] = newAtt;
                var updatedList = new BindingListView<TemplateModels>(openForm.AllValues.DataSource);


                dgvTemplateModels.DataSource = updatedList;
                dgvTemplateModels.Refresh();
                Cursor = Cursors.Default;
                SelectRow(crntModel);
            }  
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Updating Selected Record ", ex);
                NeApiAccess.Instance.Logger.LogError("Error While Updating Selected Record ", ex);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvTemplateModels.SelectedRows.Count <= 0)
                {
                    NeApiAccess.Instance.Logger.ShowErrorDialog("No row was selected.");
                    return;
                }

                var dialogResult = MessageBox.Show(@"Are you sure you want to delete the selected item?",
                    @"Delete Selected Template Model Configuration", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                var attsObj = (ObjectView<TemplateModels>)dgvTemplateModels.SelectedRows[0].DataBoundItem;
                //TemplateModelDbManager.Delete(attsObj.Object.ObjectId);

                var bindingList = (BindingListView<TemplateModels>)dgvTemplateModels.DataSource;

                if (bindingList != null)
                {
                    bindingList.DataSource.Remove(attsObj.Object);
                    var updatedList = new BindingListView<TemplateModels>(bindingList.DataSource);
                    //TemplateModelDbManager.Delete(attsObj.Object.ObjectId);
                    dgvTemplateModels.DataSource = updatedList;
                }
                dgvTemplateModels.Refresh();
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Deleting Selected Record ", ex);
                NeApiAccess.Instance.Logger.LogError("Error While Deleting Selected Record ", ex);
            }
        }

        private string GetCategory(string className, string typeName, string modName)
        {
            var neApi = NeApiAccess.Instance;
            List<NeObject> model;
            if (modName != "ANY")
            {
                model = NeApiAccess.Instance.Database.FindNeObjects("model", "TYPE_NAME ='" + typeName + "' AND CLASS_NAME='" + className + "'AND MODEL_NAME='" + modName + "'");

            }
            else
            {
                model = NeApiAccess.Instance.Database.FindNeObjects("model", "TYPE_NAME ='" + typeName + "' AND CLASS_NAME='" + className + "' ");

            }

            var catName = model[0].SafeGetFieldValue("CATEGORY_NAME");


            return catName;
        }

        private void dgvTemplateModels_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            UpdateSelected();
        }
    }
}
