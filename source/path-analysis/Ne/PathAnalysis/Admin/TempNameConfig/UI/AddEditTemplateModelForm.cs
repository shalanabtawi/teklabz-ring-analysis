﻿using Equin.ApplicationFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Reference;

namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig
{
    public partial class AddEditTemplateModelForm : Form
    {
        public TemplateModels crntTemp;
        public string TemplateName;

        public BindingListView<TemplateModels> AllValues { set; get; }
        public AddEditTemplateModelForm(string TempName, TemplateModels editObj = null)
        {
            InitializeComponent();
            FillTemplateNames(TempName);
            FillLFeatureClasses();
            TemplateName = TempName;
            cmbxFeaClass.SelectedIndex = -1;
            cmbxFeaCat.SelectedIndex = -1;
            cmbxFeaType.SelectedIndex = -1;
            cmbxFeaModel.SelectedIndex = -1;

            Cursor = Cursors.Default;
            if (editObj == null)
            {
                return;
            }

            crntTemp = editObj;
            FillLFeatureClasses();
            cmbxFeaClass.SelectedItem = editObj.ClassName;
            if (editObj.TypeName == "ANY" && editObj.CategoryName == "ANY")
            {
                var crnt = cmbxFeaCat.DataSource as List<ComboFilterItem>;
                var any = crnt.FindIndex(x => x.Code == "ANY");
                cmbxFeaCat.SelectedIndex = any;
                var types = cmbxFeaType.DataSource as List<ComboFilterItem>;
                var typeAny = types.FindIndex(x => x.Code == "ANY");

                cmbxFeaType.SelectedIndex = typeAny;

            }
            else FillExisting(editObj);


            cmbxFeaModel.SelectedItem = editObj.ModelName;
            Cursor = Cursors.Default;
        }

        private void FillExisting(TemplateModels editObj)
        {
            if (editObj.CategoryName == "ANY")
            {
                var crnt = cmbxFeaCat.DataSource as List<ComboFilterItem>;
                var any = crnt.FindIndex(x => x.Code == "ANY");
                cmbxFeaCat.SelectedIndex = any;

            }
            else
            {
                var longName = NeApiAccess.Instance.InventoryControl.GetInventoryCategory(editObj.ClassName, editObj.CategoryName);
                if (longName == null)
                {
                    cmbxFeaCat.SelectedItem = editObj.CategoryName;
                }
                else
                {
                    var catCombo = new ComboFilterItem(longName.CategoryDescription, longName.CategoryName);

                    var lisst = cmbxFeaCat.DataSource as List<ComboFilterItem>;
                    var ind = lisst.FindIndex(x => x.Code == catCombo.Code && x.Value == catCombo.Value);
                    cmbxFeaCat.SelectedIndex = ind;
                }
            }




            if (editObj.TypeName == "ANY")
            {

                var types = cmbxFeaType.DataSource as List<ComboFilterItem>;
                var typeAny = types.FindIndex(x => x.Code == "ANY");

                cmbxFeaType.SelectedIndex = typeAny;

            }
            else
            {
                if (editObj.CategoryName == "ANY")
                {

                    var model = NeApiAccess.Instance.InventoryControl.GetInventoryTypes(editObj.ClassName);
                    var typeIndx = model.Find(x => x.TypeName == editObj.TypeName);

                    var catCombo = new ComboFilterItem(typeIndx.InventoryCategory.CategoryDescription, typeIndx.CategoryName);

                    var lisst = cmbxFeaCat.DataSource as List<ComboFilterItem>;
                    var ind = lisst.FindIndex(x => x.Code == catCombo.Code && x.Value == catCombo.Value);
                    cmbxFeaCat.SelectedIndex = ind;
                    editObj.CategoryName = typeIndx.CategoryName;
                }
                var type = NeApiAccess.Instance.InventoryControl.GetInventoryType(editObj.ClassName, editObj.CategoryName, editObj.TypeName);
                if (type == null)
                {
                    var types = cmbxFeaType.DataSource as List<ComboFilterItem>;
                    var typeAny = types.FindIndex(x => x.Code == "ANY");

                    cmbxFeaType.SelectedIndex = typeAny;
                }
                else
                {
                    var typeComb = new ComboFilterItem(type.TypeDescription, type.TypeName);

                    var lisst = cmbxFeaType.DataSource as List<ComboFilterItem>;
                    var ind = lisst.FindIndex(x => x.Code == typeComb.Code && x.Value == typeComb.Value);
                    cmbxFeaType.SelectedIndex = ind;
                }
            }
        }

        private ComboFilterItem GetCategory(string className, string typeName, string modName)
        {
            var neApi = NeApiAccess.Instance;
            List<NeModel> model = null;
            if (typeName == "ANY" && modName == "ANY")
            {
                return null;
            }
            if (typeName != "ANY" && modName != "ANY")
            {
                model = neApi.Models.FindModels("TYPE_NAME='" + typeName + "' AND MODEL_NAME='" + modName + "'");
            }
            else if (typeName != "ANY" && modName == "ANY")
            {
                model = neApi.Models.FindModels("TYPE_NAME='" + typeName + "'");
                if(model.Count==0)
                {
                    var all=neApi.InventoryControl.GetInventoryTypes(className);
                    var found=all.FindAll(x => x.TypeName == typeName);
                    var type = new ComboFilterItem(found[0].InventoryCategory.CategoryDescription, found[0].InventoryCategory.CategoryName);
                    return type;
                }
            }
            else if (typeName == "ANY" && modName != "ANY")
            {
                model = neApi.Models.FindModels("MODEL_NAME='" + modName + "'");
            }
            if (model.Count == 0)
            {
                return null;
            }
            var catName = new ComboFilterItem(model[0].InventoryCategory.CategoryDescription, model[0].InventoryCategory.CategoryName);


            return catName;
        }

        private void FillTemplateNames(string tempName)
        {
            //  cmbxTemplateName.Enabled = false;
            //    cmbxTemplateName.Items.Add(tempName);
            //var allTemplates = TemplateNameDbManager.GetAllTtemplateNames();
            //var allTempNames = new List<String>();
            //foreach (var crntTemp in allTemplates)
            //{
            //    allTempNames.Add(crntTemp.TemplateName);

            //}
            //allTempNames.Sort();
            //cmbxTemplateName.DataSource = allTempNames;
            //cmbxTemplateName.SelectedIndex = -1;
        }

        private void FillLFeatureClasses()
        {
            var classes = new List<string> { "EQUIPMENT", "STRUCTURE", "SPAN", "TRANSMEDIA", "SPLICE_CLOSURE" };
            classes.Sort();
            cmbxFeaClass.DataSource = classes;

        }

        private void FillCategories(object sender, EventArgs e)
        {
            try
            {
                cmbxFeaType.DataSource = null;
                cmbxFeaModel.DataSource = null;


                Cursor = Cursors.WaitCursor;
                var slctdFeaClass = cmbxFeaClass.SelectedItem as string;
                var neApi = NeApiAccess.Instance;
                var catList = new List<ComboFilterItem>();
                var categories = neApi.InventoryControl.GetInventoryCategories(slctdFeaClass);
                foreach (var neInventoryCategory in categories)
                {
                    if (neInventoryCategory.CategoryName.Equals("ANY", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }


                    var combo = new ComboFilterItem(neInventoryCategory.CategoryDescription, neInventoryCategory.CategoryName);
                    if (catList.Contains(combo))
                    {
                        continue;
                    }

                    catList.Add(combo);
                }

                var ordered = catList.OrderBy(x => x.Code);

                var newList = ordered.ToList();
                if (slctdFeaClass != null)
                    newList.Insert(0, new ComboFilterItem("ANY", "ANY"));
                cmbxFeaCat.DisplayMember = "Code";
                cmbxFeaCat.DataSource = newList;

                Cursor = Cursors.Default;
                cmbxFeaCat.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Getting Categories : " + ex.Message);
            }
        }

        private void FillTypeName(object sender, EventArgs e)
        {
            try
            {
                cmbxFeaModel.DataSource = null;
                cmbxFeaType.DataSource = null;
                var slctdClass = cmbxFeaClass.SelectedItem as string;
                var selectedCat = cmbxFeaCat.SelectedItem as ComboFilterItem;
                if (selectedCat == null)
                {
                    cmbxFeaType.DataSource = null;
                    return;
                }
                if (string.IsNullOrEmpty(selectedCat.Code))
                {
                    cmbxFeaType.DataSource = null;
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var neApi = NeApiAccess.Instance;
                var typeList = new List<ComboFilterItem>();

                var types = neApi.InventoryControl.GetInventoryTypes(slctdClass, selectedCat.Value);



                foreach (var neInventoryTypes in types)
                {
                    if (neInventoryTypes.TypeName.Equals("ANY", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    var filterItem = new ComboFilterItem(neInventoryTypes.TypeDescription, neInventoryTypes.TypeName);

                    if (typeList.Contains(filterItem))
                    {
                        continue;
                    }
                    var index = typeList.FindIndex(x => x.Code == filterItem.Code && x.Value == filterItem.Value);
                    if (index != -1)
                    {
                        continue;
                    }
                    typeList.Add(filterItem);
                }



                var ordered = typeList.OrderBy(x => x.Value);

                var newList = ordered.ToList();
                if (slctdClass != null)
                    newList.Insert(0, new ComboFilterItem("ANY", "ANY"));
                cmbxFeaType.DataSource = newList;
                cmbxFeaType.DisplayMember = "Code";
                cmbxFeaType.SelectedIndex = -1;

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Getting Types : " + ex.Message);
            }

        }

        private void FillModels(object sender, EventArgs e)
        {
            try
            {
                cmbxFeaModel.DataSource = null;
                var slctdCls = cmbxFeaClass.SelectedItem as string;
                var selectedCategory = cmbxFeaCat.SelectedItem as ComboFilterItem;
                var selectedType = cmbxFeaType.SelectedItem as ComboFilterItem;
                if (selectedCategory == null || selectedType == null)
                {
                    return;
                }


                Cursor = Cursors.WaitCursor;
                var neApi = NeApiAccess.Instance;
                var modelList = new List<string>();
                //check whether this should turn into a combofilter list
                var models = neApi.Models.GetModels(slctdCls, selectedCategory.Value.Equals("any", StringComparison.InvariantCultureIgnoreCase)
                        ? null
                        : selectedCategory.Value,
                    selectedType.Value.Equals("any", StringComparison.InvariantCultureIgnoreCase)
                        ? null
                        : selectedType.Value,
                    true);

                foreach (var model in models)
                {
                    if (model.ModelName.Equals("ANY", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    var filterItem = model.ModelName;

                    if (modelList.Contains(filterItem))
                    {
                        continue;
                    }

                    modelList.Add(filterItem);
                }


                modelList.Sort();


                modelList.Insert(0, "ANY");
                cmbxFeaModel.DataSource = modelList;
                cmbxFeaModel.DisplayMember = "Value";
                cmbxFeaModel.SelectedIndex = -1;

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Getting Models : " + ex.Message);
            }
        }

        private void Ok(object sender, EventArgs e)
        {

            var catName = cmbxFeaCat.SelectedItem as ComboFilterItem;
            var featureClass = cmbxFeaClass.SelectedItem as string;

            var typeName = cmbxFeaType.SelectedItem as ComboFilterItem;
            var modName = cmbxFeaModel.SelectedItem as string;


            if (!ValidateInput())
            {
                return;
            }


            if (crntTemp != null)
            {
                if (catName.Value == "ANY")
                {
                    var category = GetCategory(featureClass, typeName.Value, modName);
                    if (category == null)
                    {
                        catName = cmbxFeaCat.SelectedItem as ComboFilterItem;
                    }
                    else
                    {
                        catName = category;
                    }

                }
                else
                {
                    catName = cmbxFeaCat.SelectedItem as ComboFilterItem;
                }
                crntTemp.CategoryName = catName.Value;
                crntTemp.ModelName = modName;
                crntTemp.TypeName = typeName.Value;
                crntTemp.ClassName = featureClass;
                crntTemp.TemplateName = TemplateName;

                if (CheckIfAlreadyExists(crntTemp, true))
                {
                    return;
                }

            }
            else
            {
                if (catName.Value == "ANY")
                {
                    var category = GetCategory(featureClass, typeName.Value, modName);
                    if (category == null)
                    {
                        catName = cmbxFeaCat.SelectedItem as ComboFilterItem;
                    }
                    else catName = category;
                }

                var newAtts = new TemplateModels(TemplateName, featureClass, typeName.Value, catName.Value, modName);
                if (CheckIfAlreadyExists(newAtts, false))
                {
                    return;
                }
                if (crntTemp == null)
                {
                    crntTemp = new TemplateModels();
                }
                crntTemp = newAtts;

            }
            Cursor = Cursors.Default;
            DialogResult = DialogResult.OK;
        }


        private bool CheckIfAlreadyExists(TemplateModels disp, bool edit)
        {
            if (disp == null)
            {
                return true;
            }
            if (AllValues == null)
            {
                return false;
            }
            foreach (var row in AllValues.DataSource)
            {
                var temp = row as TemplateModels;
                var clsName = temp.ClassName;
                var tempName = temp.TemplateName;
                var cat = temp.CategoryName;
                var type = temp.TypeName;
                var model = temp.ModelName;
                if (edit)
                {
                    if (disp.ObjectId != temp.ObjectId &&
                       clsName.Equals(disp.ClassName) &&
                       tempName.Equals(disp.TemplateName) && cat.Equals(disp.CategoryName) && type.Equals(disp.TypeName) && model.Equals(disp.ModelName))
                    {

                        NeApiAccess.Instance.Logger.ShowErrorDialog(
                                    "The requested row already exists, No duplications are allowed and therefore the row wont be added.");
                        return true;
                    }
                }
                else if (clsName.Equals(disp.ClassName) && tempName.Equals(disp.TemplateName) && model.Equals(disp.ModelName))
                {

                    var categoryComb = GetComboFilterItem(disp.ClassName, disp.CategoryName);
                    var typeComb = GetComboFilterItem(disp.ClassName, disp.CategoryName, disp.TypeName);
                    if ((categoryComb.Value == cat || categoryComb.Code == cat) && (typeComb.Value == type || typeComb.Code == type))

                    {
                        NeApiAccess.Instance.Logger.ShowErrorDialog(
                       "The requested row already exists, No duplications are allowed and therefore the row wont be added.");

                        return true;
                    }

                }
            }
            return false;
        }

        private ComboFilterItem GetComboFilterItem(string className, string catName, string type = null)
        {
            var returnedObj = new ComboFilterItem();
            if (type == null)
            {
                var longName = NeApiAccess.Instance.InventoryControl.GetInventoryCategory(className, catName);
                if (longName == null)
                {
                    returnedObj = null;
                }
                else
                {
                    var catCombo = new ComboFilterItem(longName.CategoryDescription, longName.CategoryName);
                    return catCombo;
                }
            }
            else
            {
                var typeName = NeApiAccess.Instance.InventoryControl.GetInventoryType(className, catName, type);
                if (type == null)
                {
                    returnedObj = null;
                }
                else
                {
                    var typeComb = new ComboFilterItem(typeName.TypeDescription, typeName.TypeName);
                    return typeComb;

                }
            }

            return returnedObj;
        }

        private bool ValidateInput()
        {


            var feaCls = cmbxFeaClass.SelectedItem;
            if (feaCls == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you select a Feature Class.");
                cmbxFeaClass.Focus();
                return false;
            }

            var category = cmbxFeaCat.SelectedItem;
            if (category == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you select a Category.");
                cmbxFeaCat.Focus();
                return false;
            }

            var typeName = cmbxFeaType.SelectedItem;
            if (typeName == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you select a type.");
                cmbxFeaType.Focus();
                return false;
            }

            var modName = cmbxFeaModel.SelectedItem;
            if (modName == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(
                    "Please make sure you select a model.");
                cmbxFeaModel.Focus();
                return false;
            }

            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            Close();
        }
    }
}
