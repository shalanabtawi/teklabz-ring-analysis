﻿using Equin.ApplicationFramework;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.Admin.TemplateModelConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.UI
{
    public partial class TemplateDescriptionConfiguration : Form
    {
        public TemplateDescriptionConfiguration()
        {
            InitializeComponent();
            LoadData();
        }
        private void LoadData()
        {
            try
            {

                dgvTemplateName.AutoGenerateColumns = false;
                var configs = TemplateNameDbManager.GetAllTtemplateNames();
                dgvTemplateName.DataSource = new BindingListView<TemplateNames>(configs);
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error while trying to load data", ex);
            }
        }
        private void Add(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

 
                var bindingListView = dgvTemplateName.DataSource as BindingListView<TemplateNames> ??
                                  new BindingListView<TemplateNames>(new List<TemplateNames>());
                var openForm = new AddEditTemplateDescription() { AllValues = bindingListView };
                var returnDialog = openForm.ShowDialog();
                if (returnDialog != DialogResult.OK)
                {
                    Cursor = Cursors.Default;
                    return;
                }
                Cursor = Cursors.Default;
                var added = openForm.crntTemp;
                openForm.AllValues.DataSource.Add(added);
                var updatedList = new BindingListView<TemplateNames>(openForm.AllValues.DataSource);

                dgvTemplateName.DataSource = updatedList;
                dgvTemplateName.Refresh();

                SelectRow(added);
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Adding New Record ", ex);
                NeApiAccess.Instance.Logger.LogError("Error While Adding New Record ", ex);
            }
        }
        private void SelectRow(TemplateNames obj)
        {
            dgvTemplateName.ClearSelection();

            foreach (DataGridViewRow gvRow in dgvTemplateName.Rows)
            {
                var src = gvRow.DataBoundItem as ObjectView<TemplateNames>;
                if (src == null)
                {
                    continue;
                }

                if (string.Equals(src.Object.ObjectId, obj.ObjectId))
                {
                    dgvTemplateName.CurrentCell = dgvTemplateName.Rows[gvRow.Index].Cells[0];
                    dgvTemplateName.Rows[gvRow.Index].Selected = true;
                    break;
                }
            }
        }

        private void Edit(object sender, EventArgs e)
        {
            UpdateSelected();

        }

        private void UpdateSelected()
        {
            try
            {
                if (dgvTemplateName.DataSource == null || dgvTemplateName.SelectedRows.Count == 0)
                {
                    NeApiAccess.Instance.Logger.ShowErrorDialog("No row was selected");
                    return;
                }

                var bindingList = dgvTemplateName.DataSource as BindingListView<TemplateNames> ??
                      new BindingListView<TemplateNames>(new List<TemplateNames>());
                var needsEditing = (ObjectView<TemplateNames>)dgvTemplateName.SelectedRows[0].DataBoundItem;

                if (needsEditing == null)
                {
                    return;
                }
                var crntModel = needsEditing.Object;
                Cursor = Cursors.WaitCursor;
                var openForm = new AddEditTemplateDescription(needsEditing.Object) { AllValues = bindingList };

                Cursor = Cursors.Default;
                var returnDialog = openForm.ShowDialog();
                if (returnDialog != DialogResult.OK)
                {
                    return;
                }

                var newAtt = openForm.crntTemp;
                if (newAtt != null)
                {
                    newAtt.ObjectId = crntModel.ObjectId;
                }

                var edittedIndex = bindingList.DataSource.IndexOf(crntModel);
                bindingList.DataSource[edittedIndex] = newAtt;
                var updatedList = new BindingListView<TemplateNames>(openForm.AllValues.DataSource);


                dgvTemplateName.DataSource = updatedList;
                dgvTemplateName.Refresh();

                SelectRow(crntModel);
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Updating Selected Record ", ex);
                NeApiAccess.Instance.Logger.LogError("Error While Updating Selected Record ", ex);
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            try
            {
                if (dgvTemplateName.SelectedRows.Count <= 0)
                {
                    NeApiAccess.Instance.Logger.ShowErrorDialog("No row was selected");
                    return;
                }

                var dialogResult = MessageBox.Show(@"Are you sure you want to delete the selected item?",
                    @"Delete Selected Template Description Configuration", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                var attsObj = (ObjectView<TemplateNames>)dgvTemplateName.SelectedRows[0].DataBoundItem;


                var existing = TemplateModelDbManager.FindTemplateModels(TemplateModelDbManager.tempName + "='" + attsObj.Object.TemplateName + "'");

                foreach (var single in existing)
                {
                    TemplateModelDbManager.Delete(single.ObjectId);
                }



                var bindingList = (BindingListView<TemplateNames>)dgvTemplateName.DataSource;

                if (bindingList != null)
                {
                    bindingList.DataSource.Remove(attsObj.Object);
                    var updatedList = new BindingListView<TemplateNames>(bindingList.DataSource);
                    TemplateNameDbManager.Delete(attsObj.Object.ObjectId);
                    dgvTemplateName.DataSource = updatedList;
                }
                dgvTemplateName.Refresh();
            }

            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("Error While Deleting Selected Record ", ex);
                NeApiAccess.Instance.Logger.LogError("Error While Deleting Selected Record ", ex);
            }

        }

        private bool CheckIfUsed(TemplateNames tempName)
        {
            var existing = TemplateModelDbManager.FindTemplateModels(TemplateModelDbManager.tempName + "='" + tempName + "'");
            if (existing.Count > 0)
            {

                return true;
            }


            return false;
        }

        private void dgvTemplateName_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            UpdateSelected();
        }
    }
}
