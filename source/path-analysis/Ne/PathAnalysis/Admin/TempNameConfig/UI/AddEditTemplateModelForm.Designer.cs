﻿namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig
{
    partial class AddEditTemplateModelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditTemplateModelForm));
            this.label1 = new System.Windows.Forms.Label();
            this.Category = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbxFeaClass = new System.Windows.Forms.ComboBox();
            this.cmbxFeaCat = new System.Windows.Forms.ComboBox();
            this.cmbxFeaType = new System.Windows.Forms.ComboBox();
            this.cmbxFeaModel = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class Name";
            // 
            // Category
            // 
            this.Category.AutoSize = true;
            this.Category.Location = new System.Drawing.Point(12, 41);
            this.Category.Name = "Category";
            this.Category.Size = new System.Drawing.Size(80, 13);
            this.Category.TabIndex = 1;
            this.Category.Text = "Category Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Type Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Model Name";
            // 
            // cmbxFeaClass
            // 
            this.cmbxFeaClass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxFeaClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFeaClass.FormattingEnabled = true;
            this.cmbxFeaClass.Location = new System.Drawing.Point(103, 6);
            this.cmbxFeaClass.Name = "cmbxFeaClass";
            this.cmbxFeaClass.Size = new System.Drawing.Size(344, 21);
            this.cmbxFeaClass.TabIndex = 1;
            this.cmbxFeaClass.SelectedIndexChanged += new System.EventHandler(this.FillCategories);
            // 
            // cmbxFeaCat
            // 
            this.cmbxFeaCat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxFeaCat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFeaCat.FormattingEnabled = true;
            this.cmbxFeaCat.Location = new System.Drawing.Point(103, 38);
            this.cmbxFeaCat.Name = "cmbxFeaCat";
            this.cmbxFeaCat.Size = new System.Drawing.Size(344, 21);
            this.cmbxFeaCat.TabIndex = 2;
            this.cmbxFeaCat.SelectedIndexChanged += new System.EventHandler(this.FillTypeName);
            // 
            // cmbxFeaType
            // 
            this.cmbxFeaType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxFeaType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFeaType.FormattingEnabled = true;
            this.cmbxFeaType.Location = new System.Drawing.Point(103, 69);
            this.cmbxFeaType.Name = "cmbxFeaType";
            this.cmbxFeaType.Size = new System.Drawing.Size(344, 21);
            this.cmbxFeaType.TabIndex = 3;
            this.cmbxFeaType.SelectedIndexChanged += new System.EventHandler(this.FillModels);
            // 
            // cmbxFeaModel
            // 
            this.cmbxFeaModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxFeaModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFeaModel.FormattingEnabled = true;
            this.cmbxFeaModel.Location = new System.Drawing.Point(103, 100);
            this.cmbxFeaModel.Name = "cmbxFeaModel";
            this.cmbxFeaModel.Size = new System.Drawing.Size(344, 21);
            this.cmbxFeaModel.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(372, 130);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(291, 130);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.Ok);
            // 
            // AddEditTemplateModelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 165);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cmbxFeaModel);
            this.Controls.Add(this.cmbxFeaType);
            this.Controls.Add(this.cmbxFeaCat);
            this.Controls.Add(this.cmbxFeaClass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Category);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(475, 204);
            this.Name = "AddEditTemplateModelForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add/Edit Template Model Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Category;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbxFeaClass;
        private System.Windows.Forms.ComboBox cmbxFeaCat;
        private System.Windows.Forms.ComboBox cmbxFeaType;
        private System.Windows.Forms.ComboBox cmbxFeaModel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
    }
}