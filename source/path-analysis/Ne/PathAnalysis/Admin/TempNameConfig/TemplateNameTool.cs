﻿using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.ArcMapUI;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.UI;
using Teklabz.Ne.PathAnalysis.Common;

namespace Teklabz.Ne.PathAnalysis.Admin.TempNameConfig {
    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
    public class TemplateNameTool : BaseCommand {

        public const string ProgId = "NETB.PA.TemplateConfig";
        public const string Guid = "ef53b6d7-12e5-463d-8d16-8a57a1204269";

        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType) {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType) {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);
        }

        #endregion

        #endregion

        public TemplateNameTool() {
            m_category = "Path Analysis Admin Tools";
            m_caption = "Templates Configuration Tool";
            m_message = "Templates Configuration Tool";
            m_toolTip = "Templates Configuration Tool";
            m_name = "Templates Configuration Tool";
            try {
                var bitmapResourceName = GetType().Name + ".bmp";
                m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            } catch (Exception ex) {
                Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        public override void OnClick() {
            if (!TemplateNameDbManager.CheckTable()) {
                return;
            }

            if (!SecurityManager.CanPerformTemplateName()) {
                return;
            }

            var open = new TemplateDescriptionConfiguration();
            open.ShowDialog();
        }

        public override void OnCreate(object hook) {
            if (hook == null)
                return;
            if (hook is IMxApplication)
                m_enabled = true;
            else
                m_enabled = false;
        }

    }
}
