﻿using ESRI.ArcGIS.Geodatabase;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.DataObjects
{
    public class AppSettingsDbManager
    {
        public const string TableName = "TK_PA_APP_SETTINGS";
        public const string PathDis = "TELECOM_PATH_DISTANCE";
        public const string Spof = "TELECOM_SPOF";
        public const string MaxSegments = "TELECOM_TRANS_NUMBER";
        public const string MaxNumRoutes = "TELECOM_ROUTE_NUMBER";
        public const string CivilPathDis = "CIVIL_PATH_DISTANCE";
        public const string CivilSpof = "CIVIL_SPOF";
        public const string CivilSegs = "CIVIL_SPAN_NUMBER";
        public const string CivilRoutes = "CIVIL_ROUTE_NUMBER";
        public const string ObjectId = "OBJECTID";
        public static readonly List<string> TemplateModelList = new List<string> {
            ObjectId,
            PathDis,
            Spof,
            MaxSegments,
            MaxNumRoutes,
            CivilPathDis,
            CivilSpof,
            CivilSegs,
            CivilRoutes
        };

        public static bool CheckTable()
        {
            var findTbl = NeApiAccess.Instance.Database.GetNeTable(TableName);
            if (findTbl == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(string.Format("Table {0} does not exist.", TableName));
                return false;
            }
            return true;
        }

        private static string ValidateFields(ITable table, List<string> tableList)
        {
            var message = string.Empty;
            try
            {
                foreach (var field in tableList)
                {
                    var indx = table.FindField(field);
                    if (indx == -1)
                    {
                        message += (string.IsNullOrEmpty(message) ? "The Fields : " : ",") + field;
                    }
                }
                if (!string.IsNullOrEmpty(message))
                {
                    message += " Was Not Found ";
                }
            }
            catch (Exception ex)
            {
                message += " " + ex;
            }
            return message;
        }

        private static void FillRowBuffer(IRowBuffer roBuffer, ITable table, AppSettings settings)
        {
            roBuffer.Value[table.FindField(PathDis)] = settings.TelecomDistanceToPath;
            roBuffer.Value[table.FindField(Spof)] = settings.TelecomSinglePointOfFailure;
            roBuffer.Value[table.FindField(MaxSegments)] = settings.TelecomMaxNoOfSegments;
            roBuffer.Value[table.FindField(MaxNumRoutes)] = settings.TelecomMaxNoOfRoutes;
            roBuffer.Value[table.FindField(CivilPathDis)] = settings.CivilDistanceToPath;
            roBuffer.Value[table.FindField(CivilRoutes)] = settings.CivilMaxNoOfRoutes;
            roBuffer.Value[table.FindField(CivilSegs)] = settings.CivilMaxNoOfSegments;
            roBuffer.Value[table.FindField(CivilSpof)] = settings.CivilSinglePointOfFailure;
            

        }
        public static AppSettings GetAppSettings()
        {
            var list = FindAppSettings(null);
            return list;
        }
        public static AppSettings FindAppSettings(int id)
        {
            var singleApp = FindAppSettings(ObjectId + " = " + id);
            return singleApp;
        }

        public static AppSettings FindAppSettings(string query)
        {
            var neApi = NeApiAccess.Instance;
            var applicationSets = new AppSettings();
            const string tableName = TableName;
            try
            {
                var objects = neApi.Database.FindNeObjects(tableName, query);
                if (objects.Count == 0)
                {
                    return null;
                }
                var model = new AppSettings();
                if (!objects[0].Fields[ObjectId].IsValueNull())
                {
                    model.ObjectId = objects[0].Fields[ObjectId].GetValue<int>();
                }

                if (!objects[0].Fields[PathDis].IsValueNull())
                {
                    model.TelecomDistanceToPath = objects[0].Fields[PathDis].GetValue<double>();
                }

                if (!objects[0].Fields[Spof].IsValueNull())
                {
                    model.TelecomSinglePointOfFailure = objects[0].Fields[Spof].GetValue<double>();
                }

                if (!objects[0].Fields[MaxSegments].IsValueNull())
                {
                    model.TelecomMaxNoOfSegments = objects[0].Fields[MaxSegments].GetValue<int>();
                }
                if (!objects[0].Fields[MaxNumRoutes].IsValueNull())
                {
                    model.TelecomMaxNoOfRoutes = objects[0].Fields[MaxNumRoutes].GetValue<int>();
                }
                if (!objects[0].Fields[CivilSpof].IsValueNull())
                {
                    model.CivilSinglePointOfFailure = objects[0].Fields[CivilSpof].GetValue<double>();
                }

                if (!objects[0].Fields[CivilSegs].IsValueNull())
                {
                    model.CivilMaxNoOfSegments = objects[0].Fields[CivilSegs].GetValue<int>();
                }

                if (!objects[0].Fields[CivilRoutes].IsValueNull())
                {
                    model.CivilMaxNoOfRoutes = objects[0].Fields[CivilRoutes].GetValue<int>();
                }
                if (!objects[0].Fields[CivilPathDis].IsValueNull())
                {
                    model.CivilDistanceToPath = objects[0].Fields[CivilPathDis].GetValue<double>();
                }



                applicationSets = model;
            }
            catch (Exception ex)
            {
                neApi.Logger.LogError(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
                throw new Exception(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
            }
            return applicationSets;
        }
        public static bool Add(AppSettings model)
        {
            var neApi = NeApiAccess.Instance;
            ICursor crsr = null;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                throw new Exception(string.Format("Table {0} was not found in the database", TableName));
            }
            try
            {
                transaction.StartTransaction();
                string message = ValidateFields(table, TemplateModelList);
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
                var roBuffer = table.CreateRowBuffer();
                crsr = table.Insert(true);
                FillRowBuffer(roBuffer, table, model);
                var oid = crsr.InsertRow(roBuffer);
                crsr.Flush();
                model.ObjectId = (int)oid;
                transaction.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Adding  " + TableName + " Values ", ex);
                return false;
            }
            finally
            {
                if (crsr != null)
                {
                    Marshal.ReleaseComObject(crsr);
                }
            }
        }
        public static bool Update(AppSettings model)
        {
            var neApi = NeApiAccess.Instance;
            ICursor crsr = null;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                return false;
            }
            try
            {
                transaction.StartTransaction();
                string message = ValidateFields(table, TemplateModelList);
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
                var queryFilter = new QueryFilter();
                queryFilter.WhereClause = "OBJECTID=" + model.ObjectId;
                crsr = table.Search(queryFilter, false);
                var row = crsr.NextRow();
                FillRowBuffer(row, table, model);
                row.Store();
                transaction.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Updating  " + TableName + " Values ", ex);
                return false;
            }
            finally
            {
                if (crsr != null)
                {
                    Marshal.ReleaseComObject(crsr);
                }
            }
        }

    }
}
