﻿namespace Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.DataObjects
{
    public class AppSettings
    {
        public double TelecomDistanceToPath { set; get; }
        public double TelecomSinglePointOfFailure { set; get; }
        public int TelecomMaxNoOfSegments { set; get; }
        public int TelecomMaxNoOfRoutes { set; get; }
        public double CivilDistanceToPath { set; get; }
        public double CivilSinglePointOfFailure { set; get; }
        public int CivilMaxNoOfSegments { set; get; }
        public int CivilMaxNoOfRoutes { set; get; }
        public int ObjectId { set; get; }

        public AppSettings(double distanceToPath, double singlePointOfFailure, int maxNoOfSegments, double civilDistanceToPath, double civilSinglePointOfFailure, int civilMaxNoOfSegments, int civilMaxNoOfRoutes)
        {
            TelecomDistanceToPath = distanceToPath;
            TelecomSinglePointOfFailure = singlePointOfFailure;
            TelecomMaxNoOfSegments = maxNoOfSegments;
            CivilDistanceToPath = civilDistanceToPath;
            CivilSinglePointOfFailure = civilSinglePointOfFailure;
            CivilMaxNoOfSegments = civilMaxNoOfSegments;
            CivilMaxNoOfRoutes = civilMaxNoOfRoutes;
        }

        public AppSettings()
        {
        }
    }
}
