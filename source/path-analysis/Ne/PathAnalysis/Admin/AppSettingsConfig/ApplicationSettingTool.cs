﻿using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.ArcMapUI;
using Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.UI;
using SecurityManager = Teklabz.Ne.PathAnalysis.Common.SecurityManager;

namespace Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig {

    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
    public class ApplicationSettingTool : BaseCommand {

        public const string ProgId = "NETB.PA.AppSet";
        private const string Guid = "b19ae012-5d08-4c97-a739-7fe126326cf5";

        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType) {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType) {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);
        }

        #endregion

        #endregion

        public ApplicationSettingTool() {
            m_category = "Path Analysis Admin Tools";
            m_caption = "Application Settings Configuration";
            m_message = "Application Settings Configuration";
            m_toolTip = "Application Settings Configuration";
            m_name = "Application Settings Configuration";
            try {
                var bitmapResourceName = GetType().Name + ".bmp";
                m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            } catch (Exception ex) {
                Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        public override void OnClick() {
            if (!AppSettingsDbManager.CheckTable()) {
                return;
            }

            if (!SecurityManager.CanPerformAppSettings()) {
                return;
            }

            var open = new ApplicationSettingsMainForm();
            open.ShowDialog();
        }

        public override void OnCreate(object hook) {
            if (hook == null)
                return;
            if (hook is IMxApplication)
                m_enabled = true;
            else
                m_enabled = false;
        }

    }
}
