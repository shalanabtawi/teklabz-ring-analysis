﻿using System;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.UI
{
    public partial class ApplicationSettingsMainForm : Form
    {
        public ApplicationSettingsMainForm()
        {
            InitializeComponent();
            var existingConfig = CheckIfExisting();
            if (existingConfig == null)
            {
                return;
            }
            txtbxDistance.Text = existingConfig.TelecomDistanceToPath.ToString();
            txtbxSpof.Text = existingConfig.TelecomSinglePointOfFailure.ToString();
            txtbxMaxRoutes.Text = existingConfig.TelecomMaxNoOfRoutes.ToString();
            txtbxMaxSegments.Text = existingConfig.TelecomMaxNoOfSegments.ToString();

            //place values for civil group box
            txtBxCiviRatio.Text = existingConfig.CivilDistanceToPath.ToString();
            txtBbxCivilRoutes.Text = existingConfig.CivilMaxNoOfRoutes.ToString();
            txtBxCivilSpof.Text = existingConfig.CivilSinglePointOfFailure.ToString();
            txtBxCivilSegments.Text = existingConfig.CivilMaxNoOfSegments.ToString();

        }

        private AppSettings CheckIfExisting()
        {
            var appSettingsFound = AppSettingsDbManager.GetAppSettings();
            if (appSettingsFound == null)
            {
                return null;
            }

            if (appSettingsFound == null)
            {
                return null;
            }

            return appSettingsFound;
        }

        private void Save(object sender, EventArgs e)
        {
            var distance = txtbxDistance.Text;
            var spof = txtbxSpof.Text;
            var maxSegments = txtbxMaxSegments.Text;
            var maxRoutes = txtbxMaxRoutes.Text;
            var civilDistance = txtBxCiviRatio.Text;
            var civilSpof = txtBxCivilSpof.Text;
            var civilRoutes = txtBbxCivilRoutes.Text;
            var civilsegs = txtBxCivilSegments.Text;

            var trimmedDis=distance.Trim('.');
            var trimSpof= spof.Trim('.');
            var trimMaxSeg= maxSegments.Trim('.');
            var trimMaxRoutes= maxRoutes.Trim('.');
            var trimCivilDis= civilDistance.Trim('.');
            var trimCivilSpof=civilSpof.Trim('.');
            var trimCivilRoutes= civilRoutes.Trim('.');
            var trimCivlSeg= civilsegs.Trim('.');
          
            if (!ValidateInput())
            {
                return;
            }
            var existingConfig = CheckIfExisting();
            var distanceNum = double.Parse(trimmedDis);
            var spofNum = double.Parse(trimSpof);
            var routesNum = int.Parse(trimMaxRoutes);
            var maxSegNumber = int.Parse(trimMaxSeg);

            var civilDistanceNum = double.Parse(trimCivilDis);
            var civilSpofNum = double.Parse(trimCivilSpof);
            var civilRoutesNum = int.Parse(trimCivilRoutes);
            var maxCivilSegNumber = int.Parse(trimCivlSeg);

            var newAppSettings = new AppSettings(distanceNum, spofNum, maxSegNumber, civilDistanceNum, civilSpofNum, maxCivilSegNumber, civilRoutesNum);
            newAppSettings.TelecomMaxNoOfRoutes = routesNum;
            if (existingConfig == null)
            {
                AppSettingsDbManager.Add(newAppSettings);
            }
            else
            {
                existingConfig.TelecomDistanceToPath = distanceNum;
                existingConfig.TelecomSinglePointOfFailure = spofNum;
                existingConfig.TelecomMaxNoOfSegments = maxSegNumber;
                existingConfig.TelecomMaxNoOfRoutes = routesNum;
                existingConfig.CivilDistanceToPath = civilDistanceNum;
                existingConfig.CivilMaxNoOfRoutes = civilRoutesNum;
                existingConfig.CivilMaxNoOfSegments = maxCivilSegNumber;
                existingConfig.CivilSinglePointOfFailure = civilSpofNum;

                AppSettingsDbManager.Update(existingConfig);
            }
            NeApiAccess.Instance.Logger.ShowMessageDialog("Saved!");
        }

        private bool ValidateInput()
        {
            var distance = txtbxDistance.Text;
            var spof = txtbxSpof.Text;
            var routes = txtbxMaxRoutes.Text;
            var civilDistance = txtBxCiviRatio.Text;
            var civilSpof = txtBxCivilSpof.Text;
            var civilRoutes = txtBbxCivilRoutes.Text;
            var maxCivil = txtBxCivilSegments.Text;
            var maxTele = txtbxMaxSegments.Text;
            if (string.IsNullOrEmpty(distance) || string.IsNullOrWhiteSpace(distance.Trim('0')))
            {
                txtbxDistance.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a distance value for the Telecom Network");
                return false;

            }
          
            if (string.IsNullOrEmpty(spof) || string.IsNullOrWhiteSpace(spof.Trim('0')))
            {
                txtbxSpof.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a single point of failure value for the Telecom Network");
                return false;

            }
            if (string.IsNullOrEmpty(maxTele) || string.IsNullOrWhiteSpace(maxTele.Trim('0')))
            {
                txtbxMaxSegments.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter the Maximum number of Transmedia value");
                return false;

            }
            if (string.IsNullOrEmpty(routes) || string.IsNullOrWhiteSpace(routes.Trim('0')))
            {
                txtbxMaxRoutes.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter the Maximum number of routes value for the Telecom Network");
                return false;

            }
            if (string.IsNullOrEmpty(civilDistance) || string.IsNullOrWhiteSpace(civilDistance.Trim('0')))
            {
                txtBxCiviRatio.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a distance value for the Civil Network");
                return false;

            }

            if (string.IsNullOrEmpty(civilSpof) || string.IsNullOrWhiteSpace(civilSpof.Trim('0')))
            {
                txtBxCivilSpof.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter a single point of failure value for the Civil Network");
                return false;

            }
            if (string.IsNullOrEmpty(maxCivil) || string.IsNullOrWhiteSpace(maxCivil.Trim('0')))
            {
                txtBxCivilSegments.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter the Maximum number of Spans value");
                return false;

            }
            if (string.IsNullOrEmpty(civilRoutes) || string.IsNullOrWhiteSpace(civilRoutes.Trim('0')))
            {
                txtBbxCivilRoutes.Focus();
                NeApiAccess.Instance.Logger.ShowErrorDialog("Please enter the Maximum number of routes value for the Civil Network");
                return false;

            }


            return true;
        }

        private void txtbxDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.' && txtbxDistance.Text != null)
            {
                var existing = txtbxDistance.Text;
                if (existing.Contains("."))
                {
                    e.Handled = true;
                }
            }
            
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtbxSpof_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.' && txtbxSpof.Text != null)
            {
                var existing = txtbxSpof.Text;
                if (existing.Contains("."))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtbxMaxSegments_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back )
            {
                e.Handled = true;
            }
        }

        private void txtbxMaxRoutes_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back )
            {
                e.Handled = true;
            }
        }

        private void txtBxCiviRatio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.' && txtBxCiviRatio.Text != null)
            {
                var existing = txtBxCiviRatio.Text;
                if (existing.Contains("."))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtBxCivilSpof_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.' && txtBxCivilSpof.Text != null)
            {
                var existing = txtBxCivilSpof.Text;
                if (existing.Contains("."))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtBxCivilSegments_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtBbxCivilRoutes_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back )
            {
                e.Handled = true;
            }
        }
    }
}
