﻿namespace Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.UI
{
    partial class ApplicationSettingsMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationSettingsMainForm));
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtbxMaxRoutes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbxDistance = new System.Windows.Forms.TextBox();
            this.txtbxSpof = new System.Windows.Forms.TextBox();
            this.txtbxMaxSegments = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtBbxCivilRoutes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBxCiviRatio = new System.Windows.Forms.TextBox();
            this.txtBxCivilSpof = new System.Windows.Forms.TextBox();
            this.txtBxCivilSegments = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(517, 345);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.Save);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtbxMaxRoutes);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtbxDistance);
            this.groupBox1.Controls.Add(this.txtbxSpof);
            this.groupBox1.Controls.Add(this.txtbxMaxSegments);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(580, 158);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Telecom Configurations";
            // 
            // txtbxMaxRoutes
            // 
            this.txtbxMaxRoutes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbxMaxRoutes.Location = new System.Drawing.Point(186, 125);
            this.txtbxMaxRoutes.MaxLength = 7;
            this.txtbxMaxRoutes.Name = "txtbxMaxRoutes";
            this.txtbxMaxRoutes.Size = new System.Drawing.Size(388, 20);
            this.txtbxMaxRoutes.TabIndex = 12;
            this.txtbxMaxRoutes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbxMaxRoutes_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Maximum Number of Routes:";
            // 
            // txtbxDistance
            // 
            this.txtbxDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbxDistance.Location = new System.Drawing.Point(186, 23);
            this.txtbxDistance.MaxLength = 7;
            this.txtbxDistance.Name = "txtbxDistance";
            this.txtbxDistance.Size = new System.Drawing.Size(388, 20);
            this.txtbxDistance.TabIndex = 6;
            this.txtbxDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbxDistance_KeyPress);
            // 
            // txtbxSpof
            // 
            this.txtbxSpof.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbxSpof.Location = new System.Drawing.Point(186, 57);
            this.txtbxSpof.MaxLength = 7;
            this.txtbxSpof.Name = "txtbxSpof";
            this.txtbxSpof.Size = new System.Drawing.Size(388, 20);
            this.txtbxSpof.TabIndex = 8;
            this.txtbxSpof.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbxSpof_KeyPress);
            // 
            // txtbxMaxSegments
            // 
            this.txtbxMaxSegments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbxMaxSegments.Location = new System.Drawing.Point(186, 93);
            this.txtbxMaxSegments.MaxLength = 7;
            this.txtbxMaxSegments.Name = "txtbxMaxSegments";
            this.txtbxMaxSegments.Size = new System.Drawing.Size(388, 20);
            this.txtbxMaxSegments.TabIndex = 10;
            this.txtbxMaxSegments.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbxMaxSegments_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Maximum Number of Transmedias:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Single Point of Failure Buffers (m):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Distance to Path Ratio:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtBbxCivilRoutes);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtBxCiviRatio);
            this.groupBox2.Controls.Add(this.txtBxCivilSpof);
            this.groupBox2.Controls.Add(this.txtBxCivilSegments);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 176);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(580, 159);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Civil Configurations";
            // 
            // txtBbxCivilRoutes
            // 
            this.txtBbxCivilRoutes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBbxCivilRoutes.Location = new System.Drawing.Point(186, 125);
            this.txtBbxCivilRoutes.MaxLength = 7;
            this.txtBbxCivilRoutes.Name = "txtBbxCivilRoutes";
            this.txtBbxCivilRoutes.Size = new System.Drawing.Size(388, 20);
            this.txtBbxCivilRoutes.TabIndex = 12;
            this.txtBbxCivilRoutes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBbxCivilRoutes_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Maximum Number of Routes:";
            // 
            // txtBxCiviRatio
            // 
            this.txtBxCiviRatio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxCiviRatio.Location = new System.Drawing.Point(186, 23);
            this.txtBxCiviRatio.MaxLength = 7;
            this.txtBxCiviRatio.Name = "txtBxCiviRatio";
            this.txtBxCiviRatio.Size = new System.Drawing.Size(388, 20);
            this.txtBxCiviRatio.TabIndex = 6;
            this.txtBxCiviRatio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBxCiviRatio_KeyPress);
            // 
            // txtBxCivilSpof
            // 
            this.txtBxCivilSpof.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxCivilSpof.Location = new System.Drawing.Point(186, 57);
            this.txtBxCivilSpof.MaxLength = 7;
            this.txtBxCivilSpof.Name = "txtBxCivilSpof";
            this.txtBxCivilSpof.Size = new System.Drawing.Size(388, 20);
            this.txtBxCivilSpof.TabIndex = 8;
            this.txtBxCivilSpof.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBxCivilSpof_KeyPress);
            // 
            // txtBxCivilSegments
            // 
            this.txtBxCivilSegments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxCivilSegments.Location = new System.Drawing.Point(186, 93);
            this.txtBxCivilSegments.MaxLength = 7;
            this.txtBxCivilSegments.Name = "txtBxCivilSegments";
            this.txtBxCivilSegments.Size = new System.Drawing.Size(388, 20);
            this.txtBxCivilSegments.TabIndex = 10;
            this.txtBxCivilSegments.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBxCivilSegments_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Maximum Number of Spans:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Single Point of Failure Buffers (m):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Distance to Path Ratio:";
            // 
            // ApplicationSettingsMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 380);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(612, 419);
            this.Name = "ApplicationSettingsMainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Application Settings Configuration";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtbxMaxRoutes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbxDistance;
        private System.Windows.Forms.TextBox txtbxSpof;
        private System.Windows.Forms.TextBox txtbxMaxSegments;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtBbxCivilRoutes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBxCiviRatio;
        private System.Windows.Forms.TextBox txtBxCivilSpof;
        private System.Windows.Forms.TextBox txtBxCivilSegments;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}