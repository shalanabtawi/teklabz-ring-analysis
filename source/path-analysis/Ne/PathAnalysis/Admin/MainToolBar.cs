﻿using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using System;
using System.Runtime.InteropServices;
using Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig;
using Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig;

namespace Teklabz.Ne.PathAnalysis.Admin
{

    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
    public class MainToolBar : BaseToolbar
    {

        private const string Guid = "8ffcb7f2-965d-4f3c-afec-0928cc573adb";
        private const string ProgId = "Teklabz.Ne.PathAnalysis.MainToolBar";

        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType)
        {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType)
        {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType)
        {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommandBars.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType)
        {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommandBars.Unregister(regKey);
        }

        #endregion

        #endregion

        public MainToolBar()
        {
            AddItem(ApplicationSettingTool.ProgId);
            BeginGroup();
            AddItem(TemplateNameTool.ProgId);
            BeginGroup();
            AddItem(RepAttributeTool.ProgId);
        }

        public override string Caption
        {
            get { return "Teklabz Path Analysis Tool"; }
        }

        public override string Name
        {
            get { return "TkPaToolBar"; }
        }

    }
}
