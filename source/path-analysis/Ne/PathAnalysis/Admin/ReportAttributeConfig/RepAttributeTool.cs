﻿using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.ArcMapUI;
using Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.UI;
using Teklabz.Ne.PathAnalysis.Common;

namespace Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig {

    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
    public class RepAttributeTool : BaseCommand {

        public const string ProgId = "NETB.PA.RepAtt";
        private const string Guid = "4d8d489d-ba80-40c6-951c-084763938e32";
        
        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType) {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType) {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);
        }

        #endregion

        #endregion

        public RepAttributeTool() {
            m_category = "Path Analysis Admin Tools";
            m_caption = "Report Attributes Configuration";
            m_message = "Report Attributes Configuration";
            m_toolTip = "Report Attributes Configuration";
            m_name = "Report Attributes Configuration";
            try {
                var bitmapResourceName = GetType().Name + ".bmp";
                m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            } catch (Exception ex) {
                Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        public override void OnClick() {
            if (!ReportAttDbManager.CheckTable()) {
                return;
            }

            if (!SecurityManager.CanPerformReportAttributes()) {
                return;
            }

            var open = new ReportAttributesForm();
            open.ShowDialog();
        }

        public override void OnCreate(object hook) {
            if (hook == null)
                return;
            if (hook is IMxApplication)
                m_enabled = true;
            else
                m_enabled = false;
        }

    }
}
