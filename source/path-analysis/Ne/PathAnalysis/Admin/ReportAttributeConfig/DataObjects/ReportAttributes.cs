﻿
namespace Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.DataObjects
{
    public class ReportAttributes
    {
        public string FieldName { set; get; }
        public int ObjectId;
        public string ClassName;
        public string DisplayClass { set; get; }

        public ReportAttributes(string fieldName, string className)
        {
            FieldName = fieldName;
            ClassName = className;
            DisplayClass = ClassName + "." + fieldName;
        }

        public ReportAttributes()
        {
        }
    }
}
