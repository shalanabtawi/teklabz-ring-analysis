﻿using ESRI.ArcGIS.Geodatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.DataObjects
{
    public class ReportAttDbManager
    {
        public const string TableName = "TK_PA_REP_ATTRIBUTES";
        public const string ObjectId = "OBJECTID";
        public const string clsName = "CLASS_NAME";
        public const string fldName = "FIELD_NAME";

        public static readonly List<string> RepAttributesList = new List<string> {
            ObjectId,
            clsName,
            fldName,

        };

        public static bool CheckTable()
        {
            var findTbl = NeApiAccess.Instance.Database.GetNeTable(TableName);
            if (findTbl == null)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog(string.Format("Table {0} does not exist.", TableName));
                return false;
            }
            return true;
        }

        private static string ValidateFields(ITable table, List<string> tableList)
        {
            var message = string.Empty;
            try
            {
                foreach (var field in tableList)
                {
                    var indx = table.FindField(field);
                    if (indx == -1)
                    {
                        message += (string.IsNullOrEmpty(message) ? "The Fields : " : ",") + field;
                    }
                }
                if (!string.IsNullOrEmpty(message))
                {
                    message += " Was Not Found ";
                }
            }
            catch (Exception ex)
            {
                message += " " + ex;
            }
            return message;
        }
        private static void FillRowBuffer(IRowBuffer roBuffer, ITable table, ReportAttributes model)
        {
            roBuffer.Value[table.FindField(clsName)] = model.ClassName;
            roBuffer.Value[table.FindField(fldName)] = model.FieldName;

        }


        public static List<ReportAttributes> GetAllReportAttributes()
        {
            var list = FindTemplateModels(null);
            return list;
        }


        public static ReportAttributes FindTemplateModel(int id)
        {
            var list = FindTemplateModels(ObjectId + " = " + id);
            return list.FirstOrDefault();
        }


        public static List<ReportAttributes> FindTemplateModels(string query)
        {
            var neApi = NeApiAccess.Instance;
            var woAutoTranss = new List<ReportAttributes>();
            const string tableName = TableName;
            try
            {
                var objects = neApi.Database.FindNeObjects(tableName, query);
                foreach (var neObject in objects)
                {
                    var allModels = new ReportAttributes();
                    if (!neObject.Fields[ObjectId].IsValueNull())
                        allModels.ObjectId = neObject.Fields[ObjectId].GetValue<int>();
                    if (!neObject.Fields[clsName].IsValueNull())
                        allModels.ClassName = neObject.Fields[clsName].GetValue<string>();
                    if (!neObject.Fields[fldName].IsValueNull())
                        allModels.FieldName = neObject.Fields[fldName].GetValue<string>();
                    allModels.DisplayClass = allModels.ClassName + "." + allModels.FieldName;
                    woAutoTranss.Add(allModels);
                }
                woAutoTranss = woAutoTranss.OrderBy(x => x.ToString()).ToList();
            }
            catch (Exception ex)
            {
                neApi.Logger.LogError(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
                throw new Exception(string.Format("Error while getting records from [{0}] using query [{1}]", TableName, query), ex);
            }
            return woAutoTranss;
        }

        public static bool Add(ReportAttributes model)
        {
            var neApi = NeApiAccess.Instance;
            ICursor crsr = null;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                throw new Exception(string.Format("Table {0} was not found in the database", TableName));
            }
            try
            {
                transaction.StartTransaction();
                string message = ValidateFields(table, RepAttributesList);
                if (!string.IsNullOrEmpty(message))
                {
                    throw new Exception(message);
                }
                var roBuffer = table.CreateRowBuffer();
                crsr = table.Insert(true);
                FillRowBuffer(roBuffer, table, model);
                var oid = crsr.InsertRow(roBuffer);
                crsr.Flush();
                model.ObjectId = (int)oid;
                transaction.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Adding  " + TableName + " Values ", ex);
                return false;
            }
            finally
            {
                if (crsr != null)
                {
                    Marshal.ReleaseComObject(crsr);
                }
            }
        }

        public static bool Delete(int objectId)
        {
            var neApi = NeApiAccess.Instance;
            var workspace = neApi.Workspace;
            var transaction = (ITransactions)workspace;
            var table = neApi.Database.GetNeTable(TableName);
            if (table == null)
            {
                neApi.Logger.LogError(string.Format("Table {0} was not found in the database", TableName));
                throw new Exception(string.Format("Table {0} was not found in the database", TableName));
            }
            try
            {
                transaction.StartTransaction();
                var queryFilter = new QueryFilter();
                queryFilter.WhereClause = string.Format("OBJECTID={0}", objectId);
                table.DeleteSearchedRows(queryFilter);
                ICursor crsr = table.Search(queryFilter, false);
                transaction.CommitTransaction();
                return crsr.NextRow() == null;
            }
            catch (Exception ex)
            {
                transaction.AbortTransaction();
                neApi.Logger.LogError("Error While Deleting " + TableName + " Values ", ex);
                return false;
            }
        }
    }

}

