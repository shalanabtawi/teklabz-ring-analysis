﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.Admin.ReportAttributeConfig.UI
{
    public partial class ReportAttributesForm : Form
    {
        public List<ReportAttributes> allAddedFields = new List<ReportAttributes>();
        public ReportAttributesForm()
        {

            InitializeComponent();
            FillAlreadyExisting();
            FillClass();
            FilterOnlyClassFields();
        }

        private void FillAlreadyExisting()
        {
            var existing = ReportAttDbManager.GetAllReportAttributes();
            if (existing == null)
            {
                return;
            }
            if (existing.Count == 0)
            {
                return;
            }
            var selectedClass = cmbxClass.SelectedItem as string;
            allAddedFields.AddRange(existing);
            var actualList = allAddedFields.FindAll(x => x.ClassName == selectedClass);
            var ordered = actualList.OrderBy(x => x.FieldName).ToList();
            listBxSelectedFields.DisplayMember = "FieldName";
            listBxSelectedFields.DataSource = ordered;
        }

        private void FillClass()
        {
            var classList = new List<string> { "TRANSMEDIA", "SPAN", "EQUIPMENT", "STRUCTURE", "SPLICE_CLOSURE" };
            classList.Sort();
            cmbxClass.DataSource = classList;
        }

        private void cmbxClass_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var selectedClass = cmbxClass.SelectedItem as string;
            if (selectedClass == null)
                return;
            FillFieldListBox(selectedClass);
            FilterOnlyClassFields();
        }

        private void FilterOnlyClassFields()
        {
            if (allAddedFields == null)
            {
                return;
            }
            var selectedClass = cmbxClass.SelectedItem as string;
            var classFlds = allAddedFields.FindAll(x => x.ClassName == selectedClass);
            if (classFlds.Count == 0)
            {
                listBxSelectedFields.DataSource = null;
                return;
            }
            classFlds.OrderBy(x => x.FieldName);
            listBxSelectedFields.DataSource = classFlds;
            listBxSelectedFields.DisplayMember = "FieldName";
        }

        private void FillFieldListBox(string selectedClass)
        {
            var existingAddFlds = listBxSelectedFields.DataSource as List<ReportAttributes>;

            var classTbl = NeApiAccess.Instance.Database.GetNeFeatureClass(selectedClass);
            var allFields = new List<ReportAttributes>();
            for (int i = 0; i < classTbl.Fields.FieldCount; i++)
            {
                if (existingAddFlds != null)
                {
                    if (existingAddFlds.Exists(x => x.FieldName == classTbl.Fields.Field[i].Name.ToString() && x.ClassName == selectedClass))
                    {
                        continue;
                    }


                }
                if (allAddedFields != null)
                {
                    if (allAddedFields.Exists(x => x.FieldName == classTbl.Fields.Field[i].Name.ToString() && x.ClassName == selectedClass))
                    {
                        continue;
                    }
                }

                allFields.Add(new ReportAttributes(classTbl.Fields.Field[i].Name.ToString(), selectedClass));

            }
            var newList = allFields.OrderBy(x => x.FieldName);
            listBxFields.DisplayMember = "FieldName";
            listBxFields.DataSource = newList.ToList();
        }

        private void AddField(object sender, System.EventArgs e)
        {
            var selectedField = listBxFields.SelectedItems;
            if (selectedField == null)
                return;

            var existingList = listBxSelectedFields.DataSource as List<ReportAttributes>;
            if (existingList == null)
            {
                existingList = new List<ReportAttributes>();
            }
            listBxSelectedFields.DisplayMember = "DisplayClass";

            var selectedClassType = cmbxClass.SelectedItem as string;
            if (selectedClassType == null)
            {
                return;
            }
            foreach (var field in selectedField)
            {
                var rep = field as ReportAttributes;
                if (rep.ClassName != selectedClassType)
                {
                    return;
                }
                //Add to selected Fields List box
                existingList.Add(rep);
                var list = existingList.OrderBy(x => x.ClassName).ToList();
                listBxSelectedFields.DataSource = list;

                //Remove From Main
                //removing from selected list box
                var oldList = listBxFields.DataSource as List<ReportAttributes>;
                oldList.Remove(rep);
                var newList = oldList.OrderBy(x => x.ClassName);
                listBxFields.DataSource = newList.ToList();
                allAddedFields.Add(rep);
            }
            FilterOnlyClassFields();
            listBxFields.DisplayMember = "FieldName";
        }

        private void RemoveField(object sender, System.EventArgs e)
        {
            var selectedClass = cmbxClass.SelectedItem as string;
            var selectedFieldToRemove = listBxSelectedFields.SelectedItems;
            var selectedField = listBxFields.SelectedItem as ReportAttributes;
            var oldNum = listBxSelectedFields.SelectedIndex;

            if (selectedClass == null)
                return;
            if (selectedFieldToRemove == null)
                return;

            foreach (var remove in selectedFieldToRemove)
            {
                var fieldRequested = remove as ReportAttributes;
                if (fieldRequested.ClassName != selectedClass)
                {
                    continue;
                }
                //removing from selected list box
                var oldList = listBxSelectedFields.DataSource as List<ReportAttributes>;
                oldList.Remove(fieldRequested);
                var newList = oldList.OrderBy(x => x.ClassName);
                listBxSelectedFields.DataSource = newList.ToList();
                //return to main fields
                var originalList = listBxFields.DataSource as List<ReportAttributes>;
                originalList.Add(fieldRequested);
                var newnList = originalList.OrderBy(x => x.FieldName);
                listBxFields.DataSource = newnList.ToList();
                SelectNextAttribute(oldNum);
                allAddedFields.Remove(fieldRequested);
            }
            FilterOnlyClassFields();
        }

        private void SelectNextAttribute(int number)
        {


            if (number == 0)
            {
                return;
            }
            listBxSelectedFields.SelectedIndex = number - 1;
        }

        private void Save(object sender, System.EventArgs e)
        {
   
            var allRepAtts = ReportAttDbManager.GetAllReportAttributes();
            foreach (var attribute in allRepAtts)
            {
                ReportAttDbManager.Delete(attribute.ObjectId);
            }
            foreach (var newAtt in allAddedFields)
            {
                ReportAttDbManager.Add(newAtt);
            }
            NeApiAccess.Instance.Logger.ShowMessageDialog("Saved!");
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Cursor = Cursors.Default;
            Close();
        }
    }
}
