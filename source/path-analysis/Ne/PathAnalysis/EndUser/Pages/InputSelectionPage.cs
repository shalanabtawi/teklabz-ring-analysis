﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.EndUser.DataObjects;
using Teklabz.NetworkEngineer.Api;
using Wizard;

namespace Teklabz.Ne.PathAnalysis.EndUser.Pages {
    public partial class InputSelectionPage : UserControl, IUserPage {

        private IActiveViewEvents_Event _activeViewEvents;

        public InputSelectionPage() {
            InitializeComponent();

            Control = this;
            IsValid = true;
            PageName = "Input Selection";
            Summary = "Select From - To Equipment and/or Site for Path Analysis";
        }

        public UserControl Control { get; }
        public string PageName { get; }
        public string Summary { get; }
        public bool IsValid { get; set; }
        public string InvalidMessage { get; set; }
        public object UserData { get; set; }

        private Dictionary<string, IElement> _selectedElements;

        public bool CurrentPageOnLoad(List<object> allControlsObjects, LastForm lastForm) {
            _selectedElements = new Dictionary<string, IElement>();
            FillFromToFeatures();
            rdbtnEquipment.Checked = true;
            rdbtnCableRoute.Checked = true;
            rdbtnUseAttributeAssociation.Checked = true;
            chbxIgnoreFlow.Checked = true;

            var neApi = NeApiAccess.Instance as NeApiAccessArcMap;
            if (neApi != null) {
                _activeViewEvents = neApi.Document.ActiveView as IActiveViewEvents_Event;
                if (_activeViewEvents != null)
                    _activeViewEvents.SelectionChanged += FeatureSelectionChanged;
            }
            
            return false;
        }

        public void NextButtonClicked() {
            var fromEquipment = cmbxFromEquipment.SelectedItem  as SelectedFeature; 
            var toEquipment = cmbToEquipment.SelectedItem as SelectedFeature; 
            var fromSite = cmbFromSite.SelectedItem as SelectedFeature; 
            var toSite = cmbToSite.SelectedItem as SelectedFeature; 
            var template = cmbxTemplateName.SelectedItem as TemplateNames;

            if ((fromEquipment == null && fromSite == null) || (toEquipment == null && toSite == null) ||
                template == null) {
                IsValid = false;
                InvalidMessage =
                    "Please make sure you select From Equipment/Site, To Equipment/Site and Template Name in order to continue.";
                return;
            }

            if (rdbtnEquipment.Checked && fromEquipment != null && toEquipment != null) {
                if (fromEquipment.Equals(toEquipment)) {
                    IsValid = false;
                    InvalidMessage =
                        "Please make sure you select different From Equipment and To Equipment in order to continue.";
                    return;
                }
            }

            if (!rdbtnEquipment.Checked && fromSite != null && toSite != null) {
                if (fromSite.Equals(toSite)) {
                    IsValid = false;
                    InvalidMessage =
                        "Please make sure you select different From Site and To Site in order to continue.";
                    return;
                }
            }

            var processor = new PathProcessor(template)
            {
                TemplateName = template,
                IgnoreFromTo = chbxIgnoreFlow.Checked,
                UseCable = rdbtnCableRoute.Checked,
                IsEquipment = rdbtnEquipment.Checked,
                UseAttributeAssociations = rdbtnUseAttributeAssociation.Checked,
            };

            var text = textBox1.Text;
            if (text != null) {
                if (_selectedElements.ContainsKey(text)) {
                    processor.InputGeometry = _selectedElements[text].Geometry;
                }
            }

            if (rdbtnEquipment.Checked) {
                processor.FromEquipmentSite = fromEquipment;
                processor.ToEquipmentSite = toEquipment;
            } else {
                processor.FromEquipmentSite = fromSite;
                processor.ToEquipmentSite = toSite;
            }

            UserData = processor;
            IsValid = true;

            _activeViewEvents.SelectionChanged -= FeatureSelectionChanged;
        }

        public void BackButtonClicked() {
            //This wont be active
        }

        public void FinishButtonClicked() {
            //This wont be active
        }

        public void HelpButtonClicked() {
            var neApi = NeApiAccess.Instance;
            var help = "Select the From and Two Equipment/Sites to analyze.";
            neApi.Logger.ShowMessageDialog("Equipment/Site Selection Help", help);
        }

        private void RefreshButtonClicked(object sender, EventArgs e) {
            FillFromToFeatures();
        }

        private void FillFromToFeatures() {
            _selectedElements = new Dictionary<string, IElement>();

            cmbxFromEquipment.DataSource = null;
            cmbToEquipment.DataSource = null;
            cmbFromSite.DataSource = null;
            cmbToSite.DataSource = null;
            
            var neApi = NeApiAccess.Instance;

            var selectedFeatureList1 = new List<SelectedFeature>();
            var selectedFeatureList2 = new List<SelectedFeature>();
            var selectedFeatureList3 = new List<SelectedFeature>();
            var selectedFeatureList4 = new List<SelectedFeature>();

            var selectedEquipment = neApi.Equipment.Selected;
            foreach (var neEquipment in selectedEquipment) {
                var newFeature = new SelectedFeature
                {
                    DisplayName = neEquipment.DisplayName,
                    ClassName = neApi.Equipment.ObjectClassName,
                    ObjectId = neEquipment.ObjectId
                };
                selectedFeatureList1.Add(newFeature);
                selectedFeatureList2.Add(newFeature);
                selectedFeatureList3.Add(newFeature);
                selectedFeatureList4.Add(newFeature);
            }

            var selectedStructures = neApi.Structures.Selected;
            foreach (var structure in selectedStructures) {
                var newFeature = new SelectedFeature
                {
                    DisplayName = structure.DisplayName,
                    ClassName = neApi.Structures.ObjectClassName,
                    ObjectId = structure.ObjectId
                };
                selectedFeatureList3.Add(newFeature);
                selectedFeatureList4.Add(newFeature);
            }

            cmbxFromEquipment.DataSource = selectedFeatureList1;
            cmbToEquipment.DataSource = selectedFeatureList2;

            cmbFromSite.DataSource = selectedFeatureList3;
            cmbToSite.DataSource = selectedFeatureList4;

            var templateNames = TemplateNameDbManager.GetAllTtemplateNames();
            cmbxTemplateName.DataSource = templateNames;
        }

        private void TemplateNameSelectedIndexChanged(object sender, EventArgs e) {
            var selectedItem = cmbxTemplateName.SelectedItem;
            if (selectedItem == null)
                return;

            var template = selectedItem as TemplateNames;
            if (template == null)
                return;

            txtTemplateDescription.Text = template.Description;
        }

        private void EquipmentCheckChanged(object sender, EventArgs e) {
            var equipmentChecked = rdbtnEquipment.Checked;

            cmbxFromEquipment.Enabled = equipmentChecked;
            cmbToEquipment.Enabled = equipmentChecked;
            cmbFromSite.Enabled = !equipmentChecked;
            cmbToSite.Enabled = !equipmentChecked;
            rdbtnCableRoute.Enabled = equipmentChecked;
            rdbtnSpanRoute.Enabled = !equipmentChecked;
            rdbtnCableRoute.Checked = equipmentChecked;
            rdbtnSpanRoute.Checked = !equipmentChecked;
        }

        private void UseAttributeAssociationCheckChanged(object sender, EventArgs e) {
            chbxIgnoreFlow.Enabled = rdbtnUseAttributeAssociation.Checked;
        }

        private void EnableSelectionButton(object sender, EventArgs e) {
            var uuid = new UIDClass {Value = "esriArcMapUI.SelectTool"};

            var neApi = NeApiAccess.Instance;
            var arcMap = (NeApiAccessArcMap)neApi;
            var document = arcMap.Application.Document;

            var commandItem = document.CommandBars.Find(uuid);
            if (commandItem == null)
                return;

            arcMap.Application.CurrentTool = commandItem;
        }
        
        private void FeatureSelectionChanged() {
            _selectedElements = new Dictionary<string, IElement>();
            textBox1.Text = string.Empty;

            var neApi = NeApiAccess.Instance;
            var arcMap = (NeApiAccessArcMap)neApi;

            var map = arcMap.Document.FocusMap;
            var graphicsContainer = (IGraphicsContainerSelect)map;
            if (graphicsContainer == null)
                return;

            IElement element;
            var count = 1;
            
            var elements = graphicsContainer.SelectedElements;
            while ((element = elements.Next()) != null) {
                if (element.Geometry.GeometryType !=esriGeometryType.esriGeometryPolygon) {
                    continue;
                }
                
                var name = string.Format("Element - {0}", count);
                textBox1.Text =name;
                _selectedElements[name] = element;
                break;
            }
        }
        
    }
}
