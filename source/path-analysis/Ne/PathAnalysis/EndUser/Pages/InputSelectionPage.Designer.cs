﻿namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    partial class InputSelectionPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputSelectionPage));
            this.tsMenu = new System.Windows.Forms.ToolStrip();
            this.tsbtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.lblFromEquipment = new System.Windows.Forms.Label();
            this.lblToEquipment = new System.Windows.Forms.Label();
            this.cmbxFromEquipment = new System.Windows.Forms.ComboBox();
            this.cmbToEquipment = new System.Windows.Forms.ComboBox();
            this.rdbtnEquipment = new System.Windows.Forms.RadioButton();
            this.rdbtnSite = new System.Windows.Forms.RadioButton();
            this.grpbxFeatureSelection = new System.Windows.Forms.GroupBox();
            this.lblInputPolygon = new System.Windows.Forms.Label();
            this.cmbToSite = new System.Windows.Forms.ComboBox();
            this.cmbFromSite = new System.Windows.Forms.ComboBox();
            this.lblToSite = new System.Windows.Forms.Label();
            this.lblFromSite = new System.Windows.Forms.Label();
            this.grpbxOutput = new System.Windows.Forms.GroupBox();
            this.txtTemplateDescription = new System.Windows.Forms.TextBox();
            this.cmbxTemplateName = new System.Windows.Forms.ComboBox();
            this.lblTemplateName = new System.Windows.Forms.Label();
            this.grpbxTraceSettings = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chbxIgnoreFlow = new System.Windows.Forms.CheckBox();
            this.rdbtnUseAttributeAssociation = new System.Windows.Forms.RadioButton();
            this.rdbtnUseGeographicalAssoociation = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdbtnCableRoute = new System.Windows.Forms.RadioButton();
            this.rdbtnSpanRoute = new System.Windows.Forms.RadioButton();
            this.btnEnableSelection = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tsMenu.SuspendLayout();
            this.grpbxFeatureSelection.SuspendLayout();
            this.grpbxOutput.SuspendLayout();
            this.grpbxTraceSettings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsMenu
            // 
            this.tsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnRefresh});
            this.tsMenu.Location = new System.Drawing.Point(0, 0);
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsMenu.Size = new System.Drawing.Size(512, 25);
            this.tsMenu.TabIndex = 0;
            this.tsMenu.Text = "toolStrip1";
            // 
            // tsbtnRefresh
            // 
            this.tsbtnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnRefresh.Image")));
            this.tsbtnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnRefresh.Name = "tsbtnRefresh";
            this.tsbtnRefresh.Size = new System.Drawing.Size(23, 22);
            this.tsbtnRefresh.Text = "Refresh Selection";
            this.tsbtnRefresh.Click += new System.EventHandler(this.RefreshButtonClicked);
            // 
            // lblFromEquipment
            // 
            this.lblFromEquipment.AutoSize = true;
            this.lblFromEquipment.Location = new System.Drawing.Point(13, 58);
            this.lblFromEquipment.Name = "lblFromEquipment";
            this.lblFromEquipment.Size = new System.Drawing.Size(86, 13);
            this.lblFromEquipment.TabIndex = 2;
            this.lblFromEquipment.Text = "From Equipment:";
            // 
            // lblToEquipment
            // 
            this.lblToEquipment.AutoSize = true;
            this.lblToEquipment.Location = new System.Drawing.Point(13, 82);
            this.lblToEquipment.Name = "lblToEquipment";
            this.lblToEquipment.Size = new System.Drawing.Size(76, 13);
            this.lblToEquipment.TabIndex = 4;
            this.lblToEquipment.Text = "To Equipment:";
            // 
            // cmbxFromEquipment
            // 
            this.cmbxFromEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxFromEquipment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFromEquipment.FormattingEnabled = true;
            this.cmbxFromEquipment.Location = new System.Drawing.Point(146, 54);
            this.cmbxFromEquipment.Name = "cmbxFromEquipment";
            this.cmbxFromEquipment.Size = new System.Drawing.Size(349, 21);
            this.cmbxFromEquipment.TabIndex = 3;
            // 
            // cmbToEquipment
            // 
            this.cmbToEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbToEquipment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToEquipment.FormattingEnabled = true;
            this.cmbToEquipment.Location = new System.Drawing.Point(146, 78);
            this.cmbToEquipment.Name = "cmbToEquipment";
            this.cmbToEquipment.Size = new System.Drawing.Size(349, 21);
            this.cmbToEquipment.TabIndex = 5;
            // 
            // rdbtnEquipment
            // 
            this.rdbtnEquipment.AutoSize = true;
            this.rdbtnEquipment.Location = new System.Drawing.Point(13, 23);
            this.rdbtnEquipment.Name = "rdbtnEquipment";
            this.rdbtnEquipment.Size = new System.Drawing.Size(75, 17);
            this.rdbtnEquipment.TabIndex = 0;
            this.rdbtnEquipment.TabStop = true;
            this.rdbtnEquipment.Text = "Equipment";
            this.rdbtnEquipment.UseVisualStyleBackColor = true;
            this.rdbtnEquipment.CheckedChanged += new System.EventHandler(this.EquipmentCheckChanged);
            // 
            // rdbtnSite
            // 
            this.rdbtnSite.AutoSize = true;
            this.rdbtnSite.Location = new System.Drawing.Point(146, 23);
            this.rdbtnSite.Name = "rdbtnSite";
            this.rdbtnSite.Size = new System.Drawing.Size(43, 17);
            this.rdbtnSite.TabIndex = 1;
            this.rdbtnSite.TabStop = true;
            this.rdbtnSite.Text = "Site";
            this.rdbtnSite.UseVisualStyleBackColor = true;
            // 
            // grpbxFeatureSelection
            // 
            this.grpbxFeatureSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpbxFeatureSelection.Controls.Add(this.btnEnableSelection);
            this.grpbxFeatureSelection.Controls.Add(this.textBox1);
            this.grpbxFeatureSelection.Controls.Add(this.lblInputPolygon);
            this.grpbxFeatureSelection.Controls.Add(this.cmbToSite);
            this.grpbxFeatureSelection.Controls.Add(this.cmbFromSite);
            this.grpbxFeatureSelection.Controls.Add(this.lblToSite);
            this.grpbxFeatureSelection.Controls.Add(this.lblFromSite);
            this.grpbxFeatureSelection.Controls.Add(this.rdbtnSite);
            this.grpbxFeatureSelection.Controls.Add(this.rdbtnEquipment);
            this.grpbxFeatureSelection.Controls.Add(this.cmbToEquipment);
            this.grpbxFeatureSelection.Controls.Add(this.cmbxFromEquipment);
            this.grpbxFeatureSelection.Controls.Add(this.lblToEquipment);
            this.grpbxFeatureSelection.Controls.Add(this.lblFromEquipment);
            this.grpbxFeatureSelection.Location = new System.Drawing.Point(4, 29);
            this.grpbxFeatureSelection.Name = "grpbxFeatureSelection";
            this.grpbxFeatureSelection.Size = new System.Drawing.Size(505, 183);
            this.grpbxFeatureSelection.TabIndex = 1;
            this.grpbxFeatureSelection.TabStop = false;
            this.grpbxFeatureSelection.Text = "From/To Feature Selection";
            // 
            // lblInputPolygon
            // 
            this.lblInputPolygon.AutoSize = true;
            this.lblInputPolygon.Location = new System.Drawing.Point(13, 154);
            this.lblInputPolygon.Name = "lblInputPolygon";
            this.lblInputPolygon.Size = new System.Drawing.Size(93, 13);
            this.lblInputPolygon.TabIndex = 10;
            this.lblInputPolygon.Text = "Input Buffer Zone:";
            // 
            // cmbToSite
            // 
            this.cmbToSite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbToSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToSite.FormattingEnabled = true;
            this.cmbToSite.Location = new System.Drawing.Point(146, 126);
            this.cmbToSite.Name = "cmbToSite";
            this.cmbToSite.Size = new System.Drawing.Size(349, 21);
            this.cmbToSite.TabIndex = 9;
            // 
            // cmbFromSite
            // 
            this.cmbFromSite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbFromSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFromSite.FormattingEnabled = true;
            this.cmbFromSite.Location = new System.Drawing.Point(146, 102);
            this.cmbFromSite.Name = "cmbFromSite";
            this.cmbFromSite.Size = new System.Drawing.Size(349, 21);
            this.cmbFromSite.TabIndex = 7;
            // 
            // lblToSite
            // 
            this.lblToSite.AutoSize = true;
            this.lblToSite.Location = new System.Drawing.Point(13, 130);
            this.lblToSite.Name = "lblToSite";
            this.lblToSite.Size = new System.Drawing.Size(44, 13);
            this.lblToSite.TabIndex = 8;
            this.lblToSite.Text = "To Site:";
            // 
            // lblFromSite
            // 
            this.lblFromSite.AutoSize = true;
            this.lblFromSite.Location = new System.Drawing.Point(13, 106);
            this.lblFromSite.Name = "lblFromSite";
            this.lblFromSite.Size = new System.Drawing.Size(54, 13);
            this.lblFromSite.TabIndex = 6;
            this.lblFromSite.Text = "From Site:";
            // 
            // grpbxOutput
            // 
            this.grpbxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpbxOutput.Controls.Add(this.txtTemplateDescription);
            this.grpbxOutput.Controls.Add(this.cmbxTemplateName);
            this.grpbxOutput.Controls.Add(this.lblTemplateName);
            this.grpbxOutput.Location = new System.Drawing.Point(4, 212);
            this.grpbxOutput.Name = "grpbxOutput";
            this.grpbxOutput.Size = new System.Drawing.Size(505, 103);
            this.grpbxOutput.TabIndex = 2;
            this.grpbxOutput.TabStop = false;
            this.grpbxOutput.Text = "Output Path Settings";
            // 
            // txtTemplateDescription
            // 
            this.txtTemplateDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTemplateDescription.Enabled = false;
            this.txtTemplateDescription.Location = new System.Drawing.Point(146, 46);
            this.txtTemplateDescription.Multiline = true;
            this.txtTemplateDescription.Name = "txtTemplateDescription";
            this.txtTemplateDescription.Size = new System.Drawing.Size(349, 51);
            this.txtTemplateDescription.TabIndex = 2;
            // 
            // cmbxTemplateName
            // 
            this.cmbxTemplateName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxTemplateName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxTemplateName.FormattingEnabled = true;
            this.cmbxTemplateName.Location = new System.Drawing.Point(146, 19);
            this.cmbxTemplateName.Name = "cmbxTemplateName";
            this.cmbxTemplateName.Size = new System.Drawing.Size(349, 21);
            this.cmbxTemplateName.TabIndex = 1;
            this.cmbxTemplateName.SelectedIndexChanged += new System.EventHandler(this.TemplateNameSelectedIndexChanged);
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.AutoSize = true;
            this.lblTemplateName.Location = new System.Drawing.Point(13, 23);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Size = new System.Drawing.Size(85, 13);
            this.lblTemplateName.TabIndex = 0;
            this.lblTemplateName.Text = "Template Name:";
            // 
            // grpbxTraceSettings
            // 
            this.grpbxTraceSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpbxTraceSettings.Controls.Add(this.panel2);
            this.grpbxTraceSettings.Controls.Add(this.panel1);
            this.grpbxTraceSettings.Controls.Add(this.panel3);
            this.grpbxTraceSettings.Location = new System.Drawing.Point(4, 317);
            this.grpbxTraceSettings.Name = "grpbxTraceSettings";
            this.grpbxTraceSettings.Size = new System.Drawing.Size(505, 136);
            this.grpbxTraceSettings.TabIndex = 3;
            this.grpbxTraceSettings.TabStop = false;
            this.grpbxTraceSettings.Text = "Analysis Rules";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(499, 0);
            this.panel2.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chbxIgnoreFlow);
            this.panel1.Controls.Add(this.rdbtnUseAttributeAssociation);
            this.panel1.Controls.Add(this.rdbtnUseGeographicalAssoociation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(499, 68);
            this.panel1.TabIndex = 4;
            // 
            // chbxIgnoreFlow
            // 
            this.chbxIgnoreFlow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chbxIgnoreFlow.AutoSize = true;
            this.chbxIgnoreFlow.Location = new System.Drawing.Point(10, 39);
            this.chbxIgnoreFlow.Name = "chbxIgnoreFlow";
            this.chbxIgnoreFlow.Size = new System.Drawing.Size(129, 17);
            this.chbxIgnoreFlow.TabIndex = 18;
            this.chbxIgnoreFlow.Text = "Ignore From - To Flow";
            this.chbxIgnoreFlow.UseVisualStyleBackColor = true;
            // 
            // rdbtnUseAttributeAssociation
            // 
            this.rdbtnUseAttributeAssociation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rdbtnUseAttributeAssociation.AutoSize = true;
            this.rdbtnUseAttributeAssociation.Location = new System.Drawing.Point(10, 10);
            this.rdbtnUseAttributeAssociation.Name = "rdbtnUseAttributeAssociation";
            this.rdbtnUseAttributeAssociation.Size = new System.Drawing.Size(148, 17);
            this.rdbtnUseAttributeAssociation.TabIndex = 16;
            this.rdbtnUseAttributeAssociation.TabStop = true;
            this.rdbtnUseAttributeAssociation.Text = "Use Attribute Associations";
            this.rdbtnUseAttributeAssociation.UseVisualStyleBackColor = true;
            this.rdbtnUseAttributeAssociation.CheckedChanged += new System.EventHandler(this.UseAttributeAssociationCheckChanged);
            // 
            // rdbtnUseGeographicalAssoociation
            // 
            this.rdbtnUseGeographicalAssoociation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rdbtnUseGeographicalAssoociation.AutoSize = true;
            this.rdbtnUseGeographicalAssoociation.Location = new System.Drawing.Point(172, 10);
            this.rdbtnUseGeographicalAssoociation.Name = "rdbtnUseGeographicalAssoociation";
            this.rdbtnUseGeographicalAssoociation.Size = new System.Drawing.Size(172, 17);
            this.rdbtnUseGeographicalAssoociation.TabIndex = 17;
            this.rdbtnUseGeographicalAssoociation.TabStop = true;
            this.rdbtnUseGeographicalAssoociation.Text = "Use Geographical Associations";
            this.rdbtnUseGeographicalAssoociation.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rdbtnCableRoute);
            this.panel3.Controls.Add(this.rdbtnSpanRoute);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(499, 49);
            this.panel3.TabIndex = 3;
            // 
            // rdbtnCableRoute
            // 
            this.rdbtnCableRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rdbtnCableRoute.AutoSize = true;
            this.rdbtnCableRoute.Location = new System.Drawing.Point(10, 16);
            this.rdbtnCableRoute.Name = "rdbtnCableRoute";
            this.rdbtnCableRoute.Size = new System.Drawing.Size(106, 17);
            this.rdbtnCableRoute.TabIndex = 15;
            this.rdbtnCableRoute.TabStop = true;
            this.rdbtnCableRoute.Text = "Use Cable Route";
            this.rdbtnCableRoute.UseVisualStyleBackColor = true;
            // 
            // rdbtnSpanRoute
            // 
            this.rdbtnSpanRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rdbtnSpanRoute.AutoSize = true;
            this.rdbtnSpanRoute.Location = new System.Drawing.Point(172, 16);
            this.rdbtnSpanRoute.Name = "rdbtnSpanRoute";
            this.rdbtnSpanRoute.Size = new System.Drawing.Size(104, 17);
            this.rdbtnSpanRoute.TabIndex = 16;
            this.rdbtnSpanRoute.TabStop = true;
            this.rdbtnSpanRoute.Text = "Use Span Route";
            this.rdbtnSpanRoute.UseVisualStyleBackColor = true;
            // 
            // btnEnableSelection
            // 
            this.btnEnableSelection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnEnableSelection.Image = ((System.Drawing.Image)(resources.GetObject("btnEnableSelection.Image")));
            this.btnEnableSelection.Location = new System.Drawing.Point(457, 150);
            this.btnEnableSelection.Name = "btnEnableSelection";
            this.btnEnableSelection.Size = new System.Drawing.Size(38, 20);
            this.btnEnableSelection.TabIndex = 12;
            this.btnEnableSelection.UseVisualStyleBackColor = true;
            this.btnEnableSelection.Click += new System.EventHandler(this.EnableSelectionButton);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(146, 150);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(305, 20);
            this.textBox1.TabIndex = 11;
            // 
            // InputSelectionPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpbxTraceSettings);
            this.Controls.Add(this.grpbxOutput);
            this.Controls.Add(this.grpbxFeatureSelection);
            this.Controls.Add(this.tsMenu);
            this.Name = "InputSelectionPage";
            this.Size = new System.Drawing.Size(512, 455);
            this.tsMenu.ResumeLayout(false);
            this.tsMenu.PerformLayout();
            this.grpbxFeatureSelection.ResumeLayout(false);
            this.grpbxFeatureSelection.PerformLayout();
            this.grpbxOutput.ResumeLayout(false);
            this.grpbxOutput.PerformLayout();
            this.grpbxTraceSettings.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip tsMenu;
        private System.Windows.Forms.ToolStripButton tsbtnRefresh;
        private System.Windows.Forms.Label lblFromEquipment;
        private System.Windows.Forms.Label lblToEquipment;
        private System.Windows.Forms.ComboBox cmbxFromEquipment;
        private System.Windows.Forms.ComboBox cmbToEquipment;
        private System.Windows.Forms.RadioButton rdbtnEquipment;
        private System.Windows.Forms.RadioButton rdbtnSite;
        private System.Windows.Forms.GroupBox grpbxFeatureSelection;
        private System.Windows.Forms.ComboBox cmbToSite;
        private System.Windows.Forms.ComboBox cmbFromSite;
        private System.Windows.Forms.Label lblToSite;
        private System.Windows.Forms.Label lblFromSite;
        private System.Windows.Forms.GroupBox grpbxOutput;
        private System.Windows.Forms.TextBox txtTemplateDescription;
        private System.Windows.Forms.ComboBox cmbxTemplateName;
        private System.Windows.Forms.Label lblTemplateName;
        private System.Windows.Forms.Label lblInputPolygon;
        private System.Windows.Forms.GroupBox grpbxTraceSettings;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rdbtnCableRoute;
        private System.Windows.Forms.RadioButton rdbtnSpanRoute;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chbxIgnoreFlow;
        private System.Windows.Forms.RadioButton rdbtnUseAttributeAssociation;
        private System.Windows.Forms.RadioButton rdbtnUseGeographicalAssoociation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnEnableSelection;
        private System.Windows.Forms.TextBox textBox1;
    }
}
