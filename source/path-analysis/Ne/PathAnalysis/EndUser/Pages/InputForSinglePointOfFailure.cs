﻿using System.Collections.Generic;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.EndUser.DataObjects;
using Wizard;

namespace Teklabz.Ne.PathAnalysis.EndUser.Pages {
    public partial class InputForSinglePointOfFailure : UserControl, IUserPage {

        public InputForSinglePointOfFailure() {
            InitializeComponent();

            Control = this;
            IsValid = true;
            PageName = "Single Point of Failure";
            Summary = "Single Point of Failure";
        }

        public UserControl Control { get; }
        public string PageName { get; }
        public string Summary { get; }
        public bool IsValid { get; set; }
        public string InvalidMessage { get; set; }
        public object UserData { get; set; }

        public bool CurrentPageOnLoad(List<object> allControlsObjects, LastForm lastForm) {
            if (allControlsObjects == null)
                return false;

            if (allControlsObjects.Count < 2)
                return false;

            var previousData = allControlsObjects[1] as Dictionary<int, Route>;
            if (previousData == null)
                return false;

            foreach (var route in previousData) {
                checkedListBox1.Items.Add(route.Value);
            }

            return false;
        }

        public void NextButtonClicked() {
            var checkedIndices = checkedListBox1.CheckedIndices;
            if (checkedIndices.Count <= 0) {
                IsValid = false;
                InvalidMessage =
                    "Please make sure you select at least one path.";
                return;
            }

            var selectedPaths = new List<Route>();
            foreach (int checkedIndex in checkedIndices) {
                var item = checkedListBox1.Items[checkedIndex];
                if (item == null)
                    continue;

                var route = item as Route;
                if (route == null)
                    continue;

                selectedPaths.Add(route);
            }

            var spofProcessor = new SpofProcessor
            {
                SelectedRoutes = selectedPaths,
                BufferDistance = (double)nudBufferDistance.Value,
                SharedDistance = (double)nudSharedDistance.Value,
                SpofMinimumSharedDistance = (double)nudSpofMinimumSharedDistance.Value
            };

            UserData = spofProcessor;
           var intersections= SpofProcessor.FindFirstSpof(spofProcessor);
            UserData = intersections;
        }

        public void BackButtonClicked() {
            
        }

        public void FinishButtonClicked() {
            
        }

        public void HelpButtonClicked() {
            
        }

    }
}
