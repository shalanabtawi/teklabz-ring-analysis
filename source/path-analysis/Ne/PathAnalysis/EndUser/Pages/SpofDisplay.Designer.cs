﻿namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    partial class SpofDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvSpofs = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSpofs)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvSpofs
            // 
            this.DgvSpofs.AllowUserToAddRows = false;
            this.DgvSpofs.AllowUserToDeleteRows = false;
            this.DgvSpofs.AllowUserToResizeRows = false;
            this.DgvSpofs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvSpofs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.FirstId,
            this.SecondId});
            this.DgvSpofs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvSpofs.Location = new System.Drawing.Point(0, 0);
            this.DgvSpofs.MultiSelect = false;
            this.DgvSpofs.Name = "DgvSpofs";
            this.DgvSpofs.ReadOnly = true;
            this.DgvSpofs.RowHeadersVisible = false;
            this.DgvSpofs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvSpofs.Size = new System.Drawing.Size(577, 356);
            this.DgvSpofs.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "IntersectionGeometry";
            this.Column1.HeaderText = "Geometry";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "IntersectionTypeProp";
            this.Column2.HeaderText = "SPOF Type";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // FirstId
            // 
            this.FirstId.DataPropertyName = "FirstId";
            this.FirstId.HeaderText = "First Feature Id";
            this.FirstId.Name = "FirstId";
            this.FirstId.ReadOnly = true;
            this.FirstId.Width = 150;
            // 
            // SecondId
            // 
            this.SecondId.DataPropertyName = "SecondId";
            this.SecondId.HeaderText = "Second Feature Id";
            this.SecondId.Name = "SecondId";
            this.SecondId.ReadOnly = true;
            this.SecondId.Width = 150;
            // 
            // SpofDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DgvSpofs);
            this.Name = "SpofDisplay";
            this.Size = new System.Drawing.Size(577, 356);
            ((System.ComponentModel.ISupportInitialize)(this.DgvSpofs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvSpofs;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondId;
    }
}
