﻿namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    partial class InputForSinglePointOfFailure
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBufferDistance = new System.Windows.Forms.Label();
            this.txtSharedDistance = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudSpofMinimumSharedDistance = new System.Windows.Forms.NumericUpDown();
            this.nudSharedDistance = new System.Windows.Forms.NumericUpDown();
            this.nudBufferDistance = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpofMinimumSharedDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSharedDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBufferDistance)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 404);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Paths for Analysis";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nudSpofMinimumSharedDistance);
            this.panel1.Controls.Add(this.nudSharedDistance);
            this.panel1.Controls.Add(this.nudBufferDistance);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtSharedDistance);
            this.panel1.Controls.Add(this.txtBufferDistance);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 94);
            this.panel1.TabIndex = 2;
            // 
            // txtBufferDistance
            // 
            this.txtBufferDistance.AutoSize = true;
            this.txtBufferDistance.Location = new System.Drawing.Point(10, 12);
            this.txtBufferDistance.Name = "txtBufferDistance";
            this.txtBufferDistance.Size = new System.Drawing.Size(80, 13);
            this.txtBufferDistance.TabIndex = 0;
            this.txtBufferDistance.Text = "Buffer Distance";
            // 
            // txtSharedDistance
            // 
            this.txtSharedDistance.AutoSize = true;
            this.txtSharedDistance.Location = new System.Drawing.Point(10, 41);
            this.txtSharedDistance.Name = "txtSharedDistance";
            this.txtSharedDistance.Size = new System.Drawing.Size(86, 13);
            this.txtSharedDistance.TabIndex = 1;
            this.txtSharedDistance.Text = "Shared Distance";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "SPOF Minimum Shared Distance (m)";
            // 
            // nudSpofMinimumSharedDistance
            // 
            this.nudSpofMinimumSharedDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudSpofMinimumSharedDistance.DecimalPlaces = 1;
            this.nudSpofMinimumSharedDistance.Location = new System.Drawing.Point(200, 66);
            this.nudSpofMinimumSharedDistance.Name = "nudSpofMinimumSharedDistance";
            this.nudSpofMinimumSharedDistance.Size = new System.Drawing.Size(387, 20);
            this.nudSpofMinimumSharedDistance.TabIndex = 8;
            // 
            // nudSharedDistance
            // 
            this.nudSharedDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudSharedDistance.DecimalPlaces = 1;
            this.nudSharedDistance.Location = new System.Drawing.Point(200, 37);
            this.nudSharedDistance.Name = "nudSharedDistance";
            this.nudSharedDistance.Size = new System.Drawing.Size(387, 20);
            this.nudSharedDistance.TabIndex = 7;
            // 
            // nudBufferDistance
            // 
            this.nudBufferDistance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudBufferDistance.DecimalPlaces = 1;
            this.nudBufferDistance.Location = new System.Drawing.Point(200, 8);
            this.nudBufferDistance.Name = "nudBufferDistance";
            this.nudBufferDistance.Size = new System.Drawing.Size(387, 20);
            this.nudBufferDistance.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkedListBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 110);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(590, 291);
            this.panel2.TabIndex = 3;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(0, 0);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(590, 291);
            this.checkedListBox1.TabIndex = 2;
            // 
            // InputForSinglePointOfFailure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "InputForSinglePointOfFailure";
            this.Size = new System.Drawing.Size(596, 404);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpofMinimumSharedDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSharedDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBufferDistance)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudSpofMinimumSharedDistance;
        private System.Windows.Forms.NumericUpDown nudSharedDistance;
        private System.Windows.Forms.NumericUpDown nudBufferDistance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtSharedDistance;
        private System.Windows.Forms.Label txtBufferDistance;
    }
}
