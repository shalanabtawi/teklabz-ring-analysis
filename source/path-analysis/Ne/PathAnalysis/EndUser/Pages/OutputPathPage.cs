﻿using Equin.ApplicationFramework;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.EndUser.DataObjects;
using Teklabz.Ne.PathAnalysis.EndUser.OutputManager;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;
using Teklabz.NetworkEngineer.Api.Extension;
using Wizard;

namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    public partial class OutputPathPage : UserControl, IUserPage
    {

        public OutputPathPage()
        {
            InitializeComponent();

            Control = this;
            IsValid = true;
            PageName = "Output Path Details";
            Summary = "Path Details";
        }

        public UserControl Control { get; }
        public string PageName { get; }
        public string Summary { get; }
        public bool IsValid { get; set; }
        public string InvalidMessage { get; set; }
        public object UserData { get; set; }
        public PathProcessor passedInfo;
        public bool CurrentPageOnLoad(List<object> allControlsObjects, LastForm lastForm)
        {
            if (allControlsObjects == null)
                return false;

            if (allControlsObjects.Count < 1)
                return false;

            var previousData = allControlsObjects[0] as PathProcessor;
            if (previousData == null)
                return false;

            var isCable = previousData.UseCable;
            BuildGrid(isCable);
            passedInfo = previousData;
            NeApiAccess.Instance.Logger.LogTiming("Start Time");
            if (previousData.UseCable)
            {
                var graph = previousData.GetCableRoutes();

                var graphDictionary = new Dictionary<int, Route>();
                var graphCount = 1;
                foreach (var route in graph)
                {
                    route.RouteNumber = graphCount;
                    graphDictionary[graphCount] = route;

                    cmbPathNames.Items.Add(string.Format("Path - {0}", graphCount));
                    graphCount++;
                }

                UserData = graphDictionary;
            }
            else
            {
                var graph = previousData.GetSpanRoutes();

                var graphDictionary = new Dictionary<int, Route>();
                var graphCount = 1;
                foreach (var route in graph)
                {
                    route.RouteNumber = graphCount;
                    graphDictionary[graphCount] = route;

                    cmbPathNames.Items.Add(string.Format("Path - {0}", graphCount));
                    graphCount++;
                }

                UserData = graphDictionary;
            }
            NeApiAccess.Instance.Logger.LogTiming("End Time");
            return false;
        }

       

        public void NextButtonClicked()
        {
            if (UserData == null)
            {
                IsValid = false;
                InvalidMessage = "Unable to find Paths";
                return;
            }

            var cast = UserData as Dictionary<int, Route>;
            if (cast == null)
            {
                IsValid = false;
                InvalidMessage = "Unable to find Paths";
                return;
            }

            IsValid = true;
        }

        public void BackButtonClicked()
        {

        }

        public void FinishButtonClicked()
        {
            //This will be disabled
        }

        public void HelpButtonClicked()
        {

        }

        private void BuildGrid(bool isCable)
        {
            dgvResults.Columns.Clear();

            dgvResults.AutoGenerateColumns = false;
            dgvResults.AllowUserToAddRows = false;
            dgvResults.AllowUserToDeleteRows = false;
            dgvResults.AllowUserToResizeRows = false;
            dgvResults.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvResults.Dock = DockStyle.Fill;
            dgvResults.Location = new System.Drawing.Point(0, 0);
            dgvResults.ReadOnly = true;
            dgvResults.RowHeadersWidth = 25;
            dgvResults.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dgvResults.Size = new System.Drawing.Size(800, 450);

            var fromStructureName = new DataGridViewTextBoxColumn
            {
                DataPropertyName = "FromStructureName",
                HeaderText = "From Structure Name",
                Name = "FromStructureName",
                ReadOnly = true
            };
            dgvResults.Columns.Add(fromStructureName);

            if (isCable)
            {
                var fromEquipmentName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "FromEquipmentName",
                    HeaderText = "From Equipment Name",
                    Name = "FromEquipmentName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(fromEquipmentName);

                var fromSpliceName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "FromSpliceName",
                    HeaderText = "From Splice Name",
                    Name = "FromSpliceName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(fromSpliceName);

                var transmediaName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "EdgeName",
                    HeaderText = "Transmedia Name",
                    Name = "TransmediaName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(transmediaName);

                var transmediaLength = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "EdgeLength",
                    HeaderText = "Transmedia Length",
                    Name = "TransmediaLength",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(transmediaLength);
            }
            else
            {
                var spanName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "EdgeName",
                    HeaderText = "Span Name",
                    Name = "SpanName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(spanName);

                var spanLength = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "EdgeLength",
                    HeaderText = "Span Length",
                    Name = "SpanLength",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(spanLength);
            }

            var toStructureName = new DataGridViewTextBoxColumn
            {
                DataPropertyName = "ToStructureName",
                HeaderText = "To Structure Name",
                Name = "ToStructureName",
                ReadOnly = true
            };
            dgvResults.Columns.Add(toStructureName);

            if (isCable)
            {
                var toEquipmentName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "ToEquipmentName",
                    HeaderText = "To Equipment Name",
                    Name = "ToEquipmentName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(toEquipmentName);

                var toSpliceName = new DataGridViewTextBoxColumn
                {
                    DataPropertyName = "ToSpliceName",
                    HeaderText = "To Splice Name",
                    Name = "ToSpliceName",
                    ReadOnly = true
                };
                dgvResults.Columns.Add(toSpliceName);
            }
        }

        private void PathSelectedIndexChanged(object sender, EventArgs e)
        {
            dgvResults.DataSource = null;

            var graph = UserData as Dictionary<int, Route>;
            if (graph == null)
                return;

            var pathName = cmbPathNames.SelectedItem as string;
            if (pathName == null)
                return;

            var pathNumber = pathName.Split('-');
            if (pathNumber.Length != 2)
                return;

            int number;
            int.TryParse(pathNumber[1], out number);

            if (!graph.ContainsKey(number))
                return;

            var route = graph[number];
            lblSegments.Text = route.Edges.Count.ToString(CultureInfo.InvariantCulture);
            lblLength.Text = route.RouteDistance.ToString(CultureInfo.InvariantCulture);

            var edges = route.Edges;

            dgvResults.DataSource = new BindingListView<Edge>(edges);
        }

        private void SelectPathSegment(object sender, EventArgs e)
        {
            var features = GetPathFeatures();
            if (features == null)
                return;

            features.Select();
        }

        private void ZoomToPathSegment(object sender, EventArgs e)
        {
            var features = GetPathFeatures();
            if (features == null)
                return;

            IEnvelope envelope = new EnvelopeClass();
            foreach (var feature in features)
            {
                envelope.Union(feature.ShapeCopy.Envelope);
            }

            envelope.ZoomTo();
        }

        private void FlashPathSegment(object sender, EventArgs e)
        {
            var features = GetPathFeatures();
            if (features == null)
                return;

            foreach (var feature in features)
            {
                feature.Flash();
            }
        }

        private void SelectAllButtonClicked(object sender, EventArgs e)
        {
            var features = GetPathFeatures(true);
            if (features == null)
                return;

            features.Select();
        }


        private List<IFeature> GetPathFeatures(bool all = false)
        {
            var neApi = NeApiAccess.Instance;

            var selectedRows = dgvResults.SelectedRows;
            if (selectedRows.Count != 1 && !all)
            {
                neApi.Logger.ShowErrorDialog("Please make sure you select one row in order to continue.");
                return null;
            }

            var list = new List<Edge>();
            if (!all)
            {
                var selectedRow = selectedRows[0].DataBoundItem as ObjectView<Edge>;
                if (selectedRow == null)
                    return null;

                var selectedPath = selectedRow.Object;
                list.Add(selectedPath);
            }
            else
            {
                foreach (DataGridViewRow row in dgvResults.Rows)
                {
                    var selectedRow = row.DataBoundItem as ObjectView<Edge>;
                    if (selectedRow == null)
                        return null;

                    var selectedPath = selectedRow.Object;
                    list.Add(selectedPath);
                }
            }

            var listOfInventory = new List<IFeature>();
            var structureIds = new List<int>();
            var equipmentIds = new List<int>();
            var spliceIds = new List<int>();
            var transmediaIds = new List<int>();
            var spanIds = new List<int>();
            foreach (var selectedPath in list)
            {
                var fromClassName = selectedPath.FromFeature.ClassName;
                var toClassName = selectedPath.ToFeature.ClassName;
                var edgeClassName = selectedPath.ClassName;

                if (neApi.Equipment.ObjectClassName == fromClassName)
                {
                    equipmentIds.Add(selectedPath.FromFeature.ObjectId);
                }
                if (neApi.Equipment.ObjectClassName == toClassName)
                {
                    equipmentIds.Add(selectedPath.ToFeature.ObjectId);
                }

                if (neApi.SpliceClosures.ObjectClassName == fromClassName)
                {
                    spliceIds.Add(selectedPath.FromFeature.ObjectId);
                }
                if (neApi.SpliceClosures.ObjectClassName == toClassName)
                {
                    spliceIds.Add(selectedPath.ToFeature.ObjectId);
                }

                if (neApi.Structures.ObjectClassName == fromClassName)
                {
                    structureIds.Add(selectedPath.FromFeature.ObjectId);
                }
                if (neApi.Structures.ObjectClassName == toClassName)
                {
                    structureIds.Add(selectedPath.ToFeature.ObjectId);
                }

                if (neApi.Transmedia.ObjectClassName == edgeClassName)
                {
                    transmediaIds.Add(selectedPath.ObjectId);
                }

                if (neApi.Spans.ObjectClassName == edgeClassName)
                {
                    spanIds.Add(selectedPath.ObjectId);
                }
            }

            var equipments = neApi.Equipment.GetEquipments(equipmentIds.ToArray());
            foreach (var neEquipment in equipments)
            {
                listOfInventory.Add(neEquipment.Feature);
                if (neEquipment.Structure != null)
                {
                    listOfInventory.Add(neEquipment.Structure.Feature);
                }
            }
            var splices = neApi.SpliceClosures.GetSpliceClosures(spliceIds.ToArray());
            foreach (var splice in splices)
            {
                listOfInventory.Add(splice.Feature);
                if (splice.Structure != null)
                {
                    listOfInventory.Add(splice.Structure.Feature);
                }
            }
            var structures = neApi.Structures.GetStructures(structureIds.ToArray());
            foreach (var structure in structures)
            {
                listOfInventory.Add(structure.Feature);
            }
            var transmedias = neApi.Transmedia.GeTransmedias(transmediaIds.ToArray());
            foreach (var transmedia in transmedias)
            {
                listOfInventory.Add(transmedia.Feature);
            }
            var spans = neApi.Spans.GetSpans(spanIds.ToArray());
            foreach (var span in spans)
            {
                listOfInventory.Add(span.Feature);
            }

            return listOfInventory;
        }

        private void ExportButtonClicked(object sender, EventArgs e)
        {
            var features = GetPathFeatures(true);
            if (features == null)
                return;
            var neInvFeatures = GetInventoryFeature(features);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog
            {
                Title = "Save an Image File",
                Filter = "PNG|*.PNG|All files (*.*)|*.*"
            };
            saveFileDialog1.ShowDialog();
            var allObjs = dgvResults.DataSource as BindingListView<Edge>;

            if (string.IsNullOrEmpty(saveFileDialog1.FileName))
                return;
            // the columns list can be static
            var extent = new ExtentInfo();
            extent.GetExtent(neInvFeatures, saveFileDialog1.FileName);
            extent.CreateWordDoc(allObjs , passedInfo);
        }


        private void GetColumRow()
        {
            var listOfClms = new List<string>();
            for (int i = 0; i < dgvResults.ColumnCount; i++)
            {
                listOfClms.Add(dgvResults.Columns[i].Name);
            }
            var allRows = new List<string>();
            var clmMax = 0;
            for (int i = 0; i < dgvResults.RowCount; i++)
            {
                if (clmMax > listOfClms.Count)
                {
                    return;
                }
                allRows.Add(dgvResults.Rows[i].Cells[clmMax].Value.ToString());
                clmMax++;

            }
            var extent = new ExtentInfo();
            //  extent.CreateWordDoc(listOfClms, allRows);
        }

        private List<NeInventoryFeature> GetInventoryFeature(List<IFeature> features)
        {
            var neApi = NeApiAccess.Instance;

            var allInvFeatures = new List<NeInventoryFeature>();

            foreach (var singleFea in features)
            {
                if (singleFea.Class.AliasName.RemoveSchemaOwner() == "EQUIPMENT")
                {
                    var equip = neApi.Equipment.GetEquipment(singleFea.OID);
                    allInvFeatures.Add(equip);
                    continue;
                }

                if (singleFea.Class.AliasName.RemoveSchemaOwner() == "TRANSMEDIA")
                {
                    var trans = neApi.Transmedia.GetTransmedia(singleFea.OID);
                    allInvFeatures.Add(trans);
                    continue;
                }

                if (singleFea.Class.AliasName.RemoveSchemaOwner() == "SPLICE_CLOSURE")
                {
                    var splice = neApi.SpliceClosures.GetSpliceClosure(singleFea.OID);
                    allInvFeatures.Add(splice);
                    continue;
                }

                if (singleFea.Class.AliasName.RemoveSchemaOwner() == "STRUCTURE")
                {
                    var str = neApi.Structures.GetStructure(singleFea.OID);
                    allInvFeatures.Add(str);
                    continue;
                }

                if (singleFea.Class.AliasName.RemoveSchemaOwner() == "SPAN")
                {
                    var span = neApi.Spans.GetSpan(singleFea.OID);
                    allInvFeatures.Add(span);
                }
            }

            return allInvFeatures;
        }

    }
}
