﻿namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    partial class OutputPathPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutputPathPage));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbPathNames = new System.Windows.Forms.ComboBox();
            this.lblSelectPath = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvResults = new System.Windows.Forms.DataGridView();
            this.tsMenu = new System.Windows.Forms.ToolStrip();
            this.tsbtnSelect = new System.Windows.Forms.ToolStripButton();
            this.tsbtnZoom = new System.Windows.Forms.ToolStripButton();
            this.tsbtnFlash = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnSelectAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnExportPath = new System.Windows.Forms.ToolStripButton();
            this.lblTotalSegments = new System.Windows.Forms.Label();
            this.lblTotalLength = new System.Windows.Forms.Label();
            this.lblSegments = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
            this.tsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblLength);
            this.panel1.Controls.Add(this.lblSegments);
            this.panel1.Controls.Add(this.lblTotalLength);
            this.panel1.Controls.Add(this.lblTotalSegments);
            this.panel1.Controls.Add(this.cmbPathNames);
            this.panel1.Controls.Add(this.lblSelectPath);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(614, 72);
            this.panel1.TabIndex = 6;
            // 
            // cmbPathNames
            // 
            this.cmbPathNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPathNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPathNames.FormattingEnabled = true;
            this.cmbPathNames.Location = new System.Drawing.Point(78, 12);
            this.cmbPathNames.Name = "cmbPathNames";
            this.cmbPathNames.Size = new System.Drawing.Size(531, 21);
            this.cmbPathNames.TabIndex = 7;
            this.cmbPathNames.SelectedIndexChanged += new System.EventHandler(this.PathSelectedIndexChanged);
            // 
            // lblSelectPath
            // 
            this.lblSelectPath.AutoSize = true;
            this.lblSelectPath.Location = new System.Drawing.Point(7, 15);
            this.lblSelectPath.Name = "lblSelectPath";
            this.lblSelectPath.Size = new System.Drawing.Size(65, 13);
            this.lblSelectPath.TabIndex = 6;
            this.lblSelectPath.Text = "Select Path:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvResults);
            this.panel2.Controls.Add(this.tsMenu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(614, 294);
            this.panel2.TabIndex = 7;
            // 
            // dgvResults
            // 
            this.dgvResults.AllowUserToAddRows = false;
            this.dgvResults.AllowUserToDeleteRows = false;
            this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResults.Location = new System.Drawing.Point(0, 25);
            this.dgvResults.Name = "dgvResults";
            this.dgvResults.ReadOnly = true;
            this.dgvResults.Size = new System.Drawing.Size(614, 269);
            this.dgvResults.TabIndex = 5;
            // 
            // tsMenu
            // 
            this.tsMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnSelect,
            this.tsbtnZoom,
            this.tsbtnFlash,
            this.toolStripSeparator1,
            this.tsbtnSelectAll,
            this.toolStripSeparator2,
            this.tsbtnExportPath});
            this.tsMenu.Location = new System.Drawing.Point(0, 0);
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsMenu.Size = new System.Drawing.Size(614, 25);
            this.tsMenu.TabIndex = 4;
            this.tsMenu.Text = "toolStrip1";
            // 
            // tsbtnSelect
            // 
            this.tsbtnSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnSelect.Image")));
            this.tsbtnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSelect.Name = "tsbtnSelect";
            this.tsbtnSelect.Size = new System.Drawing.Size(23, 22);
            this.tsbtnSelect.Text = "Select Segment";
            this.tsbtnSelect.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsbtnSelect.Click += new System.EventHandler(this.SelectPathSegment);
            // 
            // tsbtnZoom
            // 
            this.tsbtnZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnZoom.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnZoom.Image")));
            this.tsbtnZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnZoom.Name = "tsbtnZoom";
            this.tsbtnZoom.Size = new System.Drawing.Size(23, 22);
            this.tsbtnZoom.Text = "Zoom To Segment";
            this.tsbtnZoom.Click += new System.EventHandler(this.ZoomToPathSegment);
            // 
            // tsbtnFlash
            // 
            this.tsbtnFlash.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnFlash.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnFlash.Image")));
            this.tsbtnFlash.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnFlash.Name = "tsbtnFlash";
            this.tsbtnFlash.Size = new System.Drawing.Size(23, 22);
            this.tsbtnFlash.Text = "Flash Segment";
            this.tsbtnFlash.Click += new System.EventHandler(this.FlashPathSegment);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnSelectAll
            // 
            this.tsbtnSelectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnSelectAll.Image")));
            this.tsbtnSelectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSelectAll.Name = "tsbtnSelectAll";
            this.tsbtnSelectAll.Size = new System.Drawing.Size(23, 22);
            this.tsbtnSelectAll.Text = "Select Path";
            this.tsbtnSelectAll.Click += new System.EventHandler(this.SelectAllButtonClicked);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnExportPath
            // 
            this.tsbtnExportPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnExportPath.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnExportPath.Image")));
            this.tsbtnExportPath.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExportPath.Name = "tsbtnExportPath";
            this.tsbtnExportPath.Size = new System.Drawing.Size(23, 22);
            this.tsbtnExportPath.Text = "Export Path";
            this.tsbtnExportPath.Click += new System.EventHandler(this.ExportButtonClicked);
            // 
            // lblTotalSegments
            // 
            this.lblTotalSegments.AutoSize = true;
            this.lblTotalSegments.Location = new System.Drawing.Point(7, 46);
            this.lblTotalSegments.Name = "lblTotalSegments";
            this.lblTotalSegments.Size = new System.Drawing.Size(139, 13);
            this.lblTotalSegments.TabIndex = 8;
            this.lblTotalSegments.Text = "Total Number of Segments: ";
            // 
            // lblTotalLength
            // 
            this.lblTotalLength.AutoSize = true;
            this.lblTotalLength.Location = new System.Drawing.Point(234, 46);
            this.lblTotalLength.Name = "lblTotalLength";
            this.lblTotalLength.Size = new System.Drawing.Size(119, 13);
            this.lblTotalLength.TabIndex = 9;
            this.lblTotalLength.Text = "Total Route Length (m):";
            // 
            // lblSegments
            // 
            this.lblSegments.AutoSize = true;
            this.lblSegments.Location = new System.Drawing.Point(151, 46);
            this.lblSegments.Name = "lblSegments";
            this.lblSegments.Size = new System.Drawing.Size(0, 13);
            this.lblSegments.TabIndex = 10;
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(359, 46);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(0, 13);
            this.lblLength.TabIndex = 11;
            // 
            // OutputPathPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "OutputPathPage";
            this.Size = new System.Drawing.Size(614, 366);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
            this.tsMenu.ResumeLayout(false);
            this.tsMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbPathNames;
        private System.Windows.Forms.Label lblSelectPath;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvResults;
        private System.Windows.Forms.ToolStrip tsMenu;
        private System.Windows.Forms.ToolStripButton tsbtnSelect;
        private System.Windows.Forms.ToolStripButton tsbtnZoom;
        private System.Windows.Forms.ToolStripButton tsbtnFlash;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtnSelectAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbtnExportPath;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.Label lblSegments;
        private System.Windows.Forms.Label lblTotalLength;
        private System.Windows.Forms.Label lblTotalSegments;
    }
}
