﻿using System.Collections.Generic;
using System.Windows.Forms;
using Teklabz.Ne.PathAnalysis.EndUser.DataObjects;
using Wizard;

namespace Teklabz.Ne.PathAnalysis.EndUser.Pages
{
    public partial class SpofDisplay : UserControl, IUserPage
    {
        public SpofDisplay()
        {
            InitializeComponent();
            Control = this;
            IsValid = true;
            PageName = "Single Point of Failure";
            Summary = "Single Point of Failure";
        }

        public UserControl Control { get; }
        public string PageName { get; }
        public string Summary { get; }
        public bool IsValid { get; set; }
        public string InvalidMessage { get; set; }
        public object UserData { get; set; }

      

        public void NextButtonClicked()
        {
         
        }

        public void BackButtonClicked()
        {

        }

        public void FinishButtonClicked()
        {

        }

        public void HelpButtonClicked()
        {

        }

        public bool CurrentPageOnLoad(List<object> allControlsObjects, LastForm lastForm)
        {
            if (allControlsObjects == null)
                return false;

            DgvSpofs.DataSource = null;
            var finalRes = allControlsObjects[2] as List<IntersectionInfo>;
         
            DgvSpofs.DataSource = finalRes;
            return false;  }
    }
}
    

