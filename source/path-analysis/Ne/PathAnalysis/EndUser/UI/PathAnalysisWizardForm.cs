﻿using Teklabz.Ne.PathAnalysis.EndUser.Pages;
using Wizard;

namespace Teklabz.Ne.PathAnalysis.EndUser.UI {
    public partial class PathAnalysisWizardForm : WizardFormTemplate {

        public PathAnalysisWizardForm() {
            InitializeComponent();

            var page1 = new InputSelectionPage();
            AddContentPage(page1);

            var page2 = new OutputPathPage();
            AddContentPage(page2);

            var page3 = new InputForSinglePointOfFailure();
            AddContentPage(page3);

            var page4 = new SpofDisplay();
            AddContentPage(page4);
        }

    }
}
