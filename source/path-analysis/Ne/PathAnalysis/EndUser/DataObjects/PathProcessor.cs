﻿using System;
using System.Collections.Generic;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Teklabz.Ne.PathAnalysis.Admin.AppSettingsConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.TemplateModelConfig.DataObjects;
using Teklabz.Ne.PathAnalysis.Admin.TempNameConfig.DataObjects;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;
using Teklabz.NetworkEngineer.Api.Inventory;
using Teklabz.NetworkEngineer.Api.UI;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects {
    public class PathProcessor {

        public bool IgnoreFromTo;

        public bool UseCable;

        public TemplateNames TemplateName;

        public bool IsEquipment;

        public SelectedFeature FromEquipmentSite;

        public SelectedFeature ToEquipmentSite;

        public  IGeometry InputGeometry;

        public bool UseAttributeAssociations;
        

        private readonly Dictionary<string, List<TemplateModels>> _allowedModels;

        private readonly AppSettings _applicationSettings;


        public PathProcessor(TemplateNames tempName) {
            TemplateName = tempName;

            var appSettings = AppSettingsDbManager.GetAppSettings();
            if (appSettings!=null)
                _applicationSettings = appSettings;

            _allowedModels  = new Dictionary<string, List<TemplateModels>>();

            var models =
                TemplateModelDbManager.FindTemplateModels(string.Format("{0} = '{1}'", TemplateModelDbManager.tempName,
                    TemplateName));
            foreach (var config in models) {
                var key = config.ClassName;
                if (_allowedModels.ContainsKey(key)) {
                    var list = _allowedModels[key];
                    list.Add(config);
                } else {
                    _allowedModels[key] = new List<TemplateModels> {config};
                }
            }
        }


        private List<Route> _routes;

        

        private NeEquipment _startEquipment;

        private NeEquipment _endEquipment;


        public List<Route> GetCableRoutes() {
            _routes = new List<Route>();

            var neApi = NeApiAccess.Instance;
            
            if (IsEquipment) {
                _startEquipment = neApi.Equipment.GetEquipment(FromEquipmentSite.ObjectId);
                _endEquipment = neApi.Equipment.GetEquipment(ToEquipmentSite.ObjectId);
            }

            if (_startEquipment == null)
                return _routes;

            var start = _startEquipment.Feature.ShapeCopy as IProximityOperator;
            if (start == null)
                return _routes;

            _progressBar = GetProgressBar();
            _progressBar.ShowDialog();

            _shortestDistance = start.ReturnDistance(_endEquipment.Feature.ShapeCopy);

            var fromNode = new Node
            {
                ClassName = neApi.Equipment.ObjectClassName,
                DisplayName = _startEquipment.DisplayName,
                ObjectId = _startEquipment.ObjectId
            };

            var allTransmedia = _startEquipment.Transmedias;
            var cables = new List<NeTransmedia>();
            var edges = new List<Edge>();
            foreach (var neTransmedia in allTransmedia) {
                if (!IsFeatureModelConfigured(neTransmedia))
                    continue;

                if (!IsFeatureGeometryIncluded(neTransmedia)) {
                    continue;
                }

                var fromEquipment = neTransmedia.FromEquipment;
                var fromSplice = neTransmedia.FromSpliceClosure;

                var toEquipment = neTransmedia.ToEquipment;
                var toSplice = neTransmedia.ToSpliceClosure;

                if (fromEquipment == null && fromSplice == null) {
                    NeApiAccess.Instance.Logger.LogError(string.Format(
                        "Transmedia is not connected to neither to a From Equipment nor From Splice Closure {0}",
                        neTransmedia.TransmediaName));
                    continue;
                }

                if (toEquipment == null && toSplice == null) {
                    NeApiAccess.Instance.Logger.LogError(string.Format(
                        "Transmedia is not connected to neither to a To Equipment nor To Splice Closure {0}",
                        neTransmedia.TransmediaName));
                    continue;
                }

                var edge = new Edge
                {
                    ClassName = neApi.Transmedia.ObjectClassName,
                    DisplayName = neTransmedia.DisplayName,
                    EdgeName = neTransmedia.TransmediaName,
                    ObjectId = neTransmedia.ObjectId,
                    EdgeLength = Math.Round(((IPolyline)neTransmedia.Feature.ShapeCopy).Length, 3),
                    FromFeature = fromNode
                };
                edges.Add(edge);
                cables.Add(neTransmedia);
            }

            foreach (var neTransmedia in cables) {
                var fromEquipment = neTransmedia.FromEquipment;
                var fromSplice = neTransmedia.FromSpliceClosure;

                var toEquipment = neTransmedia.ToEquipment;
                var toSplice = neTransmedia.ToSpliceClosure;

                if (fromEquipment == null && fromSplice == null) {
                    continue;
                }

                if (toEquipment == null && toSplice == null) {
                    continue;
                }

                var route = new Route {IsCable = true};

                route.AddNode(fromNode);

                var edge = new Edge
                {
                    ClassName = neApi.Transmedia.ObjectClassName,
                    DisplayName = neTransmedia.DisplayName,
                    EdgeName = neTransmedia.TransmediaName,
                    ObjectId = neTransmedia.ObjectId,
                    EdgeLength = Math.Round(((IPolyline)neTransmedia.Feature.ShapeCopy).Length, 3),
                    FromFeature = fromNode
                };
                if (fromEquipment != null) {
                    edge.FromEquipmentName = fromEquipment.EquipmentName;
                    if (fromEquipment.Structure != null) {
                        edge.FromStructureName = fromEquipment.Structure.StructureName;
                    }
                }
                if (fromSplice != null) {
                    edge.FromSpliceName = fromSplice.SpliceClosureName;
                    if (fromSplice.Structure != null) {
                        edge.FromStructureName = fromSplice.Structure.StructureName;
                    }
                }
                if (toEquipment != null) {
                    edge.ToEquipmentName = toEquipment.EquipmentName;
                    if (toEquipment.Structure != null) {
                        edge.ToStructureName = toEquipment.Structure.StructureName;
                    }
                }
                if (toSplice != null) {
                    edge.ToSpliceName = toSplice.SpliceClosureName;
                    if (toSplice.Structure != null) {
                        edge.ToStructureName = toSplice.Structure.StructureName;
                    }
                }

                route.AddEdge(edge);

                NeInventoryFeature nextFeature = null;
                if (UseAttributeAssociations) {
                    if (fromEquipment != null && fromEquipment == _startEquipment) {
                        if (toSplice != null)
                            nextFeature = toSplice;
                        else {
                            nextFeature = toEquipment;
                        }
                    }

                    if (toEquipment != null && toEquipment == _startEquipment) {
                        if (fromSplice != null)
                            nextFeature = fromSplice;
                        else {
                            nextFeature = fromEquipment;
                        }
                    }

                    if (nextFeature == null)
                        continue;
                } else {
                    nextFeature = GetFeatureGeographically(neTransmedia, _startEquipment);
                    if (nextFeature == null)
                        continue;
                }

                var toNode = new Node
                {
                    ClassName = nextFeature.ObjectClassName,
                    DisplayName = nextFeature.DisplayName,
                    ObjectId = nextFeature.ObjectId
                };
                edge.ToFeature = toNode;

                if (!IsFeatureModelConfigured(nextFeature))
                    continue;

                TraceCableRoute(edges, nextFeature,route);
            }

            _progressBar.CloseDialog();

            _routes.Sort(Route.CompareShortest);
            return _routes;
        }
        
        private void TraceCableRoute(List<Edge> previousEdges, NeInventoryFeature thisFeature, Route route) {
            var neApi = NeApiAccess.Instance;

            var currentRoute = new Route
            {
                Edges = new List<Edge>(route.Edges),
                VisitedNodes = new List<Node>(route.VisitedNodes),
                RouteDistance = route.RouteDistance
            };

            if (_progressBar.Canceled) {
                return;
            }

            if (route.Edges.Count > _applicationSettings.TelecomMaxNoOfSegments)
                return;

            if (route.RouteDistance > _applicationSettings.TelecomDistanceToPath * _shortestDistance)
                return;

            if (_routes.Count >= _applicationSettings.TelecomMaxNoOfRoutes)
                return;

            _progressBar.Message = string.Format("Analyzing {0} : {1} {2}(Total Number of Paths Found: {3})",
                thisFeature.ObjectClassName,
                thisFeature.DisplayName, Environment.NewLine, _routes.Count);

            try {

                var startingTransmedia = new List<NeTransmedia>();
                if (thisFeature is NeEquipment) {
                    return;
                }

                if (thisFeature is NeSpliceClosure) {
                    startingTransmedia = ((NeSpliceClosure)thisFeature).Transmedias;
                }

                var fromNode = new Node
                {
                    ClassName = thisFeature.ObjectClassName,
                    DisplayName = thisFeature.DisplayName,
                    ObjectId = thisFeature.ObjectId
                };
                if (route.VisitedNodes.Contains(fromNode))
                    return;

                route.AddNode(fromNode);

                var cables = new List<NeTransmedia>();
                var edges = new List<Edge>();
                foreach (var neTransmedia in startingTransmedia) {
                    if (!IsFeatureModelConfigured(neTransmedia))
                        continue;
                    
                    if (!IsFeatureGeometryIncluded(neTransmedia)) {
                        continue;
                    }

                    var fromEquipment = neTransmedia.FromEquipment;
                    var fromSplice = neTransmedia.FromSpliceClosure;

                    var toEquipment = neTransmedia.ToEquipment;
                    var toSplice = neTransmedia.ToSpliceClosure;

                    if (fromEquipment == null && fromSplice == null) {
                        NeApiAccess.Instance.Logger.LogError(string.Format(
                            "Transmedia is not connected to neither to a From Equipment nor From Splice Closure {0}",
                            neTransmedia.TransmediaName));
                        continue;
                    }

                    if (toEquipment == null && toSplice == null) {
                        NeApiAccess.Instance.Logger.LogError(string.Format(
                            "Transmedia is not connected to neither to a To Equipment nor To Splice Closure {0}",
                            neTransmedia.TransmediaName));
                        continue;
                    }
                    
                    var edge = new Edge
                    {
                        ClassName = neApi.Transmedia.ObjectClassName,
                        DisplayName = neTransmedia.DisplayName,
                        EdgeName = neTransmedia.TransmediaName,
                        ObjectId = neTransmedia.ObjectId,
                        EdgeLength = Math.Round(((IPolyline)neTransmedia.Feature.ShapeCopy).Length, 3),
                        FromFeature = fromNode
                    };
                    
                    if(previousEdges.Contains(edge))
                        continue;
                    cables.Add(neTransmedia);
                    edges.Add(edge);
                }

                foreach (var neTransmedia in cables) {
                    var fromEquipment = neTransmedia.FromEquipment;
                    var fromSplice = neTransmedia.FromSpliceClosure;

                    var toEquipment = neTransmedia.ToEquipment;
                    var toSplice = neTransmedia.ToSpliceClosure;

                    if (fromEquipment == null && fromSplice == null) {
                        continue;
                    }

                    if (toEquipment == null && toSplice == null) {
                        continue;
                    }

                    var edge = new Edge
                    {
                        ClassName = neApi.Transmedia.ObjectClassName,
                        DisplayName = neTransmedia.DisplayName,
                        EdgeName = neTransmedia.TransmediaName,
                        ObjectId = neTransmedia.ObjectId,
                        EdgeLength = Math.Round(((IPolyline)neTransmedia.Feature.ShapeCopy).Length, 3),
                        FromFeature = fromNode
                    };
                    if (fromEquipment != null) {
                        edge.FromEquipmentName = fromEquipment.EquipmentName;
                        if (fromEquipment.Structure != null) {
                            edge.FromStructureName = fromEquipment.Structure.StructureName;
                        }
                    }

                    if (fromSplice != null) {
                        edge.FromSpliceName = fromSplice.SpliceClosureName;
                        if (fromSplice.Structure != null) {
                            edge.FromStructureName = fromSplice.Structure.StructureName;
                        }
                    }

                    if (toEquipment != null) {
                        edge.ToEquipmentName = toEquipment.EquipmentName;
                        if (toEquipment.Structure != null) {
                            edge.ToStructureName = toEquipment.Structure.StructureName;
                        }
                    }

                    if (toSplice != null) {
                        edge.ToSpliceName = toSplice.SpliceClosureName;
                        if (toSplice.Structure != null) {
                            edge.ToStructureName = toSplice.Structure.StructureName;
                        }
                    }

                    route.AddEdge(edge);

                    NeInventoryFeature nextFeature = null;

                    if (UseAttributeAssociations) {
                        if (thisFeature is NeEquipment) {
                            if (fromEquipment != null && fromEquipment == ((NeEquipment)thisFeature)) {
                                if (toSplice != null)
                                    nextFeature = toSplice;
                                else {
                                    nextFeature = toEquipment;
                                }
                            }

                            if (toEquipment != null && toEquipment == ((NeEquipment)thisFeature)) {
                                if (fromSplice != null)
                                    nextFeature = fromSplice;
                                else {
                                    nextFeature = fromEquipment;
                                }
                            }
                        }

                        if (thisFeature is NeSpliceClosure) {
                            if (fromSplice != null && fromSplice == ((NeSpliceClosure)thisFeature)) {
                                if (toSplice != null)
                                    nextFeature = toSplice;
                                else {
                                    nextFeature = toEquipment;
                                }
                            }

                            if (toSplice != null && toSplice == ((NeSpliceClosure)thisFeature)) {
                                if (fromSplice != null)
                                    nextFeature = fromSplice;
                                else {
                                    nextFeature = fromEquipment;
                                }
                            }
                        }

                        if (nextFeature == null)
                            continue;
                    } else {
                        nextFeature = GetFeatureGeographically(neTransmedia, thisFeature);
                        if (nextFeature == null)
                            continue;
                    }

                    var toNode = new Node
                    {
                        ClassName = nextFeature.ObjectClassName,
                        DisplayName = nextFeature.DisplayName,
                        ObjectId = nextFeature.ObjectId
                    };
                    edge.ToFeature = toNode;

                    if (!IsFeatureModelConfigured(nextFeature))
                        continue;

                    TraceCableRoute(edges, nextFeature, route);

                    //Check if this route is valid before adding
                    if (route.Edges.Count >= 2) {
                        var firstEdge = route.Edges[0];
                        var lastEdge = route.Edges[route.Edges.Count - 1];

                        var firstEdgeValid = false;
                        var lastEdgeValid = false;
                        if (firstEdge != null) {
                            var fromFeature = firstEdge.FromFeature;
                            var toFeature = firstEdge.ToFeature;
                            
                            if (_startEquipment!=null && fromFeature.ClassName == neApi.Equipment.ObjectClassName &&
                                fromFeature.ObjectId == _startEquipment.ObjectId) {
                                firstEdgeValid = true;
                            }
                            if (_startEquipment!=null && toFeature.ClassName == neApi.Equipment.ObjectClassName &&
                                toFeature.ObjectId == _startEquipment.ObjectId) {
                                firstEdgeValid = true;
                            }
                        }

                        if (lastEdge != null) {
                            var fromFeature = lastEdge.FromFeature;
                            var toFeature = lastEdge.ToFeature;
                            
                            if (_endEquipment!=null && fromFeature.ClassName == neApi.Equipment.ObjectClassName &&
                                fromFeature.ObjectId == _endEquipment.ObjectId) {
                                lastEdgeValid = true;
                            }
                            if (_endEquipment!=null && toFeature.ClassName == neApi.Equipment.ObjectClassName &&
                                toFeature.ObjectId == _endEquipment.ObjectId) {
                                lastEdgeValid = true;
                            }
                        }

                        if (firstEdgeValid && lastEdgeValid && !_routes.Contains(route)) {
                            _routes.Add(route);
                        }
                    }
                    
                    route = new Route
                    {
                        Edges = new List<Edge>(currentRoute.Edges),
                        VisitedNodes = new List<Node>(currentRoute.VisitedNodes),
                        RouteDistance = currentRoute.RouteDistance
                    };
                }
            } catch (Exception exp) {
                NeApiAccess.Instance.Logger.LogError("Error occurred when trying to get the cable routes.", exp);
            }
        }


        private NeStructure _startStructure;

        private NeStructure _endStructure;

        private double _shortestDistance;

        private NeProgressBar _progressBar;
        
        public List<Route> GetSpanRoutes() {
            _routes = new List<Route>();

            var neApi = NeApiAccess.Instance;
            
            if (!IsEquipment) {
                _startStructure = neApi.Structures.GetStructure(FromEquipmentSite.ObjectId);
                _endStructure = neApi.Structures.GetStructure(ToEquipmentSite.ObjectId);
            }

            if (_startStructure == null || _endStructure==null)
                return _routes;

            var start = _startStructure.Feature.ShapeCopy as IProximityOperator;
            if (start == null)
                return _routes;

            _progressBar = GetProgressBar();
            _progressBar.ShowDialog();

            _shortestDistance = start.ReturnDistance(_endStructure.Feature.ShapeCopy);

            var fromNode = new Node
            {
                ClassName = neApi.Structures.ObjectClassName,
                DisplayName = _startStructure.DisplayName,
                ObjectId = _startStructure.ObjectId
            };

            var allSpans = _startStructure.Spans;
            var spans = new List<NeSpan>();
            var edges = new List<Edge>();
            foreach (var span in allSpans) {
                if (!IsFeatureModelConfigured(span))
                    continue;

                if (!IsFeatureGeometryIncluded(span)) {
                    continue;
                }

                var fromStructure = span.FromStructure;
                var toStructure = span.ToStructure;
                
                if (fromStructure == null) {
                    NeApiAccess.Instance.Logger.LogError(string.Format(
                        "Span is not connected to neither to a From Structure {0}",
                        span.SpanName));
                    continue;
                }

                if (toStructure == null) {
                    NeApiAccess.Instance.Logger.LogError(string.Format(
                        "Span is not connected to neither to a To Structure {0}",
                        span.SpanName));
                    continue;
                }

                var edge = new Edge
                {
                    ClassName = neApi.Spans.ObjectClassName,
                    DisplayName = span.DisplayName,
                    EdgeName = span.SpanName,
                    ObjectId = span.ObjectId,
                    EdgeLength = Math.Round(((IPolyline)span.Feature.ShapeCopy).Length, 3),
                    FromFeature = fromNode
                };
                edges.Add(edge);
                spans.Add(span);
            }

            foreach (var span in spans) {
                var fromStructure = span.FromStructure;
                var toStructure = span.ToStructure;
                
                if (fromStructure == null) {
                    continue;
                }

                if (toStructure == null) {
                    continue;
                }

                var route = new Route {IsCable = false};
                route.AddNode(fromNode);

                var edge = new Edge
                {
                    FromStructureName =  fromStructure.StructureName,
                    ToStructureName = toStructure.StructureName,
                    ClassName = neApi.Spans.ObjectClassName,
                    DisplayName = span.DisplayName,
                    EdgeName = span.SpanName,
                    ObjectId = span.ObjectId,
                    EdgeLength = Math.Round(((IPolyline)span.Feature.ShapeCopy).Length, 3),
                    FromFeature = fromNode
                };
                route.AddEdge(edge);

                NeInventoryFeature nextFeature = null;
                if (UseAttributeAssociations) {
                    if (fromStructure == _startStructure) {
                        nextFeature = toStructure;
                    }
                    if (toStructure == _startStructure) {
                        nextFeature = fromStructure;
                    }
                    if (nextFeature == null)
                        continue;
                } else {
                    nextFeature = GetFeatureGeographically(span, _startStructure);
                    if (nextFeature == null)
                        continue;
                }

                var toNode = new Node
                {
                    ClassName = nextFeature.ObjectClassName,
                    DisplayName = nextFeature.DisplayName,
                    ObjectId = nextFeature.ObjectId
                };
                edge.ToFeature = toNode;

                if (!IsFeatureModelConfigured(nextFeature))
                    continue;

                TraceSpanRoute(edges, nextFeature,route);
            }

            _progressBar.CloseDialog();

            _routes.Sort(Route.CompareShortest);
            return _routes;
        }
        
        private void TraceSpanRoute(List<Edge> previousEdges, NeInventoryFeature thisFeature, Route route) {
            GC.Collect();

            var neApi = NeApiAccess.Instance;

            var currentRoute = new Route
            {
                Edges = new List<Edge>(route.Edges),
                VisitedNodes = new List<Node>(route.VisitedNodes),
                RouteDistance = route.RouteDistance
            };

            try {

                if (_progressBar.Canceled) {
                    return;
                }

                if (route.Edges.Count > _applicationSettings.CivilMaxNoOfSegments)
                    return;

                if (route.RouteDistance > _applicationSettings.CivilDistanceToPath * _shortestDistance)
                    return;

                if (_routes.Count >= _applicationSettings.CivilMaxNoOfRoutes)
                    return;

                _progressBar.Message = string.Format("Analyzing {0} : {1} {2}(Total Number of Paths Found: {3})",
                    thisFeature.ObjectClassName,
                    thisFeature.DisplayName, Environment.NewLine, _routes.Count);

                var startingSpans = new List<NeSpan>();
                if (thisFeature is NeStructure) {
                    if (_endStructure != null && _endStructure == ((NeStructure)thisFeature))
                        return;

                    startingSpans = ((NeStructure)thisFeature).Spans;
                }

                var fromNode = new Node
                {
                    ClassName = thisFeature.ObjectClassName,
                    DisplayName = thisFeature.DisplayName,
                    ObjectId = thisFeature.ObjectId
                };
                if (route.VisitedNodes.Contains(fromNode))
                    return;

                route.AddNode(fromNode);

                var spans = new List<NeSpan>();
                var edges = new List<Edge>();
                foreach (var neSpan in startingSpans) {
                    if (!IsFeatureModelConfigured(neSpan))
                        continue;

                    if (!IsFeatureGeometryIncluded(neSpan)) {
                        continue;
                    }
                    
                    var fromStructure = neSpan.FromStructure;
                    var toStructure = neSpan.ToStructure;
                
                    if (fromStructure == null) {
                        NeApiAccess.Instance.Logger.LogError(string.Format(
                            "Span is not connected to neither to a From Structure {0}",
                            neSpan.SpanName));
                        continue;
                    }

                    if (toStructure == null) {
                        NeApiAccess.Instance.Logger.LogError(string.Format(
                            "Span is not connected to neither to a To Structure {0}",
                            neSpan.SpanName));
                        continue;
                    }
                    
                    var edge = new Edge
                    {
                        ClassName = neApi.Spans.ObjectClassName,
                        DisplayName = neSpan.DisplayName,
                        EdgeName = neSpan.SpanName,
                        ObjectId = neSpan.ObjectId,
                        EdgeLength = Math.Round(((IPolyline)neSpan.Feature.ShapeCopy).Length, 3),
                        FromFeature = fromNode
                    };
                    
                    if(previousEdges.Contains(edge))
                        continue;
                    spans.Add(neSpan);
                    edges.Add(edge);
                }

                foreach (var neSpan in spans) {
                    var fromStructure = neSpan.FromStructure;
                    var toStructure = neSpan.ToStructure;

                    if (fromStructure == null) {
                        continue;
                    }

                    if (toStructure == null) {
                        continue;
                    }
                    
                    var edge = new Edge
                    {
                        FromStructureName = fromStructure.StructureName,
                        ToStructureName =  toStructure.StructureName,
                        ClassName = neApi.Spans.ObjectClassName,
                        DisplayName = neSpan.DisplayName,
                        EdgeName = neSpan.SpanName,
                        ObjectId = neSpan.ObjectId,
                        EdgeLength = Math.Round(((IPolyline)neSpan.Feature.ShapeCopy).Length, 3),
                        FromFeature = fromNode
                    };
                    
                    route.AddEdge(edge);
                    
                    NeInventoryFeature nextFeature = null;
                    if (UseAttributeAssociations) {
                        if (fromStructure == thisFeature) {
                            nextFeature = toStructure;
                        }
                        if (toStructure == thisFeature) {
                            nextFeature = fromStructure;
                        }
                        if (nextFeature == null)
                            continue;
                    } else {
                        nextFeature = GetFeatureGeographically(neSpan, thisFeature);
                        if (nextFeature == null)
                            continue;
                    }
                    
                    var toNode = new Node
                    {
                        ClassName = nextFeature.ObjectClassName,
                        DisplayName = nextFeature.DisplayName,
                        ObjectId = nextFeature.ObjectId
                    };
                    edge.ToFeature = toNode;

                    if (_endStructure != ((NeStructure)nextFeature)) {
                        if (!IsFeatureModelConfigured(nextFeature))
                            continue;
                    }
                    
                    TraceSpanRoute(edges, nextFeature, route);

                    //Check if this route is valid before adding
                    if (route.Edges.Count >= 2) {
                        var firstEdge = route.Edges[0];
                        var lastEdge = route.Edges[route.Edges.Count - 1];

                        var firstEdgeValid = false;
                        var lastEdgeValid = false;
                        if (firstEdge != null) {
                            var fromFeature = firstEdge.FromFeature;
                            var toFeature = firstEdge.ToFeature;
                            
                            if (_startStructure!=null && fromFeature.ClassName == neApi.Structures.ObjectClassName &&
                                fromFeature.ObjectId == _startStructure.ObjectId) {
                                firstEdgeValid = true;
                            }
                            if (_startStructure!=null && toFeature.ClassName == neApi.Structures.ObjectClassName &&
                                toFeature.ObjectId == _startStructure.ObjectId) {
                                firstEdgeValid = true;
                            }
                        }

                        if (lastEdge != null) {
                            var fromFeature = lastEdge.FromFeature;
                            var toFeature = lastEdge.ToFeature;
                            
                            if (_endStructure!=null && fromFeature.ClassName == neApi.Structures.ObjectClassName &&
                                fromFeature.ObjectId == _endStructure.ObjectId) {
                                lastEdgeValid = true;
                            }
                            if (_endStructure!=null && toFeature.ClassName == neApi.Structures.ObjectClassName &&
                                toFeature.ObjectId == _endStructure.ObjectId) {
                                lastEdgeValid = true;
                            }
                        }

                        if (firstEdgeValid && lastEdgeValid && !_routes.Contains(route)) {
                            _routes.Add(route);
                        }
                    }

                    route = new Route
                    {
                        Edges = new List<Edge>(currentRoute.Edges),
                        VisitedNodes = new List<Node>(currentRoute.VisitedNodes),
                        RouteDistance = currentRoute.RouteDistance
                    };
                }
            } catch (Exception exp) {
                NeApiAccess.Instance.Logger.LogError("Error occurred when trying to get the span routes.", exp);
            }
        }
        
        private bool IsFeatureModelConfigured(NeInventoryFeature feature) {
            var featureClassName = feature.ObjectClassName;
            var categoryName = feature.CategoryName;
            var typeName = feature.TypeName;
            
            string modelName;
            var model = feature.Model;
            if (model != null)
                modelName = model.ModelName;
            else
                modelName = feature.RefName;
            
            if (string.IsNullOrEmpty(featureClassName))
                return false;

            if(!_allowedModels.ContainsKey(featureClassName.ToUpper()))
                return false;

            var configuredModels = _allowedModels[featureClassName.ToUpper()];

            var anyCategory = configuredModels.Find(x => x.CategoryName == "ANY");
            if (anyCategory != null)
                return true;

            var categories = configuredModels.FindAll(x => string.Equals(x.CategoryName, categoryName));
            if (categories.Count <= 0)
                return false;

            var anyType = categories.Find(x => x.TypeName == "ANY");
            if (anyType != null)
                return true;

            var types = categories.FindAll(x => string.Equals(x.TypeName, typeName));
            if (types.Count <= 0)
                return false;

            var anyModel = types.Find(x => x.ModelName == "ANY");
            if (anyModel != null)
                return true;


            var models = categories.FindAll(x => string.Equals(x.ModelName, modelName));
            if (models.Count <= 0)
                return false;

            return true;
        }

        private bool IsFeatureGeometryIncluded(NeInventoryFeature feature) {
            if (InputGeometry == null)
                return true;

            var topological = InputGeometry as ITopologicalOperator;
            if (topological == null)
                return true;

            var featureGeometry = feature.Geometry;
            var intersect = topological.Intersect(featureGeometry, esriGeometryDimension.esriGeometryNoDimension);
            if (intersect == null)
                return false;

            if (intersect.IsEmpty)
                return false;

            return true;
        }

        private NeProgressBar GetProgressBar() {
            if (_progressBar != null)
                return _progressBar;

            const string progressBarName = "TRC-PB";
            _progressBar = NeProgressBarFactory.Instance.GetNamedProgressBar(progressBarName);
            if (_progressBar == null) {
                _progressBar = NeProgressBarFactory.Instance.CreateNamedProgressBar(progressBarName);
                _progressBar.Title = "Analyzing Routes...";
                _progressBar.CancelEnabled = true;
            }

            return _progressBar;
        }

        private NeInventoryFeature GetFeatureGeographically(NeInventoryFeature segment, NeInventoryFeature previousFeature) {
            var neApi = NeApiAccess.Instance;
            var segmentShape = segment.Feature.ShapeCopy as IPolyline;
            if (segmentShape == null)
                return null;

            var fromPoint = segmentShape.FromPoint;
            var toPoint = segmentShape.ToPoint;

            if (UseCable) {
                var fromSplices =
                    neApi.SpliceClosures.FindSpliceClosures("", esriSpatialRelEnum.esriSpatialRelIntersects, fromPoint);
                var fromEquipments =
                    neApi.Equipment.FindEquipments("", esriSpatialRelEnum.esriSpatialRelIntersects, fromPoint);

                var toSplices =
                    neApi.SpliceClosures.FindSpliceClosures("", esriSpatialRelEnum.esriSpatialRelIntersects, toPoint);
                var toEquipments =
                    neApi.Equipment.FindEquipments("", esriSpatialRelEnum.esriSpatialRelIntersects, toPoint);

                var fromInventory = new List<NeInventoryFeature>();
                fromInventory.AddRange(fromSplices);
                fromInventory.AddRange(fromEquipments);

                if (fromInventory.Contains(previousFeature)) {
                    fromInventory.Remove(previousFeature);
                }

                var toInventory = new List<NeInventoryFeature>();
                toInventory.AddRange(toSplices);
                toInventory.AddRange(toEquipments);

                if (toInventory.Contains(previousFeature)) {
                    toInventory.Remove(previousFeature);
                }

                if (fromInventory.Count + toInventory.Count != 1) {
                    return null;
                }

                if (fromInventory.Count == 1)
                    return fromInventory[0];

                return toInventory[0];
            } else {
                var fromStructures =
                    neApi.Structures.FindStructures("", esriSpatialRelEnum.esriSpatialRelIntersects, fromPoint);

                var toStructures =
                    neApi.Structures.FindStructures("", esriSpatialRelEnum.esriSpatialRelIntersects, toPoint);

                var previousStructure = previousFeature as NeStructure;
                if (previousStructure == null)
                    return null;

                if (fromStructures.Contains(previousStructure)) {
                    fromStructures.Remove(previousStructure);
                }

                if (toStructures.Contains(previousStructure)) {
                    toStructures.Remove(previousStructure);
                }

                if (fromStructures.Count + toStructures.Count != 1) {
                    return null;
                }

                if (fromStructures.Count == 1)
                    return fromStructures[0];

                return toStructures[0];
            }
        }

    }
}
