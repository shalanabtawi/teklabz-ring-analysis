﻿using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;
using Teklabz.NetworkEngineer.Api.Inventory;
using Teklabz.NetworkEngineer.Api.UI;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class SpofProcessor
    {

        public const string ProgressBarName = "PRG-SPOF";

        public List<Route> SelectedRoutes;

        public double BufferDistance;

        public double SharedDistance;

        public double SpofMinimumSharedDistance;



        public List<SpofResult> AnalyzeSpanRoutes(List<Route> spofUnits, double bufferDistance, double spofMinimumSharedDistance)
        {
            var results = new List<SpofResult>();

            var progressBar = GetProgressBar();
            progressBar.CancelEnabled = false;
            progressBar.MaxValue = spofUnits.Count * 2;
            progressBar.Title = "Single Point of Failure";
            progressBar.StepValue = 1;
            progressBar.CurrentValue = 0;
            progressBar.ShowDialog();

            var count = spofUnits.Count;

            for (var i = 0; i < count; i++)
            {
                var baseSpofUnit = spofUnits[i];

                for (var j = 0; j < count; j++)
                {
                    if (j <= i)
                    {
                        progressBar.Step();
                        continue;
                    }
                    var compareSpofUnit = spofUnits[j];

                    progressBar.Message =
                        string.Format("Calculating Single Point of Failure between Routes: {0} and {1}",
                            baseSpofUnit.RouteNumber, compareSpofUnit.RouteNumber);

                    results.AddRange(GetSpofSpans(baseSpofUnit, compareSpofUnit));

                    progressBar.Step();
                }
            }

            return results;
        }

        private IEnumerable<SpofResult> GetSpofSpans(Route routeOne, Route routeTwo)
        {
            var returnList = new List<SpofResult>();

            try
            {

                var baseSpans = GetSpansFromRoute(routeOne);
                var compareSpans = GetSpansFromRoute(routeTwo);

                var equals = true;
                var length = 0.0;
                var features = new List<IFeature>();
                foreach (var baseSpan in baseSpans)
                {
                    if (compareSpans.Contains(baseSpan))
                    {
                        length = Math.Round(((IPolyline)baseSpan.Feature.ShapeCopy).Length + length, 3);
                        features.Add(baseSpan.Feature);
                        continue;
                    }
                    equals = false;
                    break;
                }

                if (equals && baseSpans.Count > 0)
                {

                    return returnList;
                }

                return returnList;
            }
            catch (Exception ex)
            {
                NeApiAccess.Instance.Logger.ShowErrorDialog("An error occurred when analyzing the single point of failures.", ex);
                return new List<SpofResult>();
            }
        }

        private List<NeSpan> GetSpansFromRoute(Route route)
        {
            var neApi = NeApiAccess.Instance;
            var returnSpans = new List<NeSpan>();

            var isCable = route.IsCable;
            if (!isCable)
            {
                var spanIds = new List<int>();
                foreach (var edge in route.Edges)
                {
                    spanIds.Add(edge.ObjectId);
                }

                returnSpans = neApi.Spans.GetSpans(spanIds.ToArray());
                return returnSpans;
            }

            var cableIds = new List<int>();
            foreach (var edge in route.Edges)
            {
                cableIds.Add(edge.ObjectId);
            }

            var cables = neApi.Transmedia.GeTransmedias(cableIds.ToArray());
            foreach (var segment in cables)
            {
                returnSpans.AddRange(GetAssociatedSpans(segment));
            }
            return returnSpans;
        }

        private List<NeSpan> GetAssociatedSpans(NeTransmedia transmedia)
        {

            var neApi = NeApiAccess.Instance;
            var spansOids = new List<int>();

            var transmediaShape = transmedia.Feature.ShapeCopy;
            if (transmediaShape == null)
                return new List<NeSpan>();

            var transmediaTopo = (ITopologicalOperator)transmediaShape;

            var bufferedShape = transmediaTopo.Buffer(BufferDistance);
            if (bufferedShape == null)
                return new List<NeSpan>();

            var bufferedTopo = (ITopologicalOperator)bufferedShape;

            var spanTable = neApi.Database.GetNeFeatureClass(neApi.Spans.ObjectClassName);
            if (spanTable == null)
                return new List<NeSpan>();

            var spatialFilter = new SpatialFilterClass
            {
                Geometry = bufferedShape,
                SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects
            };

            IFeature feature = null;
            IFeatureCursor cursor = null;
            try
            {
                cursor = spanTable.Search(spatialFilter, false);
                while ((feature = cursor.NextFeature()) != null)
                {
                    //Check the minimum shared distance 
                    var spanShape = feature.ShapeCopy;
                    var intersection = bufferedTopo.Intersect(spanShape, esriGeometryDimension.esriGeometryNoDimension);
                    if (intersection == null)
                        continue;

                    var intersectionLine = intersection as IPolyline;
                    if (intersectionLine == null)
                        continue;

                    var intersectionDistance = intersectionLine.Length;
                    if (intersectionDistance < SharedDistance)
                        continue;

                    if (spansOids.Contains(feature.OID))
                        continue;

                    spansOids.Add(feature.OID);
                }

                return neApi.Spans.GetSpans(spansOids.ToArray());
            }
            catch (Exception ex)
            {
                neApi.Logger.LogError("An error occurred when trying to get spans from transmedia shape.", ex);
                return new List<NeSpan>();
            }
            finally
            {
                if (feature != null)
                {
                    Marshal.ReleaseComObject(feature);
                }

                if (cursor != null)
                {
                    Marshal.ReleaseComObject(cursor);
                }
            }
        }


        public static List<IntersectionInfo> FindFirstSpof(SpofProcessor crntPaths)
        {
            var listOfIntersections = new List<IntersectionInfo>();
            try
            {
                var allRoutes = crntPaths.SelectedRoutes;
               
                for (int i = 0; i < allRoutes.Count; i++)
                {
                    var firstRoute = allRoutes[i];
                    foreach (var singleRoute in allRoutes)
                    {
                        if (firstRoute.RouteNumber == singleRoute.RouteNumber)
                        {
                            continue;
                        }
                        var allLines = singleRoute.Edges as List<Edge>;
                        var allNodes = singleRoute.VisitedNodes;
                        
                        if (singleRoute.IsCable)
                        {
                            var crntPathLins = GetTransmedias(allLines);
                            var otherPathLines = GetTransmedias(firstRoute.Edges);
                            var foundInts = CheckOffsetIntersection(crntPathLins, otherPathLines, crntPaths.BufferDistance, crntPaths.SpofMinimumSharedDistance);
                            if (foundInts.Count != 0)
                                listOfIntersections.AddRange(foundInts);
                        }
                        else
                        {
                            var crntPathLins = GetSpans(allLines);
                            var otherPathLines = GetSpans(firstRoute.Edges);
                            var foundInts = CheckOffsetIntersection(crntPathLins, otherPathLines, crntPaths.BufferDistance, crntPaths.SpofMinimumSharedDistance);
                            if (foundInts.Count != 0)
                                listOfIntersections.AddRange(foundInts);
                        }
                        var nodesInt = CheckNodesIntersection(allNodes, firstRoute.VisitedNodes);
                        if (nodesInt.Count != 0)
                            listOfIntersections.AddRange(nodesInt);
                    }
                }
                return listOfIntersections; }
            catch(Exception ex)
            {
                NeApiAccess.Instance.Logger.LogError("Error while trying to find SPOF", ex);
            }

            return listOfIntersections;
        }

        private static List<IntersectionInfo> CheckNodesIntersection(List<Node> allNodes , List<Node>  otherPathNodes)
        {
            var intersectingNodes = new List<IntersectionInfo>();
            for (int i = 0; i < otherPathNodes.Count; i++)
            {
                foreach (var singleNode in allNodes)
                {
                    var geo = GetNodeGeometry(otherPathNodes[i]).Geometry;
                    ITopologicalOperator topo = geo as ITopologicalOperator;
                    if (otherPathNodes[i].ObjectId == singleNode.ObjectId)
                    {
                        var geom = GetNodeGeometry(singleNode).Geometry;
                        var intersection = topo.Intersect(geom, esriGeometryDimension.esriGeometryNoDimension);
                        var foundInter = new IntersectionInfo(otherPathNodes[i].ObjectId, singleNode.ObjectId, intersection);
                        foundInter.IntersectionTypeProp = IntersectionInfo.Types.IntersType.sameFeature;
                        if (!intersectingNodes.Contains(foundInter) && !intersectingNodes.Exists(x => x.FirstId == singleNode.ObjectId && x.SecondId == otherPathNodes[i].ObjectId))
                            intersectingNodes.Add(foundInter);
                        continue;
                    }
                    
                    
                    if (geo != null)
                    {
                        var geom = GetNodeGeometry(singleNode).Geometry;
                        var intersection = topo.Intersect(geom, esriGeometryDimension.esriGeometryNoDimension);
                        if (intersection != null)
                        {
                            if (!intersection.IsEmpty)
                            {
                                var foundInter = new IntersectionInfo(otherPathNodes[i].ObjectId, singleNode.ObjectId, intersection);
                                foundInter.IntersectionTypeProp = IntersectionInfo.Types.IntersType.overlap;
                                if (!intersectingNodes.Contains(foundInter) && !intersectingNodes.Exists(x => x.FirstId == singleNode.ObjectId && x.SecondId == otherPathNodes[i].ObjectId))
                                    intersectingNodes.Add(foundInter);
                            }
                          
                        }

                    }
                }
          
            }
            return intersectingNodes;
        }

        private static NeInventoryFeature GetNodeGeometry(Node node)
        {
            if (node.ClassName == "EQUIPMENT")
            {
                return NeApiAccess.Instance.Equipment.GetEquipment(node.ObjectId);
            }
            if (node.ClassName == "STRUCTURE")
            {
                return NeApiAccess.Instance.Structures.GetStructure(node.ObjectId);
            }
            if (node.ClassName == "SPLICE_CLOSURE")
            {
                return NeApiAccess.Instance.SpliceClosures.GetSpliceClosure(node.ObjectId);
            }
            return null;  }

        private static List<NeInventoryFeature> GetSpans(List<Edge> allLines)
        {
            var foundSpans = new List<NeInventoryFeature>();
            foreach (var line in allLines)
            {

                var span = NeApiAccess.Instance.Spans.GetSpan(line.ObjectId);
                foundSpans.Add(span);
            }
            return foundSpans;
        }

        private static List<IntersectionInfo> CheckOffsetIntersection(List<NeInventoryFeature> crntPathLins, List<NeInventoryFeature> otherPathLines, double bufferDistance , double spofSharedDis)
        {
            var intersectingPols = new List<IntersectionInfo>();
            var crntOffestLines = new List<PolylineInfo>();
            foreach (var newLine in crntPathLins)
            {
                ITopologicalOperator topo = newLine.Geometry as ITopologicalOperator;
                var newpoly = topo.Buffer(bufferDistance) as ITopologicalOperator;
                var polyBound = newpoly.Boundary as IPolyline;
                crntOffestLines.Add(new PolylineInfo(newLine.ObjectId, polyBound));
            }

            var otherOffsetLines = new List<PolylineInfo>();
            foreach (var newLine in otherPathLines)
            {
                ITopologicalOperator topo = newLine.Geometry as ITopologicalOperator;
                var newpoly = topo.Buffer(bufferDistance) as ITopologicalOperator;
                var polyBound = newpoly.Boundary as IPolyline;
                otherOffsetLines.Add(new PolylineInfo(newLine.ObjectId, polyBound));


            }
            for(int i = 0; i < crntOffestLines.Count; i++)
            {
                foreach(var thisLine in otherOffsetLines)
                {
                    ITopologicalOperator topo = thisLine.Polyline as ITopologicalOperator;
                    var geom = crntOffestLines[i].Polyline as IGeometry;

                    if (thisLine.PolylineId == crntOffestLines[i].PolylineId)
                    {
                        var intr = topo.Intersect(geom, esriGeometryDimension.esriGeometryNoDimension);
                        var interObj = new IntersectionInfo(thisLine.PolylineId, crntOffestLines[i].PolylineId, intr);
                        interObj.IntersectionTypeProp = IntersectionInfo.Types.IntersType.sameFeature;
                        if (!intersectingPols.Contains(interObj) && !intersectingPols.Exists(x => x.FirstId == crntOffestLines[i].PolylineId && x.SecondId == thisLine.PolylineId))
                            intersectingPols.Add(interObj);
                    }
               
                    var intersection=topo.Intersect(geom,esriGeometryDimension.esriGeometryNoDimension);
                    if (!intersection.IsEmpty)
                    {
                       
                        var intersetingPoly = intersection as IPolyline;

                        if (intersetingPoly.Length < spofSharedDis)
                        {
                            var interObj = new IntersectionInfo(thisLine.PolylineId, crntOffestLines[i].PolylineId, intersection);
                            interObj.IntersectionTypeProp = IntersectionInfo.Types.IntersType.overlap;
                            if (!intersectingPols.Contains(interObj) && !intersectingPols.Exists(x => x.FirstId == crntOffestLines[i].PolylineId && x.SecondId == thisLine.PolylineId))
                                intersectingPols.Add(interObj);
                        }
                    }
                     
                }
            }
            return intersectingPols;     }

        private static List<NeInventoryFeature> GetTransmedias(List<Edge> alllines)
        {
            var foundTrans = new List<NeInventoryFeature>();
            try
            {
               
                foreach (var line in alllines)
                {

                    var trans = NeApiAccess.Instance.Transmedia.GetTransmedia(line.ObjectId);
                    foundTrans.Add(trans);
                }
                return foundTrans;
            }catch(Exception ex)
            {
                NeApiAccess.Instance.Logger.LogError("Error whike getting transmedias" , ex);
            }

            return foundTrans;
        }

        public static IPolyline GetCombinedTransmediasPolyline(List<IFeature> transmedias)
        {

            IGeometry geometryBag = new GeometryBagClass();
            var geometryCollection = geometryBag as IGeometryCollection;
            var missing = Type.Missing;

            for (var i = 0; i < transmedias.Count; i++)
            {
                var span = transmedias[i];
                if (i == 0)
                {
                    geometryBag.SpatialReference = span.Shape.SpatialReference;
                }
                geometryCollection.AddGeometry(span.ShapeCopy, ref missing, ref missing);
            }

            ITopologicalOperator unionLine = new PolylineClass();
            unionLine.ConstructUnion(geometryBag as IEnumGeometry);
            unionLine.Simplify();

            return (IPolyline)unionLine;
        }

        public static NeProgressBar GetProgressBar()
        {
            var progressBarFactory = NeProgressBarFactory.Instance;

            var pg = progressBarFactory.GetNamedProgressBar(ProgressBarName);
            if (pg != null)
                return pg;

            return progressBarFactory.CreateNamedProgressBar(ProgressBarName);
        }

    }
}
