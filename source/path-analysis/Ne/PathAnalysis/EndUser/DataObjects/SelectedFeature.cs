﻿
namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class SelectedFeature {

        public int ObjectId;

        public string DisplayName;

        public string ClassName;

        public override string ToString() {
            return DisplayName;
        }

        public override bool Equals(object obj) {
            var cast = obj as SelectedFeature;
            if (cast == null)
                return false;

            return ClassName.Equals(cast.ClassName) && ObjectId == cast.ObjectId;
        }

    }
}
