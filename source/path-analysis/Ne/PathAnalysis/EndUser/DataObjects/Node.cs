﻿namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class Node {

        public string ClassName;

        public int ObjectId;

        public string DisplayName;
        
        public override bool Equals(object obj) {
            var cast = obj as Node;
            if (cast == null)
                return false;

            return string.Equals(ClassName, cast.ClassName) && ObjectId.Equals(cast.ObjectId);
        }

    }
}
