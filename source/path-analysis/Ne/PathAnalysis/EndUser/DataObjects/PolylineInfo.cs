﻿using ESRI.ArcGIS.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class PolylineInfo
    {
        public int PolylineId;
        public IPolyline Polyline;

        public PolylineInfo(int polylineId, IPolyline polyline)
        {
            PolylineId = polylineId;
            Polyline = polyline;
        }
    }
}
