﻿using System.Collections.Generic;
using ESRI.ArcGIS.Geometry;
using Teklabz.NetworkEngineer.Api.Inventory;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects {
    public class SpofResult {

        public int SequenceNumber { get; set; }

        public Route RouteOne;

        public Route RouteTwo;

        public string RouteNames { get; set; }

        public string RelatedTelcoFeatures { get; set; }

        public List<IGeometry> SharedGeometries;

        public double Length { get; set; }

        public List<NeSpan> SpofSpans;

        public List<NeStructure> SpofStructures;

        public string RelatedInfrastructure { get; set; }

        public SpofResult() {
            SharedGeometries = new List<IGeometry>();
            SpofSpans = new List<NeSpan>();
            SpofStructures = new List<NeStructure>();
        }

    }
}
