﻿namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects {
    public class Edge {

        public Node FromFeature;

        public string FromStructureName { get; set; }

        public string FromEquipmentName { get; set; }

        public string FromSpliceName { get; set; }


        public Node ToFeature;


        public string ToStructureName { get; set; }

        public string ToEquipmentName { get; set; }

        public string ToSpliceName { get; set; }



        public string ClassName;

        public int ObjectId;

        public double EdgeLength { get; set; }

        public string EdgeName { get; set; }

        public string DisplayName;

        public override bool Equals(object obj) {
            var cast = obj as Edge;
            if (cast == null)
                return false;

            return string.Equals(ClassName, cast.ClassName) && ObjectId.Equals(cast.ObjectId);
        }

        public override string ToString() {
            return EdgeName;
        }

    }
}
