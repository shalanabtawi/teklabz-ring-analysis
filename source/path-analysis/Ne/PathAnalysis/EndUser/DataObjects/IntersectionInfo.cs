﻿using ESRI.ArcGIS.Geometry;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class IntersectionInfo
    {
        public int FirstId { set; get; }
        public int SecondId { set; get; }
        public IGeometry IntersectionGeometry { set; get; }

        public Types.IntersType IntersectionTypeProp { set; get; }

        public IntersectionInfo(int firstId, int secondId, IGeometry intersectionGeometry)
        {
            FirstId = firstId;
            SecondId = secondId;
            IntersectionGeometry = intersectionGeometry;
        }
      
        public static class Types
        {
            public enum IntersType
            {
                sameFeature,
                overlap
            }
        }
    }
}
