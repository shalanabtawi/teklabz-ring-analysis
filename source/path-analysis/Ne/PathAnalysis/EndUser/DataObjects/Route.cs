﻿using System.Collections.Generic;

namespace Teklabz.Ne.PathAnalysis.EndUser.DataObjects
{
    public class Route {

        public bool IsCable;

        public int RouteNumber;

        public List<Edge> Edges;

        public double RouteDistance;

        public List<Node> VisitedNodes;

        public Route() {
            Edges = new List<Edge>();
            VisitedNodes = new List<Node>();
        }

        public void AddEdge(Edge edge) {
            if (Edges.Contains(edge))
                return;

            RouteDistance = RouteDistance + edge.EdgeLength;
            Edges.Add(edge);
        }

        public void AddNode(Node node) {
            if (VisitedNodes.Contains(node))
                return;

            VisitedNodes.Add(node);
        }

        public override bool Equals(object obj) {
            var cast = obj as Route;
            if (cast == null)
                return false;

            if (Edges.Count != cast.Edges.Count)
                return false;

            foreach (var edge in Edges) {
                if (cast.Edges.Contains(edge))
                    continue;
                return false;
            }

            return true;
        }

        public static int CompareShortest(Route route, Route otherRoute) {
            return route.RouteDistance.CompareTo(otherRoute.RouteDistance);
        }

        public override string ToString() {
            var text = string.Format("Path - {0} [Total Segments: {1}] [Total Distance: {2}]", RouteNumber,
                Edges.Count, RouteDistance);
            return text;
        }

    }
}
