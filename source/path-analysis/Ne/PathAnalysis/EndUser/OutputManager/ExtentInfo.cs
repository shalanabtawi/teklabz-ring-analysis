﻿using Equin.ApplicationFramework;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Output;
using Microsoft.Office.Interop.Word;
using stdole;
using System;
using System.Collections.Generic;
using System.Drawing;
using Teklabz.Ne.PathAnalysis.EndUser.DataObjects;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Base;
using Teklabz.NetworkEngineer.Api.Extension;

namespace Teklabz.Ne.PathAnalysis.EndUser.OutputManager
{
    public class ExtentInfo
    {

        readonly List<IElement> _addedElements = new List<IElement>();
        public IExport ExportedPng;
        public List<NeInventoryFeature> TableElements = new List<NeInventoryFeature>();
        public string FileName;
        public void GetExtent(List<NeInventoryFeature> allFeatures, string fileName)
        {
            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var doc = arcMap.Document;

            IEnvelope envelope = new EnvelopeClass();

            var random = new Random();
            var randomColor = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
            foreach (var feature in allFeatures)
            {
                if (feature.Feature.ShapeCopy.GeometryType == esriGeometryType.esriGeometryPolyline)
                {
                    DrawPolylineGraphicsOnMap((IPolyline)feature.Feature.ShapeCopy, randomColor);
                }
                else
                {
                    DrawPointGraphicsOnMap((IPoint)feature.Feature.ShapeCopy, randomColor, 2);
                }

              
                envelope.Union(feature.Feature.ShapeCopy.Envelope);
            }

            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map;

            envelope.ZoomTo();

            IExport docExport = new ExportPNGClass();
            IPrintAndExport docPrintExport = new PrintAndExportClass();

            if (docExport is IOutputRasterSettings)
            {
                var rasterSettings = (IOutputRasterSettings)docExport;
                rasterSettings.ResampleRatio = 1;
            }

            docExport.ExportFileName = fileName;
            docPrintExport.Export(doc.ActiveView, docExport, 300, false, null);
            DeleteAllElements(graphicsContainer);
            ExportedPng = docExport;
            FileName = fileName;
        }

        private void DrawRedLine(NeInventoryFeature feature)
        {
            var featureShape = feature.Feature.ShapeCopy as IPoint;
            if (featureShape == null)
                return;

            RgbColor color = new RgbColor { Green = 255 };
            RgbColor outline = new RgbColor { Red = 0, Green = 0, Blue = 0 };

            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map; // Explicit Cast

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbol
            {
                Color = color,
                Outline = true,
                OutlineColor = outline,
                Size = 2,
                Style = esriSimpleMarkerStyle.esriSMSSquare
            };

            simpleMarkerSymbol.YOffset = 18;
            IElement element = null;
            IMarkerElement markerElement = new MarkerElementClass();
            markerElement.Symbol = simpleMarkerSymbol;
            element = (IElement)markerElement;
            var point = feature.Feature.Shape as IPoint;
            element.Geometry = point;

            StdFont pFont = new StdFont();
            element.Geometry = point;
            var myFont = pFont as IFontDisp;


            myFont.Name = "Courier New";
            myFont.Size = 12;
            ITextElement textElement = new TextElementClass();
            ITextSymbol textSymbol = new TextSymbol();
            RgbColor green = new RgbColor();
            color.Green = 140;

            textSymbol.Color = green;
            textSymbol.Size = 16;
            textSymbol.HorizontalAlignment = esriTextHorizontalAlignment.esriTHACenter;
            textSymbol.VerticalAlignment = esriTextVerticalAlignment.esriTVACenter;
            textSymbol.Font = myFont;
            textSymbol.RightToLeft = false;
            textSymbol.Angle = 0;
            var simp = textSymbol as ISimpleTextSymbol;
            simp.YOffset = 18;
            textElement.Text = feature.DisplayName;
            textElement.Symbol = simp;
            IElement newEele = textElement as IElement;
            newEele.Geometry = feature.Feature.Shape;
            ElementCollection allEles = new ElementCollection();
            _addedElements.Add(element);
            _addedElements.Add(newEele);
            allEles.Add(element);
            allEles.Add(newEele);

            graphicsContainer.AddElements(allEles, 0);


        }

        internal void CreateWordDoc(BindingListView<Edge> tableRows, PathProcessor passedInfo)
        {
            var png = ExportedPng;
            Application winword = new Application();

            //Set status for word application is to be visible or not.  
            winword.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            //Create a new document  
            Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);


            //Add the footers into the document  
            foreach (Section wordSection in document.Sections)
            {
                //Get the footer range and add the footer details.  
                Range footerRange = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                footerRange.Font.ColorIndex = WdColorIndex.wdBlack;
                footerRange.Font.Size = 10;
                footerRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                //footerRange.Text = "Footer text goes here";
            }

            //adding text to document  
            document.Content.SetRange(0, 0);
            document.Content.Text = Environment.NewLine;

            Paragraph logoPara = document.Content.Paragraphs.Add(ref missing);
            logoPara.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            logoPara.BaseLineAlignment = WdBaselineAlignment.wdBaselineAlignCenter;

            document.InlineShapes.AddPicture("C:\\Users\\farah\\Desktop\\teklabzLogo.png", Range: logoPara.Range);
            document.Application.Selection.InsertBreak(WdBreakType.wdLineBreak);

            //Add paragraph with Heading 1 style  
            Paragraph mainPar = document.Content.Paragraphs.Add(ref missing);
            mainPar.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            //object style = "Heading 1";
            //mainPar.Range.set_Style(ref style);
            mainPar.Range.Font.Size = 25;
            mainPar.Range.Font.Bold = 1;
            mainPar.Range.Font.Color = WdColor.wdColorBlack;
            mainPar.Range.Font.ColorIndex = WdColorIndex.wdBlack;
       
            mainPar.Range.Font.Name = "Times New Roman";
            mainPar.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            mainPar.Range.Text = "Teklabz Path Analysis Tool \n ";
   
          
            mainPar.Range.InsertParagraphAfter();
            //THIS NEEDS TO BE DONE FOR EVERY PATH

            
            //Add paragraph with Heading 1 style  
            Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
            object styleHeading1 = "Heading 1";
            para1.Range.set_Style(ref styleHeading1);
            para1.Range.Text = "Path1 \n";
            para1.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            para1.Range.InsertParagraphAfter();
            Paragraph picPara = document.Content.Paragraphs.Add(ref missing);
            document.Application.Selection.InsertBreak(WdBreakType.wdLineBreak);
            document.InlineShapes.AddPicture(FileName, Range: picPara.Range);

            document.Application.Selection.InsertBreak(WdBreakType.wdLineBreak);
            //Add paragraph with Heading 2 style  
            Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
            object styleHeading2 = "Heading 2";
            para2.Range.set_Style(ref styleHeading2);
            string cableSpan=null;

            if (passedInfo.UseCable)
            {
                cableSpan = "Cable";
            }
            else cableSpan = "Span";


            para2.Range.Text = "Path 1 Information: \n From Feature:"+passedInfo.FromEquipmentSite+"\n To Feature:"+ 
                passedInfo.ToEquipmentSite + ".\n Ignore From/To: "+passedInfo.IgnoreFromTo+"\n Cable/Span:" + cableSpan + " \n";
            para2.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            para2.Range.InsertParagraphAfter();
            //document.Application.Selection.InsertBreak(WdBreakType.wdPageBreak);
            if (tableRows.Count != 0)
            {
                var newList = tableRows.DataSource as List<Edge>;
                DrawTable(document, para1, newList);
            }

            //Save the document  
            object filename = @"c:\temp1.docx";
            document.SaveAs2(ref filename);
            document.Close(ref missing, ref missing, ref missing);
            document = null;
            winword.Quit(ref missing, ref missing, ref missing);
            winword = null;



        }

        private void DrawTable(Document document, Paragraph para1, List<Edge> fieldNames)
        {
            object missing = System.Reflection.Missing.Value;
            var cols = new List<string> { "FromStructureName", "SpanName", "SpanLength", "ToStructureName" };
            Table firstTable = document.Tables.Add(para1.Range, fieldNames.Count, cols.Count, ref missing, ref missing);

            firstTable.Borders.Enable = 1;

            var fieldNumber = 0;
            var colIndex = 0;
            foreach (Row row in firstTable.Rows)
            {

                foreach (Cell cell in row.Cells)
                {

                    //   Header row
                    if (cell.RowIndex == 1)
                    {
                        cell.Range.Text = cols[colIndex];
                        cell.Range.Font.Bold = 1;
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 9;

                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                        //Center alignment for the Header cells  
                        cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                        cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        colIndex++;
                    }
                    //Data row  
                    else
                    {
                        var idx = cell.ColumnIndex;
                        cell.Range.Font.Size = 8;
                        cell.Range.Font.Name = "verdana";
                        string val = null;
                        if (idx == 1)
                        {
                            val = fieldNames[fieldNumber].FromStructureName;
                        }
                        if (idx == 2)
                        {
                            val = fieldNames[fieldNumber].DisplayName;
                        }
                        if (idx == 3)
                        {
                            val = fieldNames[fieldNumber].EdgeLength.ToString();
                        }
                        if (idx == 4)
                        {
                            val = fieldNames[fieldNumber].ToStructureName;
                        }



                        if (val != null)
                        {
                            cell.Range.Text = val.ToString();
                        }


                    }

                }
                fieldNumber++;
                if (fieldNumber >= fieldNames.Count)
                {
                    return;
                }
            }
        }



        private void DrawPolylineGraphicsOnMap(IPolyline line, Color color)
        {
            var neApi = (NeApiAccessArcMap)NeApiAccess.Instance;

            var clone = (IClone)line;
            var clonedGeom = (IPolyline)clone.Clone();

            try
            {
                var map = neApi.Document.FocusMap;

                var graphicsContainer = (IGraphicsContainer)map;


                ILineElement lineElement = new LineElementClass();
                var element = (IElement)lineElement;

                ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
                simpleLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
                simpleLineSymbol.Width = 4;

                IRgbColor clr = new RgbColorClass();
                clr.Red = color.R;
                clr.Blue = color.B;
                clr.Green = color.G;
                simpleLineSymbol.Color = clr;

                element.Geometry = clonedGeom;

                var lineSymbol = (ILineSymbol)simpleLineSymbol;
                lineElement.Symbol = lineSymbol;

                graphicsContainer.AddElement(element, 0);

                _addedElements.Add(element);
            }
            catch (Exception exp)
            {
                neApi.Logger.LogWarning("Unable to draw polyline graphics on map", exp);
            }
        }

        private void DrawPointGraphicsOnMap(IPoint point, Color color, double size)
        {
            var neApi = (NeApiAccessArcMap)NeApiAccess.Instance;

            var clone = (IClone)point;
            var clonedGeom = (IPoint)clone.Clone();

            try
            {
                var map = neApi.Document.FocusMap;

                var graphicsContainer = (IGraphicsContainer)map;

                var geom = ((ITopologicalOperator)clonedGeom).Buffer(size);


                IElement element = new PolygonElementClass { Geometry = geom };
                var fill = (IFillShapeElement)element;

                IFillSymbol fs = new SimpleFillSymbolClass();
                IRgbColor clr = new RgbColorClass();
                clr.Red = color.R;
                clr.Blue = color.B;
                clr.Green = color.G;
                fs.Color = clr;

                fill.Symbol = fs;

                graphicsContainer.AddElement(element, 0);

                _addedElements.Add(element);
            }
            catch (Exception exp)
            {
                neApi.Logger.LogWarning("Unable to draw point graphics on map", exp);
            }
        }

        private void DeleteAllElements(IGraphicsContainer graphicsContainer)
        {
            foreach (var ele in _addedElements)
            {
                if (ele == null)
                    continue;

                graphicsContainer.DeleteElement(ele);
            }
        }

    }
}
