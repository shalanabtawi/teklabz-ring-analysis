﻿using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Output;
using stdole;
using System.Collections.Generic;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Extension;


namespace Teklabz.Ne.PathAnalysis.EndUser.OutputManager
{
    public class ExtentInfo
    {

        readonly List<IElement> _addedElements = new List<IElement>();

        public void GetExtent(List<IFeature> allFeatures, string fileName)
        {
            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var doc = arcMap.Document;

            IEnvelope envelope = new EnvelopeClass();

            foreach (var feature in allFeatures)
            {
                feature.Select();
                DrawRedLine(feature);
                envelope.Union(feature.ShapeCopy.Envelope);
            }

            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map;

            envelope.ZoomTo();

            IExport docExport = new ExportPNGClass();
            IPrintAndExport docPrintExport = new PrintAndExportClass();

            if (docExport is IOutputRasterSettings)
            {
                var rasterSettings = (IOutputRasterSettings)docExport;
                rasterSettings.ResampleRatio = 1;
            }

            docExport.ExportFileName = fileName;
            docPrintExport.Export(doc.ActiveView, docExport, 300, false, null);
            DeleteAllElements(graphicsContainer);
        }

        private void DrawRedLine(IFeature feature)
        {
            var featureShape = feature.ShapeCopy as IPoint;
            if (featureShape == null)
                return;

            RgbColor color = new RgbColor { Green = 255 };
            RgbColor outline = new RgbColor { Red = 0, Green = 0, Blue = 0 };

            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map; // Explicit Cast

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbol
            {
                Color = color,
                Outline = true,
                OutlineColor = outline,
                Size = 70,
                Style = esriSimpleMarkerStyle.esriSMSSquare
            };
            simpleMarkerSymbol.XOffset = 5;
            simpleMarkerSymbol.YOffset = 10;
            IElement element = null;
            IMarkerElement markerElement = new MarkerElementClass();
            markerElement.Symbol = simpleMarkerSymbol;
            element = (IElement)markerElement;
            var point = feature.Shape as IPoint;
            element.Geometry = point;

            StdFont pFont = new StdFont();


            element.Geometry = point;


            var myFont = pFont as IFontDisp;


            myFont.Name = "Courier New";
            myFont.Size = 18;
            ITextElement textElement = new TextElementClass();
            ITextSymbol textSymbol = new TextSymbol();
            RgbColor green = new RgbColor();
            color.Green = 140;

            textSymbol.Color = green;
            textSymbol.Size = 70;
            textSymbol.HorizontalAlignment = esriTextHorizontalAlignment.esriTHACenter;
            textSymbol.VerticalAlignment = esriTextVerticalAlignment.esriTVACenter;
            textSymbol.Font = myFont;
            textSymbol.RightToLeft = false;
            textSymbol.Angle = 0;
            var simp = textSymbol as ISimpleTextSymbol;
            simp.XOffset = 5;
            simp.YOffset = 10;
            textElement.Text = "TESTETT";
            textElement.Symbol = simp;
            IElement newEele = textElement as IElement;
            newEele.Geometry = feature.Shape;
            ElementCollection allEles = new ElementCollection();
            _addedElements.Add(element);
            _addedElements.Add(newEele);
            allEles.Add(element);
            allEles.Add(newEele);

            graphicsContainer.AddElements(allEles, 0);


        }

        private void DeleteAllElements(IGraphicsContainer graphicsContainer)
        {
            foreach (var ele in _addedElements)
            {
                if (ele == null)
                    continue;
                graphicsContainer.DeleteElement(ele);
            }
        }

    }
}
