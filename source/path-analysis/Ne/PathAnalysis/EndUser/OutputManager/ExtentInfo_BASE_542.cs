﻿using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Output;
using stdole;
using System;
using System.Collections.Generic;
using Teklabz.NetworkEngineer.Api;
using Teklabz.NetworkEngineer.Api.Extension;


namespace Teklabz.Ne.PathAnalysis.EndUser.OutputManager
{
    public class ExtentInfo
    {
        List<IElement> AddedElements = new List<IElement>();
        public void GetExtent(List<IFeature> allFeatures)
        {
            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var doc = arcMap.Document;

           
            IEnvelope envelope = new EnvelopeClass();
            foreach (var feature in allFeatures)
            {
                DrawRedLine(feature);
                envelope.Union(feature.ShapeCopy.Envelope);
            }
         
            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map;
          
            envelope.ZoomTo();

            IExport docExport;
            IPrintAndExport docPrintExport;

            string sNameRoot;
            bool bReenable = false;
           

            docExport = new ExportPNGClass();
            docPrintExport = new PrintAndExportClass();

            IOutputRasterSettings RasterSettings;
            sNameRoot = "ExportActiveViewSampleOutput";

            var lResampleRatio = 1;
            if (docExport is IOutputRasterSettings)
            {
                RasterSettings = (IOutputRasterSettings)docExport;
                RasterSettings.ResampleRatio = (int)lResampleRatio;

            }

            docExport.ExportFileName = "Test1.png";
            docPrintExport.Export(doc.ActiveView, docExport, 300, false, null);
            DeleteAllElements(graphicsContainer);
        }

        private void DrawRedLine(IFeature feature)
        {
            RgbColor color = new RgbColor();
            color.Green = 255;

            RgbColor outline = new RgbColor();
            outline.Red = 0;
            outline.Green = 0;
            outline.Blue = 0;
            var arcMap = (NeApiAccessArcMap)NeApiAccess.Instance;
            var map = arcMap.Document.FocusMap;
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)map; // Explicit Cast

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbol
            {
                Color = color,
                Outline = true,
                OutlineColor = outline,
                Size = 70,
                Style = esriSimpleMarkerStyle.esriSMSSquare
            };
            simpleMarkerSymbol.XOffset = 20;
            simpleMarkerSymbol.YOffset = 50;
            IElement element = null;
            IMarkerElement markerElement = new MarkerElementClass();
            markerElement.Symbol = simpleMarkerSymbol;
            element = (IElement)markerElement;
       
              var newPoint = feature.Shape as IPoint;
          
            newPoint.X = newPoint.X + 200;
            newPoint.X = newPoint.Y + 200;
            element.Geometry = newPoint;
            // graphicsContainer.AddElement(element, 0);
            StdFont pFont = new StdFont() ;
            var myFont = pFont as IFontDisp;


            myFont.Name = "Courier New";
            myFont.Size = 18;
            ITextElement textElement = new TextElementClass();
            ITextSymbol textSymbol = new TextSymbol();
            RgbColor green = new RgbColor();
            color.Green = 140;
            
            textSymbol.Color = green;
            textSymbol.Size =70 ;
            textSymbol.HorizontalAlignment = esriTextHorizontalAlignment.esriTHACenter;
            textSymbol.VerticalAlignment = esriTextVerticalAlignment.esriTVACenter;
            textSymbol.Font = myFont;
            textSymbol.RightToLeft = false;
            textSymbol.Angle = 0;
            var simp = textSymbol as ISimpleTextSymbol;
            simp.XOffset = 20;
            simp.YOffset = 50;
            textElement.Text = "TESTETT";
            textElement.Symbol = simp;
            IElement newEele = textElement as IElement;
            newEele.Geometry = feature.Shape;
            ElementCollection allEles = new ElementCollection();
            AddedElements.Add(element);
            AddedElements.Add(newEele);
            allEles.Add(element);
            allEles.Add(newEele);
         
             graphicsContainer.AddElements(allEles, 0);
          
         
        }

        private void DeleteAllElements(IGraphicsContainer graphicsContainer)
        { 
    foreach(var ele in AddedElements)
            {
                if (ele == null)
                {
                    continue;
                }
                graphicsContainer.DeleteElement(ele);
            }

         
        }
    }
}
