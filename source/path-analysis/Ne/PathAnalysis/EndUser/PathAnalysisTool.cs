﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.ADF.CATIDs;
using ESRI.ArcGIS.ArcMapUI;
using Teklabz.Ne.PathAnalysis.Common;
using Teklabz.Ne.PathAnalysis.EndUser.UI;
using Teklabz.NetworkEngineer.Api;

namespace Teklabz.Ne.PathAnalysis.EndUser {
    [ClassInterface(ClassInterfaceType.None)]
    [Guid(Guid)]
    [ProgId(ProgId)]
    [ComVisible(true)]
     public class PathAnalysisTool : BaseCommand {

        public const string Guid = "07A1CE40-075B-4E14-AEAC-38186C2C8C2B";
        public const string ProgId = "Teklabz.Ne.PathAnalysis.EndUser.PathAnalysisTool";

        #region COM Registration Function(s)

        [ComRegisterFunction]
        [ComVisible(false)]
        private static void RegisterFunction(Type registerType) {
            ArcGisCategoryRegistration(registerType);
        }

        [ComUnregisterFunction]
        [ComVisible(false)]
        private static void UnregisterFunction(Type registerType) {
            ArcGisCategoryUnregistration(registerType);
        }

        #region ArcGIS Component CategoryName Registrar generated code

        private static void ArcGisCategoryRegistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Register(regKey);
        }

        private static void ArcGisCategoryUnregistration(Type registerType) {
            var regKey = string.Format("HKEY_CLASSES_ROOT\\CLSID\\{{{0}}}", registerType.GUID);
            MxCommands.Unregister(regKey);
        }

        #endregion

        #endregion

        public PathAnalysisTool() {
            m_category = "Teklabz";
            m_caption = "Path Analysis Tool";
            m_message = "Path Analysis Tool";
            m_toolTip = "Path Analysis Tool";
            m_name = "teklabz-path-analysis";

            try {
                var bitmapResourceName = GetType().Name + ".bmp";
                m_bitmap = new Bitmap(GetType(), bitmapResourceName);
            } catch (Exception ex) {
                Trace.WriteLine(ex.Message, "Invalid Bitmap");
            }
        }

        public override void OnCreate(object hook) {
            if (hook == null)
                return;
            if (hook is IMxApplication)
                m_enabled = true;
            else
                m_enabled = false;
        }

        public override void OnClick() {
            if (!SecurityManager.CanPerformEndUserPathAnalysis()) {
                return;
            }

            var mainForm = FindOpenForm<PathAnalysisWizardForm>();
            if (mainForm == null || mainForm.IsDisposed) {
                mainForm = new PathAnalysisWizardForm();
            }
            if (!mainForm.Visible) {
                mainForm.Show(
                    NativeWindow.FromHandle((IntPtr)((NeApiAccessArcMap)NeApiAccess.Instance).Application.hWnd));
            }
            if (mainForm.WindowState == FormWindowState.Minimized)
                mainForm.WindowState = FormWindowState.Normal;
            mainForm.Activate();
        }

        private Form FindOpenForm<T>() {
            return Application.OpenForms.Cast<Form>().FirstOrDefault(x => x is T);
        }

    }
}
