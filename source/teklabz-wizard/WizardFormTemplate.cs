﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Wizard {
    public partial class WizardFormTemplate : Form {

        private readonly List<IUserPage> _userPages;

        private int _currentIndex = -1;

        private readonly List<object> _allObjects;

        public event EventHandler OnFinishButtonClicked;

        public bool ContentListVisible {
            set { contentContainer.Panel1Collapsed = !value; }
        }

        public int ContentListWidth{
            set { contentContainer.SplitterDistance = value; }
        }

        public bool LastPageSkippedNotify { get; set; }

        public bool NextButtonEnabled{
            get { return nextBtn.Enabled; }
            set { nextBtn.Enabled = value; }
        }

        public WizardFormTemplate() {
            InitializeComponent();
            _userPages = new List<IUserPage>();
            _allObjects = new List<object>();
            backBtn.Enabled = false;
            nextBtn.Enabled = false;
            finishBtn.Enabled = false;
            LastPageSkippedNotify = true;
        }

        private void ClasseslstbxDrawItemEvent(object sender, DrawItemEventArgs e) {
            var reportsForegroundBrushSelected = new SolidBrush(Color.White);
            var reportsForegroundBrush = new SolidBrush(Color.Black);
            var reportsBackgroundBrushSelected = new SolidBrush(Color.FromKnownColor(KnownColor.Highlight));
            var reportsBackgroundBrush1 = new SolidBrush(Color.White);
            e.DrawBackground();
            var selected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            var index = e.Index;
            if(index >= 0 && index < contentLstBx.Items.Count) {
                var text = contentLstBx.Items[index].ToString();
                var g = e.Graphics;

                //background:
                var backgroundBrush = selected ? reportsBackgroundBrushSelected : reportsBackgroundBrush1;
                g.FillRectangle(backgroundBrush, e.Bounds);

                //text:
                var foregroundBrush = (selected) ? reportsForegroundBrushSelected : reportsForegroundBrush;
                var location = contentLstBx.GetItemRectangle(index).Location;
                location.Y += 8;
                location.X += 5;
                var font = new Font(e.Font.FontFamily, e.Font.SizeInPoints,selected ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Point, e.Font.GdiCharSet, e.Font.GdiVerticalFont);
                g.DrawString(text, font, foregroundBrush, location);
            }
            e.DrawFocusRectangle();
        }

        private void FormLayoutLoadEvent(object sender, EventArgs e) {
            if (_userPages.Count == 0) return;
            Next();
        }

        public void AddContentPage(IUserPage contentPage) {
            if (contentPage.Control == null) return;
            _userPages.Add(contentPage);
            _allObjects.Add(contentPage.UserData);
            contentLstBx.Items.Add(contentPage.PageName);
        }

        private void LoadNewForm() {
            var userPageControl = _userPages[_currentIndex].Control;
            userPageControl.AutoScroll = true;
            userPageControl.Dock = DockStyle.Fill;
            contentContainer.Panel2.Controls.Clear();
            contentContainer.Panel2.Controls.Add(userPageControl);
            userPageControl.Show();
            contentLstBx.SelectedIndex = _currentIndex;
            pageNameLbl.Text = contentLstBx.SelectedItem.ToString();
            summaryLbl.Text = _userPages[_currentIndex].Summary;
        }

        private void NextBtnClickEvent(object sender, EventArgs e) {
            _userPages[_currentIndex].NextButtonClicked();
            if (!ValidateBeforeNext()) return;
            Next();
        }

        private bool ValidateBeforeNext() {
            if (_userPages[_currentIndex].IsValid) {
                return true;
            }
            MessageBox.Show(this, _userPages[_currentIndex].InvalidMessage, "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            return false;
        }

        private bool ValidateBeforeFinish() {
            if (_userPages[_currentIndex].IsValid) {
                _allObjects[_currentIndex] = _userPages[_currentIndex].UserData;
                return true;
            }
            MessageBox.Show(this, _userPages[_currentIndex].InvalidMessage, "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            return false;
        }

        private void Next() {
            if(_currentIndex != -1) _allObjects[_currentIndex] = _userPages[_currentIndex].UserData;
            _currentIndex++;

            var isSkip = _userPages[_currentIndex].CurrentPageOnLoad(_allObjects, LastForm.Previous);
            if (isSkip) {
                if (_currentIndex + 1 == _userPages.Count) {
                    if (_currentIndex == 0) {
                        //If wizard has only one page, and it's skipped, The wziard will finish without doing anything
                        FinishBtnClickEvent(null, null);
                        return;
                    }
                    var skippingResult = new DialogResult();
                    if (LastPageSkippedNotify) {
                        skippingResult = MessageBox.Show(this,
                        "This next would finish the wizard, since next page is skipped,then would you like to continue?"
                        , "Last page skipped", MessageBoxButtons.YesNo);

                    }
                    //Last Page is skipped
                    if (skippingResult == DialogResult.Yes || !LastPageSkippedNotify) {
                        OnFinishButtonClicked(null, null);
                        Dispose();
                        return;
                    }
                    Back();
                    return;
                }
                Next();
                if (IsDisposed) return;
            }

            if (_currentIndex + 1 == _userPages.Count) {
                nextBtn.Enabled = false;
                finishBtn.Enabled = true;
            } else {
                nextBtn.Enabled = true;
            }
            if (_currentIndex > 0) backBtn.Enabled = true;
            LoadNewForm();
        }

        private void BackBtnClickEvent(object sender, EventArgs e) {
            _userPages[_currentIndex].BackButtonClicked();
            Back();
        }

        private void Back() {
            _currentIndex--;
            //if (_currentIndex + 1 < _userPages.Count) contentTrView.Nodes[_currentIndex + 1].NodeFont = DefaultFont;

            var isSkip = _userPages[_currentIndex].CurrentPageOnLoad(_allObjects, LastForm.Next);
            if (isSkip) {
                if (_currentIndex == 0) {
                    Next();
                    return;
                }
                Back();
            }
            if (_currentIndex + 1 != _userPages.Count) {
                nextBtn.Enabled = true;
                finishBtn.Enabled = false;
            }
            if (_currentIndex == 0) backBtn.Enabled = false;
            LoadNewForm();
        }

        private void CancelBtnClickEvent(object sender, EventArgs e) {
            Dispose();
            Close();
        }

        private void FinishBtnClickEvent(object sender, EventArgs e) {
            _userPages[_currentIndex].FinishButtonClicked();
            if (!ValidateBeforeFinish()) return;

            Dispose();
            Close();
        }

        private void HelpBtnClickEvent(object sender, EventArgs e) {
            if (_currentIndex >= 0) _userPages[_currentIndex].HelpButtonClicked();
        }

        private void NextBtnEnabledChanged(object sender, EventArgs e) {
            if(_currentIndex + 1 == _userPages.Count && nextBtn.Enabled) nextBtn.Enabled = false;
        }

    }
}
