﻿namespace Wizard {
    partial class WizardFormTemplate {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.helpBtn = new System.Windows.Forms.Button();
            this.nextBtn = new System.Windows.Forms.Button();
            this.finishBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.backBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.summaryLbl = new System.Windows.Forms.Label();
            this.pageNameLbl = new System.Windows.Forms.Label();
            this.contentContainer = new System.Windows.Forms.SplitContainer();
            this.contentLstBx = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainerPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contentContainer)).BeginInit();
            this.contentContainer.Panel1.SuspendLayout();
            this.contentContainer.SuspendLayout();
            this.splitContainerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.helpBtn);
            this.panel1.Controls.Add(this.nextBtn);
            this.panel1.Controls.Add(this.finishBtn);
            this.panel1.Controls.Add(this.cancelBtn);
            this.panel1.Controls.Add(this.backBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 323);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 38);
            this.panel1.TabIndex = 0;
            // 
            // helpBtn
            // 
            this.helpBtn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.helpBtn.Location = new System.Drawing.Point(5, 9);
            this.helpBtn.Name = "helpBtn";
            this.helpBtn.Size = new System.Drawing.Size(75, 23);
            this.helpBtn.TabIndex = 1;
            this.helpBtn.Text = "Help";
            this.helpBtn.UseVisualStyleBackColor = true;
            this.helpBtn.Click += new System.EventHandler(this.HelpBtnClickEvent);
            // 
            // nextBtn
            // 
            this.nextBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.nextBtn.Location = new System.Drawing.Point(343, 9);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(75, 23);
            this.nextBtn.TabIndex = 1;
            this.nextBtn.Text = "Next >";
            this.nextBtn.UseVisualStyleBackColor = true;
            this.nextBtn.EnabledChanged += new System.EventHandler(this.NextBtnEnabledChanged);
            this.nextBtn.Click += new System.EventHandler(this.NextBtnClickEvent);
            // 
            // finishBtn
            // 
            this.finishBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.finishBtn.Location = new System.Drawing.Point(424, 9);
            this.finishBtn.Name = "finishBtn";
            this.finishBtn.Size = new System.Drawing.Size(75, 23);
            this.finishBtn.TabIndex = 3;
            this.finishBtn.Text = "Finish";
            this.finishBtn.UseVisualStyleBackColor = true;
            this.finishBtn.Click += new System.EventHandler(this.FinishBtnClickEvent);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cancelBtn.Location = new System.Drawing.Point(505, 9);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 2;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.CancelBtnClickEvent);
            // 
            // backBtn
            // 
            this.backBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.backBtn.Location = new System.Drawing.Point(262, 8);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(75, 23);
            this.backBtn.TabIndex = 0;
            this.backBtn.Text = "< Back";
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.BackBtnClickEvent);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.summaryLbl);
            this.panel2.Controls.Add(this.pageNameLbl);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 57);
            this.panel2.TabIndex = 3;
            // 
            // summaryLbl
            // 
            this.summaryLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.summaryLbl.AutoEllipsis = true;
            this.summaryLbl.Location = new System.Drawing.Point(11, 25);
            this.summaryLbl.Name = "summaryLbl";
            this.summaryLbl.Size = new System.Drawing.Size(559, 29);
            this.summaryLbl.TabIndex = 1;
            this.summaryLbl.Text = "summary\r\nsummary";
            // 
            // pageNameLbl
            // 
            this.pageNameLbl.AutoSize = true;
            this.pageNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageNameLbl.Location = new System.Drawing.Point(11, 6);
            this.pageNameLbl.Name = "pageNameLbl";
            this.pageNameLbl.Size = new System.Drawing.Size(91, 17);
            this.pageNameLbl.TabIndex = 0;
            this.pageNameLbl.Text = "Page Name";
            // 
            // contentContainer
            // 
            this.contentContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentContainer.IsSplitterFixed = true;
            this.contentContainer.Location = new System.Drawing.Point(0, 5);
            this.contentContainer.Name = "contentContainer";
            // 
            // contentContainer.Panel1
            // 
            this.contentContainer.Panel1.Controls.Add(this.contentLstBx);
            this.contentContainer.Size = new System.Drawing.Size(584, 256);
            this.contentContainer.SplitterDistance = 147;
            this.contentContainer.SplitterWidth = 5;
            this.contentContainer.TabIndex = 4;
            // 
            // contentLstBx
            // 
            this.contentLstBx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.contentLstBx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentLstBx.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.contentLstBx.Enabled = false;
            this.contentLstBx.IntegralHeight = false;
            this.contentLstBx.ItemHeight = 30;
            this.contentLstBx.Location = new System.Drawing.Point(0, 0);
            this.contentLstBx.Name = "contentLstBx";
            this.contentLstBx.Size = new System.Drawing.Size(147, 256);
            this.contentLstBx.TabIndex = 0;
            this.contentLstBx.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.ClasseslstbxDrawItemEvent);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(2, 265);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(580, 1);
            this.label1.TabIndex = 5;
            // 
            // splitContainerPanel
            // 
            this.splitContainerPanel.Controls.Add(this.label1);
            this.splitContainerPanel.Controls.Add(this.contentContainer);
            this.splitContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerPanel.Location = new System.Drawing.Point(0, 57);
            this.splitContainerPanel.Name = "splitContainerPanel";
            this.splitContainerPanel.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.splitContainerPanel.Size = new System.Drawing.Size(584, 266);
            this.splitContainerPanel.TabIndex = 7;
            // 
            // WizardFormTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.splitContainerPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "WizardFormTemplate";
            this.Text = "FormLayout";
            this.Load += new System.EventHandler(this.FormLayoutLoadEvent);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.contentContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contentContainer)).EndInit();
            this.contentContainer.ResumeLayout(false);
            this.splitContainerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button finishBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Button helpBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label pageNameLbl;
        private System.Windows.Forms.SplitContainer contentContainer;
        private System.Windows.Forms.Label summaryLbl;
        private System.Windows.Forms.ListBox contentLstBx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel splitContainerPanel;
    }
}