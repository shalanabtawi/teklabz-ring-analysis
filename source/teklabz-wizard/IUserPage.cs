﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Wizard {
    public interface IUserPage {

        UserControl Control { get; }
        string PageName { get; }
        string Summary { get; }
        bool IsValid { get; }
        string InvalidMessage { get; }
        object UserData { get; }
        
        bool CurrentPageOnLoad(List<object> allControlsObjects, LastForm lastForm);
        void NextButtonClicked();
        void BackButtonClicked();
        void FinishButtonClicked();
        void HelpButtonClicked();
    }
}
