@echo off

set "ProgFiles=%ProgramFiles%"
if "%PROCESSOR_ARCHITECTURE%"=="AMD64" (goto 64BIT) else goto 32BIT

:64BIT
set ProgFiles=%ProgramFiles(x86)%

:32BIT
echo "%ProgFiles%"

echo.
echo.
echo ****************************************************
echo ****           Starting Build Process           ****
echo ****************************************************
echo.
echo ** [1/4] Housekeeping **

REM redirect useless stdout to nul and stderr to nul
rmdir /S /Q build-logs >nul 2>nul
mkdir build-logs >nul 2>nul
del /F /Q bin\*.* >nul 2>nul

echo ** [2/4] Building Solution **
REM build solution with minimal verbosity, with clean target then build target
lib\elevate.exe -c -w build.bat

echo ** [3/4] Creating Installer **
REM create the installer using makensis
"%ProgFiles%\NSIS\makensis" /V2 build.nsi >> build-logs\make-nsis.log

echo ** [4/4] Cleaning Up **
REM build solution with minimal verbosity, with clean target then build target
lib\elevate.exe -c -w clean.bat

echo.

echo ****************************************************
echo ****           Finished Build Process           ****
echo ****************************************************
echo ********                                    ********
echo ****     Check logs in [build-logs] folder      ****
echo ****************************************************

echo.
echo.

@echo on