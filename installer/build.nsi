;---------------------------------------------------
; Includes
;---------------------------------------------------
!addplugindir lib\plugins
!addincludedir lib\includes

!include MUI.nsh
!include Registry.nsh
!include LogicLib.nsh
!include DotNET.nsh
!include FileFunc.nsh
!include XML.nsh
;---------------------------------------------------
!define VersionCompare `!insertmacro VersionCompareCall`
!define AddToolboxTool `!insertmacro AddToolboxToolCall`
 
!macro VersionCompareCall _VER1 _VER2 _RESULT
	Push `${_VER1}`
	Push `${_VER2}`
	Call VersionCompare
	Pop ${_RESULT}
!macroend

!macro AddToolboxToolCall name
  push ${name}
  call addToolboxTool
!macroend

!macro un.RemoveToolboxToolCall name
  push ${name}
  call un.removeToolboxTool
!macroend

!macro RemoveToolboxToolCall name
  push ${name}
  call removeToolboxTool
!macroend

;---------------------------------------------------
; Required Software Versions Constants (upper case)
;---------------------------------------------------
!define DOTNET_VERSION "v4.0"
!define ARCGIS_VERSION "10.2.1"
!define NE_VERSION "16.1.0"
;---------------------------------------------------

;---------------------------------------------------
; Application Version and Name Constants (upper case)
;---------------------------------------------------
!define APP_VERSION "1.0.0.3-NE16"
!define COMPANY "Teklabz"
!define COMPANY_FULL_NAME "Teklabz"
!define PRODUCT_NAME "Path Analysis"
!define PRODUCT_NAME_SHORT "PA"

!define PA_DLL_NAME "Teklabz.PathAnalysis"
!define WIZ_DLL_NAME "Teklabz.Wizard"
!define KZ_API_NAME "Teklabz.NetworkEngineer.Api"
!define BLV_DLL_NAME "Equin.ApplicationFramework.BindingListView"
!define MO_DLL_NAME "Microsoft.Office.Interop.Word"

;---------------------------------------------------

;---------------------------------------------------
; Installation User Variables (lower case)
;---------------------------------------------------
Var "ArcGisPath"
Var "NePath"
Var "ArcGisBinPath"
Var "NeBinPath"
Var "NeToolboxBinPath"
Var "ArcGisVersion"
Var "NeVersion"
Var "DotNetDir"
Var "StartMenuFolder"
;---------------------------------------------------

;---------------------------------------------------
; Installer Properties
;---------------------------------------------------
;Installation Name
Name "${PRODUCT_NAME}"
;Output name of installation file
OutFile "${COMPANY} ${PRODUCT_NAME} (v-${APP_VERSION}).exe"

;Require admin rights on NT6+ (When UAC is turned on)
RequestExecutionLevel admin

;Default installation folder
InstallDir "$PROGRAMFILES\${COMPANY}\${PRODUCT_NAME}"
; Registry key to check for directory
InstallDirRegKey HKLM "Software\${COMPANY}\${PRODUCT_NAME_SHORT}" "Install_Dir"


;Compression to be used when creating installer
SetCompressor /FINAL /SOLID lzma
SetCompressorDictSize 64

!define ARP "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
;---------------------------------------------------

;---------------------------------------------------
; Interface Configuration
;---------------------------------------------------
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "resources\ericsson-h.bmp"
!define MUI_HEADERIMAGE_UNBITMAP "resources\ericsson-h.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP "resources\ericsson-v.bmp"
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "resources\ericsson-v.bmp"
!define MUI_ICON "resources\install.ico"
!define MUI_UNICON "resources\uninstall.ico"
!define MUI_ABORTWARNING
!define MUI_WELCOMEPAGE_TITLE_3LINES
!define MUI_FINISHPAGE_TITLE_3LINES
!define MUI_STARTMENUPAGE_NODISABLE

;Welcome Page Text
;!define MUI_WELCOMEPAGE_TITLE ""
;!define MUI_WELCOMEPAGE_TEXT ""

;Start Menu Folder Page Configuration
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM" 
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${COMPANY}\${PRODUCT_NAME_SHORT}" 
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "StartMenuFolder"
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${COMPANY}"

!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_UNFINISHPAGE_NOAUTOCLOSE

!define MUI_FINISHPAGE_LINK "Ericsson Home Page"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.ericsson.com"
!define MUI_FINISHPAGE_LINK_COLOR 000080
;!define MUI_FINISHPAGE_TITLE ""
;!define MUI_FINISHPAGE_TEXT ""

XPStyle on
BrandingText "${COMPANY_FULL_NAME}"
;---------------------------------------------------

;---------------------------------------------------
; Modern UI Pages
;---------------------------------------------------

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU 0 $StartMenuFolder
;custom page to make sure arcmap, arccat and other apps are closed
Page Custom LockedApplicationList
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
;custom page to make sure arcmap, arccat and other apps are closed
UninstPage Custom un.LockedApplicationList
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English" 
;---------------------------------------------------

!macro VerifyUserIsAdmin
UserInfo::GetAccountType
pop $0
${If} $0 != "admin" ;Require admin rights on NT4+
        messageBox mb_iconstop "Administrator rights required!"
        setErrorLevel 740 ;ERROR_ELEVATION_REQUIRED
        quit
${EndIf}
!macroend

Function .onInit
	SetShellVarContext all
	!insertmacro VerifyUserIsAdmin

	Call FillEnvVariables
	
	${VersionCompare} "$ArcGisVersion" "${ARCGIS_VERSION}" $R0
	${If} $R0 == 2
		MessageBox MB_OK|MB_ICONSTOP "Your current ArcGIS version [$ArcGisVersion] does not meet the minimum requirements. You need at least version ${ARCGIS_VERSION} to continue."
		Abort
	${EndIf}
	
	${VersionCompare} "$NeVersion" "${NE_VERSION}" $R0
	${If} $R0 == 2
		MessageBox MB_OK|MB_ICONSTOP "Your current Network Engineer version [$NeVersion] does not meet the minimum requirements. You need at least version ${NE_VERSION} to continue."
		Abort
	${EndIf}
	
	StrCmp $DotNetDir "" 0 +3
	messageBox MB_OK "Unable to find Microsoft .NET installation path. Please make sure the .NET framework is installed."
	Abort
	
	StrCmp $ArcGisBinPath "" 0 +3
	messageBox MB_OK "Unable to find the ArcGIS installation path."
	Abort
	
	StrCmp $NeBinPath "" 0 +3
	messageBox MB_OK "Unable to find the Network Engineer installation path."
	Abort
	
FunctionEnd

Function un.onInit

	SetShellVarContext all
	!insertmacro VerifyUserIsAdmin

	Call un.FillEnvVariables
	
	StrCmp $DotNetDir "" 0 +3
	messageBox MB_OK "Unable to find Microsoft .NET installation path. Please make sure the .NET framework is installed."
	Abort
	
	StrCmp $ArcGisBinPath "" 0 +3
	messageBox MB_OK "Unable to find the ArcGIS installation path."
	Abort
	
	StrCmp $NeBinPath "" 0 +3
	messageBox MB_OK "Unable to find the Network Engineer installation path."
	Abort
	
FunctionEnd


Section "Teklabz Path Analysis" InstallationFiles

	SectionIn RO
	
	SetOutPath "$INSTDIR"
	
	;Unregister and delete Any Old DLLs
	DetailPrint "Removing Old Installation (if any)"
	Call RemoveOldInstallation
	
	DetailPrint "Installing Product"
	
	;Store installation folder
	WriteRegStr HKLM "Software\${COMPANY}\${PRODUCT_NAME_SHORT}" "InstallDir" $INSTDIR
	
	;Create uninstaller
	WriteUninstaller "$INSTDIR\Uninstall.exe"
	
	DetailPrint "Copying Product Files"
	
	CreateDirectory "$INSTDIR\DatabaseUpgrade"
	CreateDirectory "$INSTDIR\Documents"

	File "bin\${PA_DLL_NAME}.dll"
	File "bin\${PA_DLL_NAME}.pdb"
	File "bin\${PA_DLL_NAME}.tlb"
	File "bin\${WIZ_DLL_NAME}.dll"
	File "bin\${KZ_API_NAME}.dll"
	File "bin\${MO_DLL_NAME}.dll"
	File "bin\${BLV_DLL_NAME}.dll"
	File "resources\uninstall.ico"
	File "resources\teklabz.ico"
	
	
	SetOutPath "$INSTDIR\DatabaseUpgrade"
	File "db\00-seed-version.xml"
	File "db\01-Path-Analysis-Schema.xml"
	File "db\tk-pa-object-roles.xml"
	File "db\tk-pa-security-roles.xml"
	

	SetOutPath "$INSTDIR\Documents"
	
	DetailPrint "Registering Product File"
	
	SetOutPath "$INSTDIR"
	nsExec::ExecToLog '"$PROGRAMFILES\Common Files\ArcGIS\bin\ESRIRegAsm.exe" /s /p:desktop "${PA_DLL_NAME}.dll"'
	
	
	!insertmacro MUI_STARTMENU_WRITE_BEGIN 0
		;Create shortcuts
		DetailPrint "Adding Start Menu Shortcuts"
		CreateDirectory "$SMPROGRAMS\$StartMenuFolder\${PRODUCT_NAME}"
		CreateShortCut  "$SMPROGRAMS\$StartMenuFolder\${PRODUCT_NAME}\Uninstall Teklabz Path Analysis.lnk" "$INSTDIR\Uninstall.exe"
	!insertmacro MUI_STARTMENU_WRITE_END
	
	WriteRegStr HKLM "${ARP}" "DisplayName" "TEKLABZ Path Analysis"
	WriteRegStr HKLM "${ARP}" "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
	WriteRegStr HKLM "${ARP}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "${ARP}" "Publisher" "${COMPANY}"
	WriteRegStr HKLM "${ARP}" "DisplayVersion" "${APP_VERSION}"
	WriteRegStr HKLM "${ARP}" "DisplayIcon" "$\"$INSTDIR\teklabz.ico$\""
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "${ARP}" "EstimatedSize" "$0"
	
	DetailPrint "Done"
	
SectionEnd

Section "Uninstall"

	DetailPrint "Removing Old Installation"
	;Unregister and delete Any Old DLLs
	Call un.RemoveOldInstallation

SectionEnd

;---------------------------------------------------
; Section Descriptions
;---------------------------------------------------
	!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
		!insertmacro MUI_DESCRIPTION_TEXT ${InstallationFiles} "TEKLABZ Path Analysis"
	!insertmacro MUI_FUNCTION_DESCRIPTION_END
;---------------------------------------------------


;---------------------------------------------------
; MACROS and Functions
; If functions is used in install and uninstall
; it should be placed in the SHARED_FUNCTIONS
; macro so that it will automatically produce 
; the required functions, otherwise, just place
; it outside
;---------------------------------------------------
!macro SHARED_FUNCTIONS un

	Function ${un}DeleteDirIfEmpty
	  FindFirst $R0 $R1 "$0\*.*"
	  strcmp $R1 "." 0 NoDelete
	   FindNext $R0 $R1
	   strcmp $R1 ".." 0 NoDelete
		ClearErrors
		FindNext $R0 $R1
		IfErrors 0 NoDelete
		 FindClose $R0
		 Sleep 1000
		 RMDir "$0"
	  NoDelete:
	   FindClose $R0
	FunctionEnd

	; Given a .NET version number, this function returns that .NET framework's
	; install directory. Returns "" if the given .NET version is not installed.
	; Params: [version] (eg. "v2.0")
	; Return: [dir] (eg. "C:\WINNT\Microsoft.NET\Framework\v2.0.50727")
	Function ${un}GetDotNetDir
		Exch $R0 ; Set R0 to .net version major
		Push $R1
		Push $R2
	 
		; set R1 to minor version number of the installed .NET runtime
		EnumRegValue $R1 HKLM \
			"Software\Microsoft\.NetFramework\policy\$R0" 0
		IfErrors getdotnetdir_err
	 
		; set R2 to .NET install dir root
		ReadRegStr $R2 HKLM \
			"Software\Microsoft\.NetFramework" "InstallRoot"
		IfErrors getdotnetdir_err
	 
		; set R0 to the .NET install dir full
		StrCpy $R0 "$R2$R0.$R1"
	 
	getdotnetdir_end:
		Pop $R2
		Pop $R1
		Exch $R0 ; return .net install dir full
		Return
	 
	getdotnetdir_err:
		StrCpy $R0 ""
		Goto getdotnetdir_end
	 
	FunctionEnd
	
	Function ${un}FillEnvVariables
		ReadRegStr "$ArcGisPath" HKLM "Software\ESRI\Desktop10.2" "TargetPath"
		ReadRegStr "$ArcGisVersion" HKLM "Software\ESRI\Desktop10.2" "RealVersion"
		ReadRegStr "$NePath" HKLM "Software\Telcordia Technologies\Network Engineer\2.0" "InstallDir"
		ReadRegStr "$NeVersion" HKLM "Software\Telcordia Technologies\Network Engineer\2.0" "ReleaseNumber"
		StrCpy "$ArcGisBinPath" "$ArcGisPathbin"
		StrCpy "$NeBinPath" "$NePathBin"
		StrCpy "$NeToolboxBinPath" "$NePathNEAdminTools\Bin"
		;get directory of .NET framework installation
		Push "${DOTNET_VERSION}"
		Call ${un}GetDotNetDir
		Pop $DotNetDir
	FunctionEnd
	
	Function ${un}RemoveOldInstallation

		DetailPrint "Un-registering Product Files"
		
		IfFileExists "$INSTDIR\${PA_DLL_NAME}.dll" 0
		nsExec::ExecToLog '"$PROGRAMFILES\Common Files\ArcGIS\bin\ESRIRegAsm.exe" /s /u /p:desktop "$INSTDIR\${PA_DLL_NAME}.dll"'
		
		
		DetailPrint "Removing Product Files"
		
		IfFileExists "$INSTDIR\${PA_DLL_NAME}.dll" 0
		Delete "$INSTDIR\${PA_DLL_NAME}.dll"
		
		IfFileExists "$INSTDIR\${PA_DLL_NAME}.pdb" 0
		Delete "$INSTDIR\${PA_DLL_NAME}.pdb"
		
		IfFileExists "$INSTDIR\${PA_DLL_NAME}.tlb" 0
		Delete "$INSTDIR\${PA_DLL_NAME}.tlb"
		
		IfFileExists "$INSTDIR\${WIZ_DLL_NAME}.dll" 0
		Delete "$INSTDIR\${WIZ_DLL_NAME}.dll"
		
		IfFileExists "$INSTDIR\${KZ_API_NAME}.dll" 0
		Delete "$INSTDIR\${KZ_API_NAME}.dll"
		
		IfFileExists "$INSTDIR\${KZ_API_NAME}.pdb" 0
		Delete "$INSTDIR\${KZ_API_NAME}.pdb"
		
		IfFileExists "$INSTDIR\${MO_DLL_NAME}.dll" 0
		Delete "$INSTDIR\${MO_DLL_NAME}.dll"
		
		IfFileExists "$INSTDIR\${BLV_DLL_NAME}.dll" 0
		Delete "$INSTDIR\${BLV_DLL_NAME}.dll"
		
		IfFileExists "$INSTDIR\Uninstall.exe" 0
		Delete "$INSTDIR\Uninstall.exe"
		
		IfFileExists "$INSTDIR\uninstall.ico" 0
		Delete "$INSTDIR\uninstall.ico"
		
		IfFileExists "$INSTDIR\teklabz.ico" 0
		Delete "$INSTDIR\teklabz.ico"
		
		IfFileExists "$INSTDIR\DatabaseUpgrade\00-seed-version.xml" 0
		Delete "$INSTDIR\DatabaseUpgrade\00-seed-version.xml"
		
		IfFileExists "$INSTDIR\DatabaseUpgrade\01-path-analysis-schema.xml" 0
		Delete "$INSTDIR\DatabaseUpgrade\01-path-analysis-schema.xml"
		
		IfFileExists "$INSTDIR\DatabaseUpgrade\tk-pa-security-roles.xml" 0
		Delete "$INSTDIR\DatabaseUpgrade\tk-pa-security-roles.xml"
		
		IfFileExists "$INSTDIR\DatabaseUpgrade\tk-pa-object-roles.xml" 0
		Delete "$INSTDIR\DatabaseUpgrade\tk-pa-object-roles.xml"
		
		StrCpy $0 "$INSTDIR\DatabaseUpgrade"
		IfFileExists "$INSTDIR\DatabaseUpgrade" 0
		Call ${un}DeleteDirIfEmpty
		
		StrCpy $0 "$INSTDIR\Documents"
		IfFileExists "$INSTDIR\Documents" 0
		Call ${un}DeleteDirIfEmpty
		
		StrCpy $0 "$INSTDIR"
		IfFileExists "$INSTDIR" 0
		Call ${un}DeleteDirIfEmpty
	  
		StrCpy $0 "$INSTDIR"
		IfFileExists "$INSTDIR" 0
		Call ${un}DeleteDirIfEmpty
		
		!insertmacro MUI_STARTMENU_GETFOLDER 0 $StartMenuFolder
		IfFileExists "$SMPROGRAMS\$StartMenuFolder\${PRODUCT_NAME}" 0
		RMDir /r "$SMPROGRAMS\$StartMenuFolder\${PRODUCT_NAME}"
		
		StrCpy $0 "$SMPROGRAMS\$StartMenuFolder"
		Call ${un}DeleteDirIfEmpty
		
		DetailPrint "Removing Registry Keys"
		DeleteRegKey HKLM "Software\${COMPANY}\${PRODUCT_NAME_SHORT}"		
		DeleteRegKey HKLM "${ARP}"
		
	FunctionEnd
	
	Function ${un}LockedApplicationList
		!insertmacro MUI_HEADER_TEXT 'Running Applications' ''
		LockedList::AddModule $ArcGisBinPath\agfshp.exe
		LockedList::AddModule $ArcGisBinPath\aisdtslist.exe
		LockedList::AddModule $ArcGisBinPath\aisdtsp2a.exe
		LockedList::AddModule $ArcGisBinPath\aisdtsr2g.exe
		LockedList::AddModule $ArcGisBinPath\AppESRIPrintLocal.exe
		LockedList::AddModule $ArcGisBinPath\AppLockMgr.exe
		LockedList::AddModule $ArcGisBinPath\AppROT.exe
		LockedList::AddModule $ArcGisBinPath\ArcCatalog.exe
		LockedList::AddModule $ArcGisBinPath\ArcGISAppLauncher.exe
		LockedList::AddModule $ArcGisBinPath\ArcGlobe.exe
		LockedList::AddModule $ArcGisBinPath\ArcMap.exe
		LockedList::AddModule $ArcGisBinPath\ARConfig.exe
		LockedList::AddModule $ArcGisBinPath\ArcReader.exe
		LockedList::AddModule $ArcGisBinPath\ArcReaderHost.exe
		LockedList::AddModule $ArcGisBinPath\ArcScene.exe
		LockedList::AddModule $ArcGisBinPath\avmifshp.exe
		LockedList::AddModule $ArcGisBinPath\categories.exe
		LockedList::AddModule $ArcGisBinPath\CatInstall.exe
		LockedList::AddModule $ArcGisBinPath\DataLicInstall.exe
		LockedList::AddModule $ArcGisBinPath\DesktopAdmin.exe
		LockedList::AddModule $ArcGisBinPath\ESRIErrorReporter.exe
		LockedList::AddModule $ArcGisBinPath\ESRIErrorWebReporter.exe
		LockedList::AddModule $ArcGisBinPath\EsriNumpy.EXE
		LockedList::AddModule $ArcGisBinPath\EsriPython251.exe
		LockedList::AddModule $ArcGisBinPath\esriRegSvr32.exe
		LockedList::AddModule $ArcGisBinPath\Import71.exe
		LockedList::AddModule $ArcGisBinPath\mp.exe
		LockedList::AddModule $ArcGisBinPath\Python.exe
		LockedList::AddModule $ArcGisBinPath\RegCat.exe
		LockedList::AddModule $ArcGisBinPath\RegisterFixed93.exe
		LockedList::AddModule $ArcGisBinPath\SchematicDesigner.exe
		LockedList::AddModule $ArcGisBinPath\SetupCrystal.exe
		LockedList::AddModule $ArcGisBinPath\SetupSDB.exe
		LockedList::AddModule $ArcGisBinPath\SHAPEDXF.EXE
		LockedList::AddModule $ArcGisBinPath\shpagf.exe
		LockedList::AddModule $NeToolboxBinPath\NetworkEngineer.Toolbox.exe
		LockedList::Dialog /autonext
		Pop $R0
	FunctionEnd
	
	Function ${un}removeToolboxTool
		exch $R0
		DetailPrint "Removing Toolbox Tool $R0 from default configs"
		${xml::LoadFile} "$NeBinPath\Configuration\ToolboxConfiguration.xml" $0
		${xml::RootElement} $2 $0
		${xml::XPathNode} "/Configuration/DefaultTools/Tool[@name='$R0']" $0
		IntCmp $0 -1 notFound	
		${xml::RemoveNode} $0
	notFound:
		${xml::SaveFile} "$NeBinPath\Configuration\ToolboxConfiguration.xml" $0
		${xml::Unload}
		pop $R0
	FunctionEnd
	
!macroend

!insertmacro SHARED_FUNCTIONS ""
!insertmacro SHARED_FUNCTIONS "un."

Function VersionCompare
 
	Exch $1
	Exch
	Exch $0
	Exch
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $7
 
	begin:
	StrCpy $2 -1
	IntOp $2 $2 + 1
	StrCpy $3 $0 1 $2
	StrCmp $3 '' +2
	StrCmp $3 '.' 0 -3
	StrCpy $4 $0 $2
	IntOp $2 $2 + 1
	StrCpy $0 $0 '' $2
 
	StrCpy $2 -1
	IntOp $2 $2 + 1
	StrCpy $3 $1 1 $2
	StrCmp $3 '' +2
	StrCmp $3 '.' 0 -3
	StrCpy $5 $1 $2
	IntOp $2 $2 + 1
	StrCpy $1 $1 '' $2
 
	StrCmp $4$5 '' equal
 
	StrCpy $6 -1
	IntOp $6 $6 + 1
	StrCpy $3 $4 1 $6
	StrCmp $3 '0' -2
	StrCmp $3 '' 0 +2
	StrCpy $4 0
 
	StrCpy $7 -1
	IntOp $7 $7 + 1
	StrCpy $3 $5 1 $7
	StrCmp $3 '0' -2
	StrCmp $3 '' 0 +2
	StrCpy $5 0
 
	StrCmp $4 0 0 +2
	StrCmp $5 0 begin newer2
	StrCmp $5 0 newer1
	IntCmp $6 $7 0 newer1 newer2
 
	StrCpy $4 '1$4'
	StrCpy $5 '1$5'
	IntCmp $4 $5 begin newer2 newer1
 
	equal:
	StrCpy $0 0
	goto end
	newer1:
	StrCpy $0 1
	goto end
	newer2:
	StrCpy $0 2
 
	end:
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Exch $0
FunctionEnd

Function addToolboxTool
		exch $R0
		DetailPrint "Adding Toolbox Tool $R0 to default configs"
		${xml::LoadFile} "$NeBinPath\Configuration\ToolboxConfiguration.xml" $0
		${xml::RootElement} $2 $0
		${xml::XPathNode} "/Configuration/DefaultTools/Tool[@name='$R0']" $0
		IntCmp $0 -1 skipRemove	
		${xml::RemoveNode} $0
	skipRemove:	
		${xml::CreateNode} '<Tool name="$R0"><Information identifier="$R0" visible="true" /></Tool>' $1
		${xml::RootElement} $2 $0
		${xml::XPathNode} "/Configuration/DefaultTools" $0
		IntCmp $0 -1 notFound
		${xml::InsertEndChild} $1 $0
	notFound:
		${xml::SaveFile} "$NeBinPath\Configuration\ToolboxConfiguration.xml" $0
		${xml::Unload}
		pop $R0
FunctionEnd
;---------------------------------------------------
;---------------------------------------------------
